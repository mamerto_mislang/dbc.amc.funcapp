const service = require('../lib/services');
const { jsonResponse, jsonCustomResponse, jsonBusParams  } = require('../lib/services/common/helpers');
const {logIt2} = require('../lib/services/common/logger');
const fa = require('../lib/services/common/share/loglevel');
const bus = require('../lib/services/servicebus');

const sendProcessContractRequestToQueue = async function(dealId, matterId, oid, context)
{
    const body = {
        dealId     : `${dealId}`,
        matterId   : `${matterId}`,
        oid        : `${oid}`
    };
    
    logIt2(fa.info, context, `[deal::${dealId}] sendProcessContractRequestToQueue to bus`, [JSON.stringify(body)]);
    const payload = jsonBusParams(body, context);
    await bus.sendMessageId(payload, 'contract-updates-queue', `${dealId}`, context);
}

module.exports = async function (context, req) {
    const cmdType  = context.bindingData.cmdtype;    
    const matterId = req.query && (req.query.matter_id || req.query.matterId) || '';
    const hash = req.query && (req.query.hash) || '';
    
    try {        
        
        logIt2(fa.info, context, `[[pscemreview cmdType:${cmdType} matterId:${matterId} hash:${hash}`);
        if (cmdType == 'getocrfields'){
            const state    = req.query && (req.query.state || req.query.state);
            logIt2(fa.info, context, `[[pscemreview state]:${state}`);
            const ocrFields = await service.getOcrFields(state, context);            
            context.res  = jsonCustomResponse(ocrFields);

        }else if(cmdType == 'getocrdata' && matterId){
            const ocrData = await service.getOcrData(matterId, hash, context);     
              
            context.res  = jsonCustomResponse(ocrData);
        }else if(cmdType == 'postocrdata' && matterId){
            const ocrReviewData = req.body || '{}';            
            const ocrResponse = await service.postOcrData(matterId, ocrReviewData, context);
            await sendProcessContractRequestToQueue(ocrResponse.dealId, matterId, ocrResponse.oid, context);

            context.res  = jsonCustomResponse({matterId: matterId, message : 'Successfully updated'});
        }
        
    } catch (err) {
        const errMsg   = err.message || '{error : 1}';
        const errStack = err.stack || '';        
        // logIt2(fa.error, context, `[[pscemreview].main(-) Error response >>`, [err.message, errStack]);
        context.log.error("[[pscemreview].main(-) Error response >>", [err.message, errStack]);
        context.res = jsonResponse(errMsg, 400);

    }finally {
        context.done();
    }
}
