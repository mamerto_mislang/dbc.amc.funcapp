const helper = require('../lib/services/common/helpers');
const common = require('../lib/services/common/constant');
const { sendEventTopic } = require('../lib/services/eventgrid/callApi');
const { BlobServiceClient } = require('@azure/storage-blob');
const multipart = require("parse-multipart");
const { pipedriveDeal } = require('../lib/services/pipedrive');
const service = require('../lib/services');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    const dealId =  req.query && (req.query.deal_id || req.query.dealId);
    context.log(`[${correlationId}] processing request (+)....`);

    try {
        const subject = req.query && (req.query.subject || 'ContractUploadForm');
        const relevantFiles = req.query && (req.query.relevantFiles || req.query.relevantFile);
        context.log(`[${correlationId}](+) debug params:`, dealId, req.query);

        let alreadyExist = false;

        if (dealId){
            context.log(`[${correlationId}](+) inside:`);
            const pipe = await pipedriveDeal(dealId, context);
            alreadyExist = (pipe.stageId==common.PD_MCREADY_STATUS) ? true : false;
            context.log(`[${correlationId}-deal::${dealId}](+)debug pipe:`, pipe, alreadyExist);
        }
        const isOcr = helper.enableOcr(subject);

        if (alreadyExist){
            const customData = helper.errorResponse(dealId);
            context.res = helper.jsonResponse(customData);        
        
        }else if (!req.body || req.body===undefined){

            const customData = helper.createEventData2(req, null, null, isOcr);
            context.log(`[${correlationId}-deal::${dealId}](+)nofile customData:`, customData);
            const topicResponse = await sendEventTopic(customData, isOcr);        
            context.log(`[${correlationId}-deal::${dealId}](~)nofile sendEventTopic response:`, topicResponse);
            context.res = helper.jsonResponse(customData);
            
        }else{

            const bodyBuffer = Buffer.from(req.body);
            const boundary = multipart.getBoundary(req.headers['content-type']);    
            const parts = multipart.Parse(bodyBuffer, boundary);
            const blobServiceClient = await BlobServiceClient.fromConnectionString(process.env.AZURE_STORAGE_CONNECTION);        
            const container = process.env.AZURE_STORAGE_CONTAINER;
            const containerClient = await blobServiceClient.getContainerClient(container);
            const blobName = helper.getBlobName(parts[0].filename, req);
            const blockBlobClient = containerClient.getBlockBlobClient(blobName);              

            const uploadBlobResponse = await blockBlobClient.upload(parts[0].data, parts[0].data.length);
            context.log(`[${correlationId}-deal::${dealId}](+) uploadBlobResponse:`, uploadBlobResponse);
            let relevantMapFiles;

            if (relevantFiles){
                relevantMapFiles = await service.getMapDocuments(relevantFiles, context);
            }
            const customData =  helper.createEventData2(req, container, blobName, isOcr, relevantMapFiles);
            context.log(`[${correlationId}-deal::${dealId}](+) customData:`, JSON.stringify(customData));  
            const topicResponse = await sendEventTopic(customData, isOcr);          
            context.res = helper.jsonResponse(customData);
        }

    } catch (err) {        
        context.log.error(`[${correlationId}-deal::${dealId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[${correlationId}-deal::${dealId}].main(-) Stacktrace >>`, err.stack);
        }
        context.res = jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}
