const helper = require('../lib/services/common/helpers');
const { sendContractUpdatesEventTopic } = require('../lib/services/eventgrid/callApi');
const { BlobServiceClient } = require('@azure/storage-blob');
const multipart = require("parse-multipart");
const service = require('../lib/services');
const mysqldb = require('../lib/services/database/mysql');

module.exports = async function (context, req) {
    const matterId =  req.query && (req.query.matter_id || req.query.matterId);
    const ctype =  req.query && req.query.ctype || '';

    let dealId;
    context.log(`[contract-updates processing request (+)....`);

    try {
        context.log(`[contract-updates(+) debug params:`, matterId, ctype, req.query);
        let matterExist = false;
        let state;
        let matterType;

        if (matterId){
            context.log(`[contract-updates](+) amc inside:`);            
            const res = await service.checkMatterData(matterId, context);
            dealId    = (res && res.dealId) || 0;
            
            if(!res || dealId === 0){
                context.log(`[contract-updates(~)] matterId::${matterId} (~) check fastDB`);
                const fastResponse = await mysqldb.getMatterFromFAST(matterId, context);
                if (fastResponse && fastResponse.length > 0){
                    dealId     = fastResponse[0].id;
                    matterType = fastResponse[0].matter_type_id;
                    context.log(`[contract-updates(~)] matterId::${matterId} (~)debug fastResponse `, JSON.stringify(fastResponse));
                }                
            }else{
                matterType = res.matterType;
            }
                        
            if (dealId > 0 ){
                matterExist =  true;
                state = helper.getStateMatterType(parseInt(matterType));
            }            
            context.log(`[contract-updates(~)] matterId::${matterId} (+)debug state: ${state} db.matterExist:`, res, matterExist);
            
            if (!matterExist){
                context.log.error(`[contract-updates(~)] matterId::${matterId}(-) error in processing data!`);
                const customData = helper.errorMatterResponse(matterId, "Ther matter is either OLD matter type or does not exist!");
                context.res = helper.jsonResponse(customData, 400);            
            }else{
                
                context.log(`[contract-updates(~)] matterId::${matterId}(-) processing data...`);

                const bodyBuffer         = Buffer.from(req.body);
                const boundary           = multipart.getBoundary(req.headers['content-type']);    
                const parts              = multipart.Parse(bodyBuffer, boundary);
                const blobServiceClient  = BlobServiceClient.fromConnectionString(process.env.AZURE_STORAGE_CONNECTION);        
                const container          = process.env.AZURE_STORAGE_CONTAINER;
                const containerClient    = blobServiceClient.getContainerClient(container);
                const blobName           = helper.getContractUpdatesBlobName(parts[0].filename, matterId);
                const blockBlobClient    = containerClient.getBlockBlobClient(blobName);
                const uploadBlobResponse = await blockBlobClient.upload(parts[0].data, parts[0].data.length);

                context.log(`[contract-updates(~)] matterId::${matterId} uploadBlobResponse:`, JSON.stringify(uploadBlobResponse));
                
                const customData =  helper.createContractUpdatesEventData(req, container, blobName, state, dealId, matterId);
                context.log(`[contract-updates(~)] matterId::${matterId} customData:`, JSON.stringify(customData));
                await sendContractUpdatesEventTopic(customData);
                context.res = helper.jsonResponse(customData[0]);
            }
        }

    } catch (err) {        
        context.log.error(`[contract-updates(~)].main(-) matterId::${matterId} Error response >>`, err.message || err);        
        if(err.stack){
            context.log.error(`[contract-updates(~)].main(-) matterId::${matterId} Stacktrace >>`, err.stack);
        }
        context.res = helper.jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}
