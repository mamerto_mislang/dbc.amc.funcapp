const service = require('../lib/services');
const { accessToken } = require('../lib/services/actionstep/getASToken');
const collection = require('../lib/services/actionstep/collections');

const { jsonResponse, jsonCustomResponse, jsonBusParams } = require('../lib/services/common/helpers');
const { validMatterSourcePipe } = require('../lib/services/pipedrive/deal');
const {logIt2} = require('../lib/services/common/logger');
const fa = require('../lib/services/common/share/loglevel');
const bus = require('../lib/services/servicebus');

const sendCSVRequestToQueue = async function(dealId, matterId, context)
{
    const body = {
        dealId     : parseInt(dealId),
        matterId   : parseInt(matterId)
    };
    context.log(`[deal::${dealId}] sendCSVRequestToQueue to bus`, JSON.stringify(body));
    const payload = jsonBusParams(body, context);
    await bus.sendMessageId(payload, 'request-upload-as-queue', `${dealId}`, context);
}

module.exports = async function (context, req) {
    const dealId   = req.body.dealId;
    const matterId = req.body.matterId;
    const oidX     = req.body.oid || '';
    const token    = (await accessToken());
    let matter;
    let deal;

    try {
        
        logIt2(fa.info, context, `[deal::${dealId}](+)ocrprocessupdates matterId:${matterId} oidX:${oidX}`);
        const mcPipeSource = await service.getMatterSource(dealId, context);
        const pipe = validMatterSourcePipe(dealId, mcPipeSource, context);

        if (!pipe || !pipe.pipedata){           
           matter = await service.getActionstepMatterDetails(matterId, token, context);
           deal   = await service.getPipedriveDeal(dealId, context);
           logIt2(fa.info, context, `[deal::${dealId}].main(~) matter source from pipedrive/actionstep > `, [JSON.stringify(matter), JSON.stringify(deal)]);

           if ((deal && deal.error) || !deal){
                throw new Error(`Failed to fetch deal ${dealId} in Pipedrive. Error: ${(deal && deal.msg)||''}`);
           }else if(!matter){
                throw new Error(`Failed to fetch matter ${matterId} in Acionstep!`);
           }

        }else{
            matter = pipe.matter;
            deal   = pipe.pipedata.deal;
            logIt2(fa.info, context, `[deal::${dealId}].main(~) matter source pipedata received >>`, [JSON.stringify(pipe)]);
        }
        
        const ocrParticipantRoles = [71,155,157,148,158,163,164,125,127];
        // (await service.deleteMatterParticipants(matterId, token, context, ocrParticipantRoles));
        const sourceDeleteParticipants = await service.deleteMatterParticipantSource(matterId, token, context, ocrParticipantRoles);
        const address = {};
        const ocrResult = await service.processOCR(matter, deal, token, {"ocrUpdate": true, "oid": oidX, "sourceDeleteParticipants" : sourceDeleteParticipants}, context);
        (await collection.updateMatterDataCollection(matter.matterId, deal, address, ocrResult, token, context));

        if (ocrResult.ocrExtended){
            (await service.createMatterFileNote(matterId, 'Processing of Signed Contract Document - Done!', token, context));
        }        
        (await service.notifyTeamsPscem(matter, deal, context));
        
        if(matter && matter.matterId){
            await sendCSVRequestToQueue(dealId, matterId, context);
            context.res  = jsonCustomResponse({ message: 'Deal successfully processed!', dealId: parseInt(dealId), matterId : parseInt(matter.matterId), matterName : matter.matterName });
        }else{                              
            throw new Error(`PSCEM: Failed to process dealId ${dealId} !`);
        }        
        
    } catch (err) {
        const errMsg   = err.message || 'error';
        const errStack = err.stack || '';
        await service.notifyTeamsPscemError(dealId, matterId, errMsg, errStack, context);
        
        logIt2(fa.error, context, `[deal::${dealId}].main(-) Error response >>`, [err.message, errStack]);
        context.res = jsonResponse(errMsg, 400);

    }finally {
        context.done();
    }
}
