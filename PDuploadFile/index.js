const helper = require('../lib/services/common/helpers');
const tule = require('../lib/services/common/dataTools');
const doc = require('../lib/services/common/documents');
const pipedrive = require('../lib/services/pipedrive');
const { sleep } = require('../lib/services/common/utils');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    context.log(`[${correlationId}::PDuploadFile] processing request (+)....`);

    try {       
        context.log(`[${correlationId}::PDuploadFile](+) debug body:`, req.body);
        const dealId = (req.body && req.body.dealid);
        const contractUrl = (req.body && req.body.contracturl);
        context.log(`[${correlationId}::PDuploadFile](+) [dealId:${dealId} contract_url: ${contractUrl}]:`);      

        const payload = await tule.urlGetContent(contractUrl);
        const filename = helper.getBlobName2(contractUrl);
        const docType = doc.getDocumentType(filename);
        const res = await pipedrive.uploadDocumentInPipedrive(dealId,filename, payload, docType, context);
        if (res.message && res.message.indexOf('failed') !=-1){
            
            context.log.error(`[${correlationId}::PDuploadFile](~) uploadDocumentInPipedrive-1 error:`, res.message );
            let res2;

            for (let index = 1; index < 4; index++) {
                context.log(`[${correlationId}::PDuploadFile](~) uploadDocumentInPipedrive2 retry counter: #`, index );
                sleep(3000);

                res2 = await pipedrive.uploadDocumentInPipedrive(dealId,filename, payload, docType, context);
                if (res2.message && res2.message.indexOf('failed') !=-1){
                    context.log(`[${correlationId}::PDuploadFile](~) uploadDocumentInPipedrive2 error: #${index}`, res2.message );
                    continue;
                }else{
                    break;
                }
            }

            if (res2.message && res2.message.indexOf('failed') !=-1){
                throw new Error(res2.message);
            }
            context.log(`[${correlationId}::PDuploadFile](~) uploadDocumentInPipedrive2: `, res2.data );
            context.res = helper.jsonResponse(res2.data);

        }else{
            context.log(`[${correlationId}::PDuploadFile](~) uploadDocumentInPipedrive:`, res.data );
            context.res = helper.jsonResponse(res.data);
        }
        
    } catch (err) {
        context.res = helper.jsonResponse(err.message, 400);
        context.log.error(`[${correlationId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[${correlationId}].main(-) Stacktrace >>`, err.stack);
        }

    }finally {
        context.done();
    }
}
