const service = require('../lib/services');
const { accessToken } = require('../lib/services/actionstep/getASToken');
const { getFlowData } = require('../lib/services/pipedrive');
const { personDetailsPipe } = require('../lib/services/pipedrive/person');
const collection = require('../lib/services/actionstep/collections');
const datalake = require('../lib/services/datalake');
const { jsonResponse, jsonCustomResponse } = require('../lib/services/common/helpers');
const { validPublishPipe } = require('../lib/services/pipedrive/deal');
const { send2BusQueueV2 } = require('../lib/services/notify');
let AMCStep = 0;
let mcInfoBus;

const processMatter = async function(xpipe, context)
{
    const dealId    = xpipe.dealId;
    const deal      = xpipe.pipedata.deal;
    const address   = xpipe.pipedata.address;
    const personId  = xpipe.pipedata.personId;
    const token     = (await accessToken());

    // (await service.resetPipedriveAMCLabel(dealId, context));
    const mcInfo = await service.getDealLastStage(dealId, context);
    context.log(`[deal::${dealId}].dataprocess (~) mcinfo:`, JSON.stringify(mcInfo));

    let lastStageId = (mcInfo && mcInfo.stageId) || 0;
    
    if (!mcInfo){
        context.log(`[deal::${dealId}] dataprocess - deal has just been created! >> `, lastStageId);
    }
    
    //await send2BusQueueV2(AMCStep, dealId, 0, 'info', '<b>STATUS:</b> Re:Automatic Matter Creation initiated ....', context);
    
    AMCStep=2;
    context.log(`[deal::${dealId}] mainrun stage-${lastStageId}@step-${AMCStep}(~)`);
    let matter,matterId,clientId;
    const person  = (await personDetailsPipe(dealId, personId, context));

    if (lastStageId <= AMCStep){        
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) matter creation init...`);
        const client = (await service.processClientParticipant(deal, person, token, context));
        matter   = (await service.createMatter(deal, client, token, context));
        matterId = matter.matterId;
        clientId = (client.clientIdCode && client.clientIdCode.clientId) || false;
        mcInfoBus =  {...matter, clientId: clientId};

    }else{
        context.log(`[deal::${dealId}] skipping stage-${lastStageId}@step-${AMCStep}(~) matter creation info fetched`);

        matter = {matterId: mcInfo.matterId, matterName: mcInfo.matterName};
        matterId = mcInfo.matterId;
        clientId = mcInfo.clientId;
    }
    
    AMCStep=3;
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) matter name update`);
        (await service.updateMatterName(matterId, matter.matterName, token, context));
    } 
        
    AMCStep=4;
    if (lastStageId <= AMCStep || lastStageId == 8 ){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) reset participants`);
        lastStageId = AMCStep;

        (await service.deleteMatterParticipants(matterId, token, context));

        (await service.createMatterParticipants(matterId, deal, clientId, token, context));
        (await service.createPropertyAddressParticipant(matterId, address, person, token, context));
        (await service.createSalesPersonParticipant(matterId, deal, token, context));                     
        (await service.createSMSGatewayParticipant(matterId, token, context));
    }
    
    AMCStep=5;    
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) pd filenotes/upload pd to as`);
        const pdDataFlow = (await getFlowData(deal.dealId));    
        (await service.updateMatterFileNotes(matterId, pdDataFlow.notes, token, context));
        (await service.uploadDocsPipedriveToActionstep(matterId, pdDataFlow.files, token, context));
    }
        
    AMCStep=6;
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) matter info in pd`);
        (await datalake.updateMatterPipedrive(matterId, deal.dealId, context));
    }
        
    AMCStep=7;
    // if (nextToProcess(lastStageId, system.pd_stage ))
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) update pd status`);
        (await service.updatePipedriveStatus(matter, deal.dealId, context));
    }
        
    AMCStep=8;    
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~) ocr processing`);
        const ocrResult = await service.processOCR(matter, deal, token, {"ocrUpdate": false, "oid": 0}, context);        
        (await collection.updateMatterDataCollection(matterId, deal, address, ocrResult, token, context));        
        (await service.notifyTeams(matter, deal, address, ocrResult.dailyMatterCount, context));
    }
        
    AMCStep=9;    
    if (lastStageId <= AMCStep){
        context.log(`[deal::${dealId}] running stage-${lastStageId}@step-${AMCStep}(~)MC Notes to PD`);
        (await service.sendMCNotesToPipedrive(matter, deal, person, address, context));
    }
    return {...matter, clientId: clientId};
}

module.exports = async function (context, req) {
    const dealId = req.body.dealId;

    try {
        const pipe = validPublishPipe(req.body, context);
        if (!pipe){
            context.res = jsonResponse(`Deal ${dealId} is invalid!`);
        }else{
            
            context.log.info(`[deal::${dealId}].main(~) re:pipedata received >>`, JSON.stringify(pipe));
            const matter = await processMatter(pipe, context);
            if(matter){
                const msg = '<b>STATUS:</b> Re:Automatic Matter Creation - Completed!';
                // await send2BusQueueV2(AMCStep, dealId, matter.matterId, 'info', msg, context);
                context.res  = jsonCustomResponse({ message: 'Deal successfully processed!', dealId: pipe.dealId, matterId : matter.matterId, matterName : matter.matterName });
            }else{
                context.res = jsonResponse(`Deal ${dealId} encountered unknown error!`);
            }

        }       
        
    } catch (err) {
        context.log.error(`[deal::${dealId}].main(-) Error response >>`, err.message);
        const errMsg   = err.message || 'error';
        const errStack = (err.stack) ? (err.stack).replace(/\n/gi, "</br/>") : '';
        const msg = '<b>STATUS:</b> AMC encountered error!<br>Please contact IT team for Support!';

       // await send2BusQueueV2(AMCStep, dealId, 0, 'error', msg, context, errMsg, errStack, JSON.stringify(mcInfoBus));
        if(err.stack){
            context.log.error(`[deal::${dealId}].main(-) Stacktrace >>`, err.stack);
        }
        context.res = jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}

