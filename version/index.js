module.exports = async function (context, req) {
    context.log('Version function processed a request.');

    const kind = (req.query.kind || (req.body && req.body.kind)) || '';
    
    const responseMessage = {
        version : "v3.01",
        buildDate : "June 18 2022",
        branchRelease : "master && dev && all-pscem-refactored-v3",
        releaseDescription : "PSCEM Review All - Refactored " + kind,
        history : [            
            {
                version : "v2.08",
                buildDate : "April 23 2022",
                branchRelease : "master && property-address-collection && feature/pscem-review-page-transform-ui",
                releaseDescription : "PSCEM Review Page ",
            },
            {
                version : "v2.07",
                buildDate : "April 19 2022",
                branchRelease : "property-address-collection && feature/pscem-review-page-transform-ui",
                releaseDescription : "PSCEM Review Page ",
            },
            {
                 version : "v2.01",
                 buildDate : "March 25 2022",
                 branchRelease : "master-rel-v2-hotfixes",
                 releaseDescription : "Urgent Release ",
            },
            {
                version : "v1.01",
                buildDate : "March 03 2022",
                branchRelease : "oes-605-upload-ocr-result",
                releaseDescription : "Sprint 4 ",
            }
        ]
    }

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: responseMessage
    };
}