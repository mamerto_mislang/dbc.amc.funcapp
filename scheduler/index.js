const service = require('../lib/services');
const { jsonResponse } = require('../lib/services/common/helpers');
const { send2BusQueue } = require('../lib/services/notify');

module.exports = async function (context, myTimer) {
    var timeStamp = new Date().toISOString();
    
    if (myTimer.isPastDue){
        context.log('AMC.scheduler is running late!');        
    }
    context.log('AMC.scheduler timer trigger function ran!', timeStamp);   
    
    try {
        
        const status = await service.resetDailyMatter(context);
        context.log('AMC.scheduler resetDailyMatter: ', status);   

        if (status.rc2){
            throw new Error('Daily Matter was not reset!');
        }
        context.res = jsonResponse('Daily Matter was successfully reset!');

    } catch (err) {
        context.log.error(`[sched].main(-) Error response >>`, err.message);
        const errMsg   = err.message || 'error';
        const errStack = (err.stack) ? (err.stack).replace(/\n/gi, "</br/>") : '';
        await send2BusQueue(0, 0, 'error', '<b>STATUS:</b>AMC resetDailyMatter encountered error!<br>System Self Alert!', context, errMsg, errStack);

        if(err.stack){
            context.log.error(`[sched].main(-) Stacktrace >>`, err.stack);
        }
        context.res = jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
};

