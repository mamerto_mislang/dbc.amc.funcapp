const helper = require('../lib/services/common/helpers');
const service = require('../lib/services');
const { testJSON } = require('../lib/services/common/utils');

module.exports = async function (context, req) {
    const dealId =  req.query && (req.query.dealId);
    const cmdType =  context.bindingData.cmdtype;
    
    const pdContent = req.body;
    const isJSON = testJSON(JSON.stringify(pdContent));
    const jLen   = (pdContent || req.body || '').length;

    context.log(`[pipedrive-updates processing request (+)....debug body cmdType:${cmdType} (jLen: ${jLen} json: ${isJSON})`, pdContent, pdContent.length, JSON.stringify(pdContent));

    try {
        context.log(`[pipedrive-updates(+) debug params:`, dealId, req.query);
        if (dealId){
            
            context.res = helper.jsonResponse('Success');          
        }
       

    } catch (err) {        
        context.log.error(`[pipedrive-updates(~)].main(-) dealId::${dealId} Error response >>`, err.message || err);        
        if(err.stack){
            context.log.error(`[pipedrive-updates(~)].main(-) dealId::${dealId} Stacktrace >>`, err.stack);
        }
        context.res = helper.jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}
