const axios = require('axios');

// const process = {
//   env : {
//     // ACTIONSTEP_AUTH_HOST : "https://testdev.conveyancingapp.io/api/actionstep/key/get"
//     ACTIONSTEP_AUTH_HOST : "https://apidev2-staging.conveyancingapp.io/fast/refresh_token"
//     // ACTIONSTEP_AUTH_HOST : "https://tools.thinkconveyancing.com.au/api/actionstep/key/get"
//   } 
// }

const getAccessToken = async function () {
  const url = `${process.env.ACTIONSTEP_AUTH_HOST}`;
  const response = await axios.get(url, {
    headers: {
        'Content-Type': 'application/json',
    }
  });
  return response.data || {};
};


exports.accessToken = async function () {
  const response = await getAccessToken();
  const url = `${process.env.ACTIONSTEP_AUTH_HOST}`;
  
  //staging
  if (url.indexOf('staging') !=-1){
      return response;
  
    }else{
      if (response.data.actionstep.access_token){
          return response.data.actionstep.access_token;
      }  
      return false;
  }
}
