const axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
const {encode, decode} = require('base64-arraybuffer');
const {logIt2} = require('../common/logger');
const fa = require('../common/share/loglevel');

// let process = {
//   env : {
//     ACTIONSTEP_API_ENDPOINT : "https://ap-southeast-2.actionstepstaging.com/api/rest"
//     // ACTIONSTEP_API_ENDPOINT : "https://ap-southeast-2.actionstep.com/api/rest"
//   }
// }

exports.post = async function (resource, xdata, bearer, ctx) {
    const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
    const options = {
      headers: {
        'Authorization': `Bearer ${bearer}`,
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
      }
    };  
    const response = await axios.post(url, xdata, options);
    return response.data || {};
};

exports.post2 = async function (resource, xdata, bearer, context) {
  const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
  const options = {
    headers: {
      'Authorization': `Bearer ${bearer}`,
      'Content-Type': 'application/vnd.api+json',
      'Accept': 'application/vnd.api+json',
    }
  };  

  try {
    logIt2(fa.info, context, '[callApi].post2 debug >', [JSON.stringify(xdata)]);
    const response = await axios.post(url, xdata, options);
    if (response && response.data){      
        const xresponse = JSON.stringify(response.data);
        logIt2(fa.debug, context, '[callApi].post2 debug >>', [JSON.stringify(response.data)] );

        if ((xresponse).indexOf('Access Denied') !=-1){
            throw new Error('Access Denied response from Actionstep');
        }
    }
    return response.data || {};

  } catch (err) {
      const errMsg = (err && err.response && err.response.data) ? err.response.data : err.message || err;
      const errMsgJson = {'CustomizedError' : errMsg};
      logIt2(fa.error, context, '[callApi].post2 error >>', [JSON.stringify(errMsgJson)] );
      
      const errorLast = JSON.stringify(errMsg);
      if ((errorLast||'').indexOf('ValidationError') !=-1){
          logIt2(fa.info, context, '[callApi].post2 ignoring this error >>', [errorLast] );
      }else{
          throw Error(JSON.stringify(errMsgJson));
      }      
  }
};

exports.put = async function (resource, xdata, bearer, context) {
  const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
  const options = {
    headers: {        
      'Authorization': `Bearer ${bearer}`,
      'Content-Type': 'application/vnd.api+json',
      'Accept': 'application/vnd.api+json',
    }
  };
  const response = await axios.put(url, xdata, options);
  return response.data || {};
};

exports.put2 = async function (resource, xdata, bearer, context) {
  const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
  const options = {
    headers: {        
      'Authorization': `Bearer ${bearer}`,
      'Content-Type': 'application/vnd.api+json',
      'Accept': 'application/vnd.api+json',
    }
  };
  // const response = await axios.put(url, xdata, options);
  // return response.data || {};

  try {
    const response = await axios.put(url, xdata, options);
    return response.data || {};
  } catch (err) {
    const errMsg = (err && err.response && err.response.data) ? err.response.data : err;
    context.log.error("[callApi].put2 error >>", JSON.stringify(errMsg));
    throw Error(errMsg);
  }
};


exports.get = async function (resource, bearer, ctx) {
    const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
    const options = {
      headers: {
        'Authorization': `Bearer ${bearer}`,
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
      }
    };
    const response = await axios.get(url, options);
    return response.data || {};
};

exports.get2 = async function (resource, bearer, context) {
  const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
  const options = {
    headers: {
      'Authorization': `Bearer ${bearer}`,
      'Content-Type': 'application/vnd.api+json',
      'Accept': 'application/vnd.api+json',
    }
  };
  // const response = await axios.get(url, options);
  // console.log("GET response >>", response);
  // return response.data || {};

  try {
      const response = await axios.get(url, options);
      return response.data || {};
      
  } catch (err) {
    const errMsg = (err && err.response && err.response.data) ? err.response.data : err.message || err;
    const errMsgJson = {'CustomizedError' : errMsg};
    logIt2(fa.error, context, '[callApi].get2 error >>', [JSON.stringify(errMsgJson)]);
    throw Error(JSON.stringify(errMsgJson));
}  
};

exports.upload_Working = async function(resource, filename, payload, token, docType, ctx)
{
    const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}?part_count=1&part_number=1`;
    var data = new FormData();
    data.append("file", payload, {contentType: docType, filename: filename });
    var config = {
        method: 'post',
        url: url,
        headers: { 
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${token}`,
            ...data.getHeaders()
        },
        data : data
    };
    const response = await axios(config);
    return response;
}

exports.upload = async function(resource, filename, payload, token, docType, ctx)
{
    const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}?part_count=1&part_number=1`;
    var data = new FormData();
    data.append("file", payload, {contentType: docType, filename: filename });
    var config = {
        method: 'post',
        url: url,
        headers: { 
            'Content-Type': 'multipart/form-data;boundary=' + data.getBoundary(),
            'Authorization': `Bearer ${token}`,
            ...data.getHeaders()
        },
        data : data,
        maxContentLength: Infinity,
        maxBodyLength: Infinity
    };
    const response = await axios(config);
    return response;
}

exports.delete2 = async function (resource, bearer, context) {  
  
  const url = `${process.env.ACTIONSTEP_API_ENDPOINT}/${resource}`;
  
  const options = {
    headers: {
      'Authorization': `Bearer ${bearer}`,
      'Content-Type': 'application/vnd.api+json',
      'Accept': 'application/vnd.api+json',
    }
  };  

  try {
    // context.log("[callApi].delete debug resource >", resource);
    console.log("[callApi].delete debug resource >", resource);
    const response = await axios.delete(url, options);
    if (response && response.data){      
        const xresponse = JSON.stringify(response.data);
        // context.log(`[callApi].delete debug >>`, JSON.stringify(response.data));
        console.log(`[callApi].delete debug >>`, JSON.stringify(response.data));
        
        if ((xresponse).indexOf('Access Denied') !=-1){
            throw new Error('Access Denied response from Actionstep');
        }
        return response.data || {};
    }
    return response || {};

  } catch (err) {
      const errMsg = (err && err.response && err.response.data) ? err.response.data : err.message || err;
      const errMsgJson = {'CustomizedError' : errMsg};
      // context.log.error("[callApi].delete error >>", JSON.stringify(errMsgJson));
      console.log("[callApi].delete error >>", JSON.stringify(errMsgJson));

      throw Error(JSON.stringify(errMsgJson));
  }
};