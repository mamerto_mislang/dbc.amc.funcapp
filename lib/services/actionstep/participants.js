const common = require ('../common/constant');
const actionstep = require('.');
const { formatLine02, testJSON } = require('../common/utils');

const getLawyersAssignment = function (state, context)
{
    const LQAssigned = process.env.DEFAULT_LAWYERS || '';
    if (LQAssigned){
        const lqBuf = Buffer.from(LQAssigned, 'base64').toString('utf-8');
        const validJson = testJSON(JSON.stringify(lqBuf));
        if (validJson){
            const lqJson = JSON.parse(lqBuf);
            const lqId = lqJson[state];
            context.log(`[getLawyersAssignment] DEBUG LAWYERS_CONFIG lqId:${lqId} >`, lqJson );
            return lqId;
        }else{
            context.log(`[getLawyersAssignment] DEBUG INVALID LAWYERS_CONFIG >`, JSON.stringify(lqBuf));
        }
    }
    return false;
}

const newKonektaUpdater = async function(participants, matterId, deal, token, context)
{
    const state = `${deal.state}`;
    const lawyerId = getLawyersAssignment(state, context) || common.ASSIGNED_LAWYERS[state];

    for (let i = 0; i < participants.length; i++) {
        const el = participants[i];
        const pType = el.typeId;
        const PRINCIPAL_SOLICITOR = 160;
        const ASSIGNED_LAWYER = 169;
        let participantId;

        if (pType === PRINCIPAL_SOLICITOR){
            participantId = el.default;
        }else if (pType === ASSIGNED_LAWYER){
            participantId = lawyerId;
        }else if (el.default > 0 && el.excluded === ''){
            participantId = el.default;
        }

        if (participantId){
            const xdata = {
                actionparticipants : {
                    links : {
                        action : matterId,
                        participantType : el.typeId,
                        participant : participantId
                    }
                }
            }
    
            context.log(`[matter::${matterId}].newKonektaUpdater actionparticipants > `, JSON.stringify(xdata));
            const response = (await actionstep.updateMatterParticipant(xdata, token, context));
            context.log(`[matter::${matterId}].newKonektaUpdater updateMatterParticipant >> `, response.actionparticipants);
        }
    }
}

const oldKonektaUpdater = async function(participants, matterId, deal, token, context)
{
    const principalLawyer = common.PRINCIPAL_LAWYERS[deal.state] || 513;
    const sro = common.STATE_REVENUE_OFFICE[deal.state] || 1377;

    for (let i = 0; i < participants.length; i++) {
        const el = participants[i];
        const pType = el.typeId;
    
        let participantId;

        if (el.default > 0 ){
            participantId = el.default;
        }else{
            if (pType ===68 || pType ===128){
                participantId = (pType ===68) ? principalLawyer : sro;
            }else{
                continue;
            }            
        }

        if (participantId){
            const xdata = {
                actionparticipants : {
                    links : {
                        action : matterId,
                        participantType : el.typeId,
                        participant : participantId
                    }
                }
            }
    
            context.log(`[matter::${matterId}].oldKonektaUpdater actionparticipants > `, JSON.stringify(xdata));
            const response = (await actionstep.updateMatterParticipant(xdata, token, context));
            context.log(`[matter::${matterId}].oldKonektaUpdater updateMatterParticipant >> `, response.actionparticipants);
        }
    }
}


exports.updateMatter = async function(matterId, deal, token, context)
{
    
    const newWorkflow = (deal.matterTypeId > 66) ?  true : false;
    const participantsData = (newWorkflow) ? common.PARTICIPANTS : common.PARTICIPANTS_OLD;

    const participants = participantsData.filter(function(row) {
        return row.status === 1;
    });

    if (newWorkflow){
        await newKonektaUpdater(participants, matterId, deal, token, context);
    }else{
        await oldKonektaUpdater(participants, matterId, deal, token, context);
    }    
}

exports.createPropertyAddressContact = async function (pData, person, token, context)
{
    const formattedAddress = formatLine02(pData);

    const tData = {
        companyName             : formattedAddress.companyName,
        physicalAddressLine1    : formattedAddress.line1,
        physicalCity            : (pData.suburb) ? pData.suburb : '',
        physicalStateProvince   : pData.state ? pData.state : '',
        physicalPostCode        : pData.post_code ? pData.post_code : '',
        physicalCountry         : 'Australia',
        phone1Number            : person.phone ? person.phone : '',
        email                   : person.email ? person.email : ''
    };
    
    const propertyContact = {
        participants : {
            isCompany : 'T',
            companyName             : tData.companyName,
            physicalAddressLine1    : tData.physicalAddressLine1,
            physicalCity            : tData.physicalCity,
            physicalStateProvince   : tData.physicalStateProvince,
            physicalPostCode        : tData.physicalPostCode,
            physicalCountry         : tData.physicalCountry,
            mailingAddressLine1     : tData.physicalAddressLine1,
            mailingCity             : tData.physicalCity,
            mailingStateProvince    : tData.physicalStateProvince,
            mailingPostCode         : tData.physicalPostCode,
            mailingCountry          : tData.physicalCountry, 
            phone1Label             : (tData.phone1Number && (((tData.phone1Number).trim()).substr(0,2)==='04')) ? 'Mobile' : 'Business',
            phone1Number            : tData.phone1Number,
            email                   : tData.email
        }
    };

    const response = (await actionstep.newParticipant(propertyContact, token, context));
    console.log("DEBUG newParticipant", JSON.stringify(response.participants));

    if (response.participants && response.participants.id){
        return response.participants.id;
    }
    return false;
}

exports.createSMSGateway = async function (token, context)
{
    const tData = {
        companyName          : 'SMS Gateway',
        email                : 'SMS@Konekta.com.au',
        physicalAddressLine1 : '',
        physicalCity         : '',
        physicalStateProvince: '',
        physicalPostCode     : '',
        physicalCountry      : 'Australia'
    };

    const propertyContact = {
        participants : {
            isCompany : 'T',
            companyName             : tData.companyName,
            physicalAddressLine1    : tData.physicalAddressLine1,
            physicalCity            : tData.physicalCity,
            physicalStateProvince   : tData.physicalStateProvince,
            physicalPostCode        : tData.physicalPostCode,
            physicalCountry         : tData.physicalCountry,
            mailingAddressLine1     : tData.physicalAddressLine1,
            mailingCity             : tData.physicalCity,
            mailingStateProvince    : tData.physicalStateProvince,
            mailingPostCode         : tData.physicalPostCode,
            mailingCountry          : tData.physicalCountry        
        }
    };

    const response = (await actionstep.newParticipant(propertyContact, token, context));
    console.log("DEBUG createSMSGateway", JSON.stringify(response));

    if (response.participants && response.participants.id){
        return response.participants.id;
    }
    return false;
}
