const actionstep = require('.');
const offers = require('../pipedrive/offers');
const dataCollectionIds = require('./schema/id');
const {fx, fxs, fxSelected, capitalize ,stringUp, fillUpName} = require('../common/utils');
const {logIt2} = require('../common/logger');
const fa = require('../common/share/loglevel');
const pdHelper = require('../pipedrive/paramHelpers');
const pxcollect  = require("../templates/ocr/processCollection");

const getMatterCollectionIds = async  function(matterId, token, context)
{
    const res = await actionstep.matterCollection(matterId, token, '');
    const listCollection = res.datacollectionrecordvalues || [];
    let cid = [];

    for (let i = 0; i < listCollection.length; i++) {
        const el  = listCollection[i];        
        const pos = (el.id).indexOf('--');
        if (pos !=-1){
            const collectionId = (el.id).substring(0, pos);
            if(collectionId){
                cid.push(collectionId);
            }            
        }
    }

    var uSet = new Set(cid);
    return [...uSet];
}

const postNewMatterCollection = async function (matterId, matterCollectionFieldId, token, context)
{
    try {
        const param = {
            datacollectionrecords : {
                links : {
                    action : matterId,
                    dataCollection : matterCollectionFieldId
                }
            }
        }
        const response = await actionstep.newCollection(param, token, context);
        return response;    
    } catch (err) {
        const errMsg   = err.message || 'error';
        logIt2(fa.error, context, `[matterId::${matterId}].postNewMatterCollection DEBUG Error >`, [errMsg]);
    }    
};

const pipedriveCollectionSource = function(deal)
{
    const pdBilling = {
        AdditionalInfo  : (deal.NewFieldMatterType)         ? deal.NewFieldMatterType       : deal.additionalInfo,
        DiscountOffered : (deal.discountOffered)            ? deal.discountOffered          : false,
        FixedFee        : (deal.fixedFee)                   ? deal.fixedFee                 : false,
        OfferApplied    : (deal.offerApplied)               ? offers[deal.offerApplied]     : false,
        ConveyType      : (deal.NewFieldConveyancingType)   ? deal.NewFieldConveyancingType : deal.conveyanceType
    }
    return pdBilling;
}

const getCollectionRecord = async function(matterId, collectionIds, colFields = [], token)
{
    const collectionFields = colFields || [];
    let collectionRecords  = [];
    const response = await actionstep.matterCollection(matterId, token);
    
    (response.datacollectionrecordvalues || []).forEach(el => {
        const row       = (el.id||'').split('--');
        const cfieldId  = (row[1]||'');
        
        if (row.length === 3 && cfieldId.length > 0 ){
            if (el.links.dataCollection == collectionIds.whattobill                                                 ||
               (el.links.dataCollection == collectionIds.keydates           && collectionFields.includes(cfieldId)) ||
               (el.links.dataCollection == collectionIds.transactiondetails && collectionFields.includes(cfieldId)) ||
               (el.links.dataCollection == collectionIds.propertydetails    && collectionFields.includes(cfieldId)) ||
               (el.links.dataCollection == collectionIds.propertyaddress))
            {
                collectionRecords.push(el);
            }
        }
    });
    return collectionRecords;
}

const createNewMatterCollection = async function(matterId, matterCollectionIds, token, context, ocrReview = false)
{
   
    if (!ocrReview){
        await postNewMatterCollection(matterId, matterCollectionIds.keydates, token, context);
        await postNewMatterCollection(matterId, matterCollectionIds.whattobill, token, context);
        await postNewMatterCollection(matterId, matterCollectionIds.propertydetails, token, context);
        await postNewMatterCollection(matterId, matterCollectionIds.propertyaddress, token, context);      
        await postNewMatterCollection(matterId, matterCollectionIds.transactiondetails, token, context);
    }else{
        const collectionListId = await getMatterCollectionIds(matterId, token, context);

        if (!collectionListId.includes(matterCollectionIds.keydates)){
            await postNewMatterCollection(matterId, matterCollectionIds.keydates, token, context);
        }
        if (!collectionListId.includes(matterCollectionIds.whattobill)){
            await postNewMatterCollection(matterId, matterCollectionIds.whattobill, token, context);
        }
        if (!collectionListId.includes(matterCollectionIds.propertydetails)){
            await postNewMatterCollection(matterId, matterCollectionIds.propertydetails, token, context);
        }
        if (!collectionListId.includes(matterCollectionIds.transactiondetails)){
            await postNewMatterCollection(matterId, matterCollectionIds.transactiondetails, token, context);
        }
    }
}

const prepareAddressCollection = function(address)
{
    const addr = {
        Postcode     : fx(address.post_code),    
        State        : fx(address.state),
        StreetName   : fxs(address.street_name, 'up'),
        StreetNumber : fx(address.street_no),
        StreetType   : fxs(address.street_type, 'up'),
        Suburb       : fxs(address.suburb, 'up'),
        UnitNumber   : fx(address.unit_no),
        PropertyType : fxs(address.property_type, 'up')
    }
    return addr;
}

const getCollectionRecordData = function(index, collectionData= {})
{
    const asData = collectionData[index] || '';
    return `${asData}`;
}

const updateMatterDataCollection = async function(matterId, deal, address, ocrResult, token, context)
{
    const state = deal.state;
    let extendedAS;
    let pdAddressCollection;

    const matterCollectionIds = {
        whattobill          : dataCollectionIds['whattobill'][state],
        keydates            : dataCollectionIds['keydates'][state],
        transactiondetails  : dataCollectionIds['transactiondetails'][state],
        propertydetails     : dataCollectionIds['propertydetails'][state],
        propertyaddress     : dataCollectionIds['propertyaddress'][state]
    };

    logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection address/matterCollectionIds >`, [JSON.stringify(address), JSON.stringify(matterCollectionIds)]);
    
    if (address && Object.keys(address).length > 0){
        pdAddressCollection = prepareAddressCollection(address);        
        logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection DEBUG pdAddressCollection >`, [JSON.stringify(pdAddressCollection)]);
    }

    const pipedriveCollection = pipedriveCollectionSource(deal);        
    logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection pipedriveCollection >> `, [JSON.stringify(pipedriveCollection)]);

    if (ocrResult.ocrExtended){
        extendedAS = await newProcessCollection(deal, ocrResult, token, context);
    }    
    logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection extendedAS >> `, [JSON.stringify(extendedAS)]);

    const sourceData = {...pdAddressCollection, ...pipedriveCollection, ...extendedAS};

    (await createNewMatterCollection(matterId, matterCollectionIds, token, context, ocrResult.ocrReview));
    const response = await getCollectionRecord(matterId, matterCollectionIds, Object.keys(sourceData), token) || [];
    
    logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection @sourceData >> `, [JSON.stringify(sourceData)]);

    if (response){        
        for (let i = 0; i < response.length; i++) {
            const el        = response[i];           
            const keyField  = (el.id).split('--');
            const index     = keyField[1];
            const sourceDataField = getCollectionRecordData(index, sourceData);               
            if ((sourceDataField||'').length > 0) {
                const postCollectionRecord = {
                    datacollectionrecordvalues : {
                        stringValue : sourceDataField,
                        links : {
                            action : matterId,
                            dataCollectionField  : el.links.dataCollectionField,
                            dataCollectionRecord : el.links.dataCollectionRecord,
                            dataCollection       : el.links.dataCollection
                        }
                    }
                };
                                
                logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection postCollectionRecord > `, [JSON.stringify(postCollectionRecord)]);
                const res = await actionstep.updateCollection(el.id, postCollectionRecord, token);                
                logIt2(fa.info, context, `[deal::${deal.dealId}].updateMatterDataCollection postCollectionRecord > `, [JSON.stringify(res.datacollectionrecordvalues)]);
            }            
        }
    }
}

const searchCTSName = async function(fullName, token, context)
{
    const personX = fillUpName(fullName);
    const person = {
        isCompany: 'F',
        companyName: '',       
        email: '',
        ...personX
    };    

    const personParam = pdHelper.createClientParam(person);
    const res = await actionstep.searchClient(personParam, token, context);
    if (res){
         let ctsPerson = pdHelper.getClientCredential(res, true);
         if (ctsPerson && ctsPerson.clientId){
            return ctsPerson.clientId;
         }
    }
    return false;
}

const newProcessCollection = async function(deal, ocrResult, token, context)
{
    const state         = deal.state;
    const extOcr        = ocrResult.ocrExtended;    
    const extOcrLen     = Object.keys(extOcr).length;
    let ocrCollectID    = pxcollect.previewASCollection(extOcr, state, context);

    logIt2(fa.info, context, `[newProcessCollection] debug extOcr/ocrCollectID:`, [JSON.stringify(extOcr), JSON.stringify(ocrCollectID)]);        

    if (extOcrLen < 1){
        return false;
    }
    let ctsId;
    const ctsName = pxcollect.getASCollectionById(ocrCollectID, state, 'ctsnam', context);
    if (token){
        ctsId = await searchCTSName(ctsName, token, context) || '';
    }else{
        ctsId = fxs(ctsName);
    }
    
    //fixed value in actionstep
    ocrCollectID['ctsnam'] = ctsId;
    ocrCollectID['kreceived'] = 'Yes - Signed Contract';
    
    if (deal){
        ocrCollectID['ConveyType'] = deal.NewFieldConveyancingType || deal.conveyanceType;
    }
    
    if (state == 'QLD'){
        // ocrCollectID['uncondate'] = ocrCollectID['findate']||'';
        const spcLen = (ocrCollectID['spc1det']||'').length;
        logIt2(fa.info, context, `[newProcessCollection] debug spcLen:${spcLen}`);   

        if ( spcLen > 154 ){
            const spCond = (ocrCollectID['spc1det']||'');

            ocrCollectID['spc1det'] = spCond.substring(0, 154);
            // ocrCollectID['spc2det'] = spCond.substring(155, 314);            
            // if (spcLen > 314){
            //     ocrCollectID['spc3det'] = spCond.substring(315, 479);
            // }            
            // if (spcLen > 479){
            //     ocrCollectID['spc4det'] = spCond.substring(480);
            // }            
        }        
    }else if (state == 'VIC'){
        if (ocrCollectID.findate){
            ocrCollectID['uncondate'] = ocrCollectID['findate'];
        }        
    }
        
    logIt2(fa.info, context, `[newProcessCollection] debug ocrCollectID:`, [JSON.stringify(ocrCollectID)]);   
    return ocrCollectID;
}

module.exports = {
    getMatterCollectionIds          : getMatterCollectionIds,
    newProcessCollection            : newProcessCollection,
    updateMatterDataCollection      : updateMatterDataCollection
}
