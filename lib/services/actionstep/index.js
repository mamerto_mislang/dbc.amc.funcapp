const { stringUp } = require('../common/utils');
const  {delete2, post, put, upload, get2, post2 } = require('./callApi');

exports.createMatter = async function (xdata, token, context) 
{
   const response = await post2('actioncreate', xdata, token, context);
   return response;
}

exports.updateMatter = async function (matterId, xdata, token, context) 
{
  const response = await put(`actions/${matterId}`, xdata, token, context);
  return response;
}

exports.linkMatterParticipant = async function (xdata, token, context) 
{
  const response = await post2('actionparticipants', xdata, token, context);
  return response;
}

exports.matterParticipants = async function (matterId, token, context) 
{
  const response = await get2(`actionparticipants/?action=${matterId}&include=action,participant`, token, context);
  return response;
}

exports.matterDetails = async function (matterId, token, context) 
{
  const response = await get2(`actions?id=${matterId}&include=action,participant`, token, context);
  return response;
}

exports.updateMatterParticipant = async function (xdata, token, context) 
{
  const response = await post2('actionparticipants', xdata, token, context);
  return response;
}

exports.searchClient = async function (xClient, token, context)
{
    let resource;
    const email = xClient.email;
    const xcompany = xClient.companyName || xClient.company;

    if (email && email.length > 1 ){
          const email = encodeURIComponent(xClient.email);
          resource = `participants?email=${email}`;
    }else{
          if (xClient.isCompany){
                if (!xcompany) return false;
                const companyName = encodeURIComponent(stringUp(xcompany));
                resource = `participants?companyName=${companyName}`;              
          }else{
                if (!xClient.firstName || !xClient.lastName){
                    return false;
                }
                resource = `participants?firstName=${xClient.firstName}&lastName=${xClient.lastName}&middleName=${xClient.middleName}`;
          }
    } 
    context.log("[searchClient](~) resource > ", resource);
    const response = await get2(resource, token, context);
    return response;
}

exports.searchCouncil = async function (xClient, token, context)
{
    const xcompany = xClient.companyName || xClient.company;
    if (!xClient.isCompany || !xcompany){
        context.log("[searchCouncil](~) xClient empty > ", xClient);
        return false;
    }

    const companyName = encodeURIComponent(stringUp(xcompany));
    const resource = `participants?companyName=${companyName}`;              
    
    context.log("[searchCouncil](~) resource > ", resource);
    const response = await get2(resource, token, context);
    return response;
}

exports.newParticipant = async function (xdata, token, context) 
{
  const response = await post2('participants', xdata, token, context);
  return response;
}

exports.newFileNotes = async function (xdata, token, context) 
{
  const response = await post2('filenotes', xdata, token, context);
  return response;
}

exports.matterCollection = async function (matterId, token, context) 
{
  const response = await get2(`datacollectionrecordvalues?action=${matterId}&pageSize=200`, token, context);
  return response;
}

exports.newCollection = async function (xdata, token, context) 
{
  const response = await post2('datacollectionrecords', xdata, token, context);
  return response;
}

exports.updateCollection = async function (fieldId, xdata, token, context) 
{
  const resource = `datacollectionrecordvalues/${fieldId}`; 
  const response = await put(resource, xdata, token, context);
  return response;
}

exports.createClient = async function(xdata, token, context)
{
    const response = await post2('participants', xdata, token, context);
    return response;
}

exports.uploadDocument = async function(docX, payload, token, context)
{
    const response = await upload('files', docX.filename, payload, token, docX.docTye, context);
    return response;
}

exports.linkDocumentToMatter = async function (matterId, actionstepFileId, filename, token, context)
{
    const xparam = {
        actiondocuments : {
            displayName : `${filename}`,
            file : `${actionstepFileId};${filename}`,
            links : {
                action : `${matterId}`
                //folder : ``
            }
        }
    };
    const response = await post('actiondocuments', xparam, token);
    return response;
}


exports.TestErrorMatterDetails = async function (matterId, token, context) 
{
  const response = await get2(`actions?id=${matterId}&include=action,participant`, token, context);
  return response;
}

exports.deleteParticipant = async function (matterId, partiTypeId, participantId, token, context) 
{
  const response = await delete2(`actionparticipants/${matterId}--${partiTypeId}--${participantId}`, token, context);
  return response;
}

exports.deleteParticipantByString = async function (participantSource, token, context) 
{
  const response = await delete2(`actionparticipants/${participantSource}`, token, context);
  return response;
}

exports.testMatterDetails = async function (matterId, token) 
{
  const response = await get2(`actions?id=${matterId}`, token);
  return response;
}
