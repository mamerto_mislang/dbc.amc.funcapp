const { fixAddress, reFixAddress, simplifyAddress } = require('../common/helper/address');
const {fx, fxFormat, cleanAddress, fxPhoneLabel, fxPhoneOption, fillUpName, formatLine} = require('../common/utils');
const {logIt2} = require('../common/logger');
const fa = require('../common/share/loglevel');
const common = require('../common/constant');

exports.createClientFromPD = function(deal, person)
{
    const isCompany = person.isCompany || 'F'
    const line1     = formatLine(person);

    const params = {
        participants : {
                isCompany             : isCompany,
                companyName           : (isCompany == 'T') ? person.name : '',
                salutation            : '',
                firstName             : fx(person.firstName, person.testFirstName),
                middleName            : fx(person.testMiddleName),
                lastName              : fx(person.lastName, person.testLastName),
                suffix                : '',
                preferredName         : '',
                physicalAddressLine1  : cleanAddress(line1),
                physicalCity          : fx(person.suburb),
                physicalStateProvince : fx(person.state, deal.state),
                physicalPostCode      : fx(person.post_code),
                physicalCountry       : 'Australia',
                mailingAddressLine1   : cleanAddress(line1),
                mailingCity           : fx(person.suburb),
                mailingStateProvince  : fx(person.state, deal.state),
                mailingPostCode       : fx(person.post_code),
                phone1Number          : fx(person.phone),
                phone1Label           : fxPhoneLabel(person.phone),
                email                 : fx(person.email)
        }
    };
    return params;
}

exports.createParticipantFromOCR = function(ocr, dealState, participantType, context)
{
    logIt2(fa.info, context, `[=============  createParticipantFromOCR =================] DEBUG participant-ocr [${participantType}]`, [JSON.stringify(ocr)]);

    const person   = fillUpName(ocr.name);
    const postCode =  ocr.post_code || ocr.postcode;
    let state      = (ocr.state || dealState || '').trim().toUpperCase();

    if (state.length > 1 && !common.STATES.includes(state)){
        state = dealState;
    }

    let extension    = (state && postCode)  ? `${state}, ${postCode}`  : state || postCode;
    extension        = (ocr.suburb && extension) ? `${ocr.suburb}, ${extension}` : extension || ocr.suburb;
    
    const baseAddr = { unit_street_no : '', street_name : '', street_type : '', suburb : '', state : '', post_code : ''};
    let addrObj    = (ocr.address) ? fixAddress(ocr.address, extension, state, ocr) : baseAddr;
    let unitStreetNo = streetName = '';
    unitStreetNo     = addrObj.unit_street_no;
    streetName       = addrObj.street_name;

    logIt2(fa.info, context, `DEBUG *1 `, [extension, addrObj]);

    if(!unitStreetNo && !streetName){
        const addrObj2 = (ocr.address) ? fixAddress(ocr.address, '', state, ocr) : baseAddr;
        unitStreetNo = addrObj2.unit_street_no;
        streetName   = addrObj2.street_name;        
        logIt2(fa.info, context, `DEBUG *2 `, [addrObj2]);

        if (!unitStreetNo && !streetName && addrObj2.suburb){
            const ust = reFixAddress(ocr.address, addrObj2.suburb);
            unitStreetNo = ust.unitStreetNo || '';
            streetName = ust.streetName || '';
            logIt2(fa.info, context, `DEBUG *3 `, [ust]);       
                
        }else if (!unitStreetNo && !streetName){
                const ust2   = simplifyAddress(ocr.address, state);
                logIt2(fa.info, context, `DEBUG *4 `, [ust2]);

                unitStreetNo = ust2.unit_street_no || '';
                streetName   = ust2.street_name || '';
                addrObj.post_code = addrObj.post_code || ust2.post_code;
                addrObj.suburb    = addrObj.suburb || ust2.suburb;
                addrObj.state     = addrObj.state || ust2.state;            
        }
    }

    let line1 = (streetName && unitStreetNo)  ? `${unitStreetNo} ${streetName}` :  (streetName ? streetName : ((unitStreetNo) ? unitStreetNo : ''));
    const line2 = (ocr.address2) ? ocr.address2 : '';
    
    if (addrObj['raw_line'] && addrObj['raw_line'].length > 3){
        line1 = addrObj['raw_line'];
        logIt2(fa.info, context, `DEBUG raw_line *2 `, [line1]);
    }

    const params = {
        participants : {
                isCompany             : (ocr.isCompany) ? 'T' : 'F',
                companyName           : (ocr.isCompany) ? fx(ocr.name) : '',
                salutation            : '',
                firstName             : fxFormat(person.firstName),
                middleName            : fxFormat(person.middleName),
                lastName              : fxFormat(person.lastName),
                suffix                : '',
                preferredName         : '',
                physicalAddressLine1  : cleanAddress(line1),
                physicalAddressLine2  : cleanAddress(line2),
                physicalCity          : fx(addrObj.suburb || ocr.suburb),
                physicalStateProvince : fx(state || addrObj.state),
                physicalPostCode      : fx(addrObj.post_code || ocr.postcode),
                physicalCountry       : 'Australia',
                mailingAddressLine1   : cleanAddress(line1),
                mailingAddressLine2   : cleanAddress(line2), 
                mailingCity           : fx(addrObj.suburb || ocr.suburb),
                mailingStateProvince  : fx(state || addrObj.state),
                mailingPostCode       : fx(addrObj.post_code || ocr.postcode),
                phone1Number          : fx(ocr.phone),
                phone1Label           : fxPhoneLabel(ocr.phone),
                phone2Number          : fxPhoneOption(ocr.option_mobile, ocr.phone),
                phone2Label           : fxPhoneLabel(ocr.option_mobile),
                email                 : fx(ocr.email)
        }
    };
    
    if (params.participants.isCompany === 'T' && params.participants.companyName){
        return params;
    }else if (params.participants.isCompany === 'F' &&  params.participants.firstName && params.participants.lastName){
        return params;
    }
    logIt2(fa.info, context, `[createParticipantFromOCR] Invalid OCR [participantType : ${participantType}] , information not sufficient!`, [params]);
    return false;
}
