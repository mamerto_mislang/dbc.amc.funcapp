const dataCollectionIds = {
    'whattobill' : {
        QLD : '337',
        VIC : '346',
        NSW : '343',
        SA  : '351',
        TAS : '360',
        WA  : '367'
    },
    'keydates' : {
        QLD : '336',
        VIC : '349',
        NSW : '342',
        SA  : '354',
        TAS : '363',
        WA  : '370'
    },
    'transactiondetails' : {
        QLD : '338',
        VIC : '347',
        NSW : '345',
        SA  : '352',
        TAS : '361',
        WA  : '368'
    },
    'propertydetails' : {
        QLD : '339',
        VIC : '348',
        NSW : '341',
        SA  : '353',
        TAS : '362',
        WA  : '369'
    },
    'propertyaddress' : {
        QLD : '359',
        VIC : '377',
        NSW : '375',
        SA  : '357',
        TAS : '366',
        WA  : '373'
    }
};

module.exports = Object.freeze(dataCollectionIds);