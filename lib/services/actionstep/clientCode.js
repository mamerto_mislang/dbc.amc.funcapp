const { get } = require('./callApi');
const actionstep = require('.');
const common =require('../common/constant');
const {logIt2, logIt} = require('../common/logger');
const fa = require('../common/share/loglevel');

const nextPage = async function (clientCode, page, token) {
  // const resource = `actions?pageSize=200&page=${page}&name_ilike=%${clientCode}%`;
  const resource = `actions?pageSize=200&page=${page}&name_ilike=*${clientCode}*`;    
  logIt(fa.debug, `[nextPage] resource >`, [resource]);
  const response = await get(resource, token);
  return response;
};

const validMatterName = function (matterName,cliCode)
{
    const xcliCode = `-${cliCode}`;
    const idx = (matterName||'').lastIndexOf(xcliCode);
    //console.log("DEBUG validMatterName", xcliCode, idx);

    return (idx === -1) ? false : true;
}

const checkIfExistClientInMatter = async function(matterId, clientId, token)
{
    const res = await actionstep.matterParticipants(matterId, token,'tx');
    const matterParticipants = res.actionparticipants || [];

    const mClient = matterParticipants.find((row) => {
        return  (`${clientId}` === row.links.participant && 
                (`${common.AS_CLIENT_TYPE}` === row.links.participantType || 
                `${common.AS_CLIENT_PRIMARY_TYPE}` === row.links.participantType)) ? row : null;
    });
    //console.log("DEBUG mClient", mClient);
    return (mClient===undefined || mClient==null) ? false : true;
    // for (let i = 0; i < matterParticipants.length; i++) {

    // }
}

const clientMattersCorrectButLongLogic = async function (clientId, clientCode, matterData, rc, token) {
  let xClientMatters = [];

  if (rc==1){
      xClientMatters.push({matterId: matterData.id, matterName: matterData.name});
      return xClientMatters;
  }
  for (let i = 0; i < matterData.length; i++) {    
      if (validMatterName(matterData[i].name, clientCode)){
        const response = await checkIfExistClientInMatter(matterData[i].id, clientId, token);
          if (response){
              xClientMatters.push({matterId: matterData[i].id, matterName: matterData[i].name});
          }
      }
      //console.log("matterDta => ", matterData[i].id, matterData[i].name);      
  }  
  return xClientMatters || [];
};


const clientMatters = async function (clientId, clientCode, matterData, rc) {
  let xClientMatters = [];

  if (rc==1){
      xClientMatters.push({matterId: matterData.id, matterName: matterData.name});
      return xClientMatters;
  }
  for (let i = 0; i < matterData.length; i++) {
      const participants = matterData[i].links.primaryParticipants;
      // console.log("DEBUG clientMatters", matterData[i]);
      if (validMatterName(matterData[i].name, clientCode)){

        const index  = (matterData[i].name).lastIndexOf(clientCode);
        const rCode1 = (matterData[i].name).substring(index + 3, index + 5) - 0;
        const rCode2 = (matterData[i].name).substring(index + 3, index + 6);
        const rCode  = (rCode2.lastIndexOf('-') > 0 ) ? rCode1 : (rCode2 - 0);
        
        //Logic applies if client is the primary participant(100%)
        /*
        let participant;
        const arLen = participants.length;
        if (participants !=null && arLen > 0){
              participant = participants[0];
              if (clientId == participant || (arLen > 1 && clientId == participant[1])){ 
                  xClientMatters.push({matterId: matterData[i].id, matterName: matterData[i].name, reference: cCode});                  
              }
        }
        */

        //Logic applies to all related matters regardless if clientid is in the matter(50%)
        xClientMatters.push({matterId: matterData[i].id, matterName: matterData[i].name, reference: rCode});
        // console.log("matterDta => ", matterData[i].id, matterData[i].name);
      }  
  }  
  return xClientMatters || [];
};

const latestClientMatter = function (arrData) 
{  
  logIt(fa.debug, `[latestClientMatter](+) debug arrData`, [JSON.stringify(arrData)]);
  const response =  (arrData || []).reduce((acc, item) => acc = acc.reference > item.reference ? acc : item, 0);
  return response;
};

const latestClientCode =  function(xdata){
    
    if (!xdata.matterName) throw new Error('Matter Name does not exist!');

    const matterNameArr = xdata.matterName.split('-');
    if (matterNameArr.length && 
      matterNameArr.length > 0 ){
        const cliCode = matterNameArr[matterNameArr.length - 2];
        let _suffixCode  = Number((cliCode||'').substring(3)) + 1;
        _suffixCode     = _suffixCode+'';
        const suffixCode = _suffixCode.padStart(2, '0')
        const defaCliCode =  (cliCode||'').substring(0,3);
        logIt(fa.debug, `[latestClientCode] defaCliCode|suffixCode ${defaCliCode}${suffixCode}`);
        return `${defaCliCode}${suffixCode}`;
    }
    throw new Error('Matter Name is empty!');
};

const getNewCode = function(cliCode)
{
    let _suffixCode  = Number((cliCode||'').substring(3)) + 1;
    _suffixCode     = _suffixCode+'';
    const suffixCode = _suffixCode.padStart(2, '0')
    const defaCliCode =  (cliCode||'').substring(0,3);
    return `${defaCliCode}${suffixCode}`;
}

const latestClientCode2 =  function(xdata){
    
  if (!xdata.matterName || !xdata.matterId) throw new Error('Matter ID/Name does not exist!');

  const m  = xdata.matterName;
  let indexPos1 = m.indexOf(xdata.matterId);
  if (indexPos1!=-1){
      indexPos1--;
      const m2 = m.substring(0, indexPos1);
      if(m2){
          let indexPos2 = m2.lastIndexOf('-');
          if (indexPos2 !=-1){
            indexPos2++;
            const m3 = m2.substring(indexPos2, indexPos1);
            return (m3) ? getNewCode(m3) : false;
          }          
      }
  }
  return false;
};

exports.getClientCodeMatterName = async function (clientId, clientCode, token, context) {
    let page = 0;
    let isNextPage = 1;
    let associatedMatters = [];
    let clientCodeHint =`${clientCode}01`;
    // console.log(`getClientCodeMatterName:cientId ${clientId}`);
    
    try {
          while (isNextPage){
            page++;
            const response = await nextPage(clientCode, page, token);
        
            if (response.actions){
                isNextPage = response.meta.paging.actions.nextPage;
                const rc = response.meta.paging.actions.recordCount;
                const res = await clientMatters(clientId, clientCode, response.actions, rc);
                associatedMatters.push(...res);
            }else{
                isNextPage = 0;
            }
          }
          
          if ((associatedMatters || []).length> 0 ){              
              const latestMatter = latestClientMatter(associatedMatters);
              clientCodeHint = latestClientCode2(latestMatter);
              if (!clientCodeHint){
                  clientCodeHint = latestClientCode(latestMatter);
              }
              logIt2(fa.debug, context, `[getClientCodeMatterName](~) latestMatter clientCodeHint:${clientCodeHint} debug > `, [latestMatter]);
          }
    } catch (error) {
        logIt2(fa.error, context, `[getClientCodeMatterName](-) latestMatter error > `, [error, error.msg]);
    }
    finally{      
      if ((clientCodeHint || '').lastIndexOf(clientCode) ==-1){
          logIt2(fa.error, context, `[getClientCodeMatterName](-) latestMatter fixing clientCode > ${clientCode}02`);
          return `${clientCode}02`;
      }
      return clientCodeHint;
    }   
}

