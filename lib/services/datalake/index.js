
const {post} = require('./callApi');
const {logIt2} = require('../common/logger');
const fa = require('../common/share/loglevel');


exports.updateMatterPipedrive = async function(matterId, dealId, context)
{
    const xdata = {
        DealID : dealId,
        MatterID : matterId
    };
    const response = await post('data/update-matter-id', xdata, 'MatterUpdate', context);
    logIt2(fa.info, context, `[DataLake::updateMatterPipedrive](~) dealId::${dealId} matterId: ${matterId} response >>`, [response]);
    return response;
}
