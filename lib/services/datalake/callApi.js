const axios = require('axios');

exports.post = async function (resource, xdata, xdatasource = 'MatterUpdate', ctx) {
  const url = `${process.env.DATALAKE_API_ENDPOINT}/${resource}`;
  const options = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': '*/*',
      'X-Data-Source':  xdatasource,
      'X-Api-Key': `${process.env.DATALAKE_API_KEY}`,
    }
  };

  const response = await axios.post(url, xdata, options);
  return response.data || {};
};
