const { jsonBusParams } = require("../common/helpers");
const { sendMessage } = require("../servicebus");
const { randomUUID } = require('crypto');

exports.send2BusQueue = async function(dealId, matterId, status, msg, context, errMsg='', errStack='', topic='notification-service')
{
    const message = `${msg}<br><br>**** <i>AMC SYSTEM GENERATED</i> ****`
    const dev_testing_env = process.env.DEV_TESTING_ENV || 'disable';

    const body = {
        messageId  : `${randomUUID()}`,
        deal_id    : dealId,
        matter_id  : matterId,
        status     : status,
        message    : message,
        errorMsg   : errMsg,
        errorStack : errStack
    };

    const payload = jsonBusParams(body, context);
    await sendMessage(payload, topic, context);
    // if (dev_testing_env == 'enable'){
    //     context.log(`[deal::${dealId}](send2BusQueue) Sending to BusQueue`, body);
    // }else{
    //     await sendMessage(payload, topic, context);
    // }    
    
}

exports.send2BusQueueV2 = async function(stepId=0, dealId, matterId, status, msg, context, errMsg='', errStack='', mc_info = '', topic='notification-service-v2')
{
    const message = `${msg}<br><br>**** <i>AMC SYSTEM GENERATED</i> ****`

    const body = {
        messageId  : `${randomUUID()}`,
        deal_id    : dealId,
        matter_id  : matterId,
        status     : status,
        message    : message,
        errorMsg   : errMsg,
        errorStack : errStack,
        step_id    : stepId,
        mc_info    : mc_info
    };

    const payload = jsonBusParams(body, context);
    await sendMessage(payload, topic, context);
    // context.log(`[deal::${dealId}](send2BusQueue) Sending to BusQueue`, body);
}

exports.send2AMCErrorBus = async function(dealId, matterId, status, msg, context, errMsg='', errStack='', topic='notification-service-v2')
{
    const message = `${msg}<br><br>**** <i>AMC SYSTEM GENERATED</i> ****`

    const body = {
        messageId  : `${randomUUID()}`,
        deal_id    : dealId,
        matter_id  : matterId,
        status     : status,
        message    : message,
        errorMsg   : errMsg,
        errorStack : errStack
    };

    const payload = jsonBusParams(body, context);
    await sendMessage(payload, topic, context);
    // context.log(`[deal::${dealId}](send2BusQueue) Sending to BusQueue`, body);
}