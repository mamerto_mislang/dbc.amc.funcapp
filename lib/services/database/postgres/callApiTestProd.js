const pg = require('pg');

let process = {
    env : {
        AMC_DB_ENDPOINT : "dbc-pi-postgresql-prod-01.postgres.database.azure.com",
        AMC_DB_USER : "mattercreation@dbc-pi-postgresql-prod-01",
        AMC_DB_PASSWORD : "avdvV7lsUmw",
        AMC_DB_NAME : "amc_forms"
    }
}

exports.initDBConnectionProd = async function()
{
    const config = {
        host: `${process.env.AMC_DB_ENDPOINT}`,
        user: `${process.env.AMC_DB_USER}`,
        password: `${process.env.AMC_DB_PASSWORD}`,
        database: `${process.env.AMC_DB_NAME}`,
        port: 5432,
        ssl: true
    };
    const pool = new pg.Pool(config);

    const client = await pool.connect();
    console.log("DEBUG pool.idleCount, pool.totalCount, pool.waitingCount: ", pool.idleCount, pool.totalCount, pool.waitingCount)        
    return client;
}

exports.disconnectDBProd = async function(client)
{
    await client.release();
    console.log("<< Disconnect DB >>");
}

exports.queryProd = async function(client, sql)
{
    const result = await client.query(sql);
    return result;
}

// const queryDatabase = async function()
// {
//     try {
//         const querySpec = 'SELECT * FROM field_mapping;';    
//         const pool = new pg.Pool(config);         
//         const client = await pool.connect();      
//         const result = await client.query(querySpec);   
//         client.release();
//         console.log("Done =>");
//         // Return the query resuls back to the caller as JSON
//         // context.res = {
//         //     status: 200,
//         //     isRaw: true,
//         //     body: result.rows,
//         //     headers: {
//         //         'Content-Type': 'application/json'
//         //     }
//         // };

//     } catch (err) {
//         console.log(err.message);
//     }
// }
// queryDatabase();