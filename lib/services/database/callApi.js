const pg = require('pg');

// let process = {
//     env : {
//         AMC_DB_ENDPOINT : "dbc-pi-postgresql-01.postgres.database.azure.com",
//         AMC_DB_USER : "amcdevelopment@dbc-pi-postgresql-01",
//         AMC_DB_PASSWORD : "FkPvf5o8pJLs",
//         AMC_DB_NAME : "amc_forms_dev"
//     }
// }

exports.initDBConnection = async function()
{
    const config = {
        host: `${process.env.AMC_DB_ENDPOINT}`,
        user: `${process.env.AMC_DB_USER}`,
        password: `${process.env.AMC_DB_PASSWORD}`,
        database: `${process.env.AMC_DB_NAME}`,
        port: 5432,
        ssl: true
    };
    const pool = new pg.Pool(config);

    const client = await pool.connect();
    // console.log("DEBUG pool.idleCount, pool.totalCount, pool.waitingCount: ", pool.idleCount, pool.totalCount, pool.waitingCount)        
    return client;
}

exports.disconnectDB = async function(client)
{
    await client.release();
}

exports.query = async function(client, sql)
{
    const result = await client.query(sql);
    return result;
}