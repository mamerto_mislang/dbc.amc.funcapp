const { matterName } = require('../common/helpers');
const db = require('./callApi');

exports.createPoolConnection = async function()
{
    const client = await db.initDBConnection();
    return client;
}

exports.releasePoolConnection = async function(client)
{
    await db.disconnectDB(client);
}

exports.createDailyMatter = async function(clientPool, matterId, matterName)
{
    const res = await clientPool.query(
        `INSERT INTO "daily_matter" ("matter_id", "matter_name")  
         VALUES ($1, $2) RETURNING id;`, [matterId, matterName]);
    const newId = (res && res.rows[0].id) ? res.rows[0].id : false;
    return newId;
}

exports.checkDeal = async function(clientPool, dealId)
{
    if (dealId === undefined || dealId==null){
        throw Error("Deal Id is empty!");
    }
    const sql = `select count(*) as cnt from web_forms where deal_id=${dealId} and active=true`;
    const res = await clientPool.query(sql);
    if (!res.rows || !res.rows[0].cnt){
        throw Error("Error encountered in database!");
    }
    return (parseInt(res.rows[0].cnt) > 0 ) ? true : false; 
}

exports.checkDealMatterPending = async function(clientPool, dealId)
{
    const sql = `select count(*) as cnt from matters where deal_id=${dealId} and active=true`;
    const res = await clientPool.query(sql);
    if (!res.rows || !res.rows[0].cnt){
        throw Error("Error encountered in database!");
    }
    return (parseInt(res.rows[0].cnt) > 0 ) ? true : false; 
}

exports.checkMatterContractUpdates = async function(clientPool, matterId)
{
    const sql = `select deal_id,matter_type,matter_name from matters where matter_id=${matterId} and active=true`;
    const res = await clientPool.query(sql);
 
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    
    const matterType = parseInt(res.rows[0].matter_type);
    const dealId = res.rows[0].deal_id; 
    const matterName = res.rows[0].matter_name;

    return {dealId: dealId, matterType: matterType, matterName: matterName};
}


exports.checkDealLastStage = async function(clientPool, dealId)
{
    const sql = `select stage_id, mc_info, matter_id from matters where deal_id=${dealId} and active=true`;
    const res = await clientPool.query(sql);
    
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    const mcInfo  = res.rows[0].mc_info;
    const stageId = parseInt(res.rows[0].stage_id);
    const matterId = res.rows[0].matter_id; 
    const mc = mcInfo || {};
    return {...mc, stageId: stageId, dbMatterId: matterId};
}

exports.createPendingMatter = async function(clientPool, data)
{
    const res = await clientPool.query(
        `INSERT INTO "matters" ("deal_id", "matter_id", "matter_name", "matter_type", "pipedrive_done")  
         VALUES ($1, $2, $3, $4, $5) RETURNING id;`, [data.dealId, data.matterId, data.matterName, data.matterType, data.pipedriveDone]);
    const newId = (res && res.rows[0].id) ? res.rows[0].id : false;
    return newId;
}

exports.updatePendingMatter = async function(clientPool, data)
{
    // const sql = `UPDATE matters SET matter_id=${d.matterId}, matter_name='${d.matterName}', matter_type=${d.matterType}, pipedrive_done = TRUE WHERE deal_id = ${d.dealId};`;
    // console.log("DEBUG sql => ", sql)
    const res = await clientPool.query(
        `UPDATE "matters" SET "matter_id"=$1, "matter_name"=$2, "matter_type"=$3, "pipedrive_done" = TRUE, "actionstep_done" = TRUE, stage_id=10, updated_at = now() WHERE "deal_id" = $4 and active=true;`, [data.matterId, data.matterName, data.matterType, data.dealId]);
    return res;

    // const res = await clientPool.query(sql);
    // return res;
}

exports.createDealPendingMatter = async function(clientPool, dealId)
{
    const res = await clientPool.query(
        `INSERT INTO "matters" ("deal_id")  
         VALUES ($1) RETURNING id;`, [dealId]);
    const newId = (res && res.rows[0].id) ? res.rows[0].id : false;
    return newId;
}

exports.createDealPendingMatterPayload = async function(clientPool, dealId, payload = '')
{
    const res = await clientPool.query(
        `INSERT INTO "matters" ("deal_id", "mc_source")  
         VALUES ($1, $2) RETURNING id;`, [dealId, payload]);
    const newId = (res && res.rows[0].id) ? res.rows[0].id : false;
    return newId;
}

exports.createDocumentMap = async function(clientPool, dealId, document, url)
{
    dealId = dealId || 0;
    const res = await clientPool.query(
        `INSERT INTO "document_mapping" ("deal_id", "document", "url")
         VALUES ($1, $2, $3) RETURNING id;`, [dealId, document, url]);
    const newId = (res && res.rows[0].id) ? res.rows[0].id : false;
    return newId;
}

exports.retrieveOcrDB = async function (clientPool, dealId, oid=0, context)
{
    let sql;
    if (oid > 1){
        sql = `select a.id,a.ocr_fields,a.form_fields,a.subject, TO_CHAR(b.created_at, 'DD-MM-YYYY') as mcdate,a.ocr_update,file_ref from web_forms a left join matters b on (a.deal_id=b.deal_id) 
        where a.deal_id=${dealId} and a.active=true and a.id=${oid} order by a.id desc limit 1`;
    }else{
        sql = `select a.id,a.ocr_fields,a.form_fields,a.subject, TO_CHAR(b.created_at, 'DD-MM-YYYY') as mcdate,a.ocr_update,file_ref from web_forms a left join matters b on (a.deal_id=b.deal_id)
        where a.deal_id=${dealId} and a.active=true and ((length(a.ocr_fields::text) > 2 and a.subject not in ('PropertyTransferForm', 'ContractDraftingForm')) or (length(a.form_fields::text) > 2 and a.subject in ('PropertyTransferForm', 'ContractDraftingForm'))) order by a.id desc limit 1`;
    }    

    context.log(`[dealId:${dealId} retrieveOcrDB](+) oid:${oid} sql:${sql}`);
    const res = await clientPool.query(sql);
    context.log(`[dealId:${dealId} retrieveOcrDB](~) result:`, res.rowCount);

    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    // context.log(res.rows);
    return res.rows[0];
}


exports.getMapDocumentURL = async function (clientPool, fileIds, context)
{
    const sql = `select url from document_mapping where id in (${fileIds}) order by id`;
    const res = await clientPool.query(sql);
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    return res.rows;
}

exports.getDailyMatterCount = async function (clientPool)
{
    const sql = `select count(*) as cnt from daily_matter`;
    
    const res = await clientPool.query(sql);
    if (res.rowCount === 0) return false;
    return res.rows[0].cnt;
}

exports.resetPendingMatter = async function(clientPool)
{
    const res = await clientPool.query(`TRUNCATE TABLE daily_matter RESTART IDENTITY;`);
    return res;
}

exports.updateStepInPendingMatter = async function(clientPool, dealId, stepId=0, mcInfo='')
{
    const mc = JSON.parse(mcInfo);
    const matterId     = mc.matterId
    const matterName   = mc.matterName;
    const matterTypeId = mc.matterTypeId;
    console.log("[updateStepInPendingMatter] DEBUG mc", mc);

    const res = await clientPool.query(`UPDATE "matters" SET stage_id=$1,mc_info=$2, matter_id=$3, matter_name=$4, matter_type=$5, updated_at = now() WHERE deal_id=$6;`, [stepId, mcInfo, matterId, matterName, matterTypeId, dealId ] );
    return res;
}

exports.getMatterSource = async function(clientPool, dealId)
{
    const sql = `select mc_source, matter_id,matter_name,matter_type  from matters where deal_id=${dealId} and active=true and (length(mc_source::text) > 3)`;
    const res = await clientPool.query(sql);
    
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }

    const mcSource   = res.rows[0].mc_source || {};
    const matterId   = res.rows[0].matter_id || '';
    const matterName = res.rows[0].matter_name || '';
    const matterType = res.rows[0].matter_type || '';
    
    const matter =    {"matterId": matterId, "matterName": matterName, "matterType" :  matterType};
    return {...mcSource, matter };
}

exports.dbMatterDetails = async function(clientPool, matterId)
{
    const sql = `select deal_id,matter_type,mc_source from matters where matter_id=${matterId} and active=true`;
    const res = await clientPool.query(sql);
 
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    
    const mcSource   = res.rows[0].mc_source;
    const matterType = parseInt(res.rows[0].matter_type);
    const dealId     = res.rows[0].deal_id; 
    return {dealId: dealId, matterType: matterType, mcSource: ((mcSource||'').length > 2) ? true : false };
}

exports.retrieveOcrDBResult = async function (clientPool, dealId, context)
{
    const sql = `select id,ocr_fields,form_fields,subject, TO_CHAR(created_at, 'DD-MM-YYYY') as mcdate from web_forms 
    where deal_id=${dealId} and active=true and ((length(ocr_fields::text) > 2 and subject not in ('PropertyTransferForm', 'ContractDraftingForm')) or (length(form_fields::text) > 2 and subject in ('PropertyTransferForm', 'ContractDraftingForm'))) order by id desc limit 1`;

    const res = await clientPool.query(sql);
    if (res.rowCount === 0) return false;

    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    // context.log(res.rows);
    return res.rows[0];
}

exports.getOcrDBResult = async function (clientPool, dealId, context)
{
    const sql = `select id,ocr_fields,form_fields,subject, TO_CHAR(created_at, 'DD-MM-YYYY') as mcdate from web_forms where deal_id=${dealId} and active=true 
        	and ((length(ocr_fields::text) > 2 and subject not in ('PropertyTransferForm', 'ContractDraftingForm')) or (length(form_fields::text) > 2 and subject in ('PropertyTransferForm', 'ContractDraftingForm'))) order by id desc limit 1`;

    const res = await clientPool.query(sql);
    context.log(`[dealId: ${dealId}](~) getOcrDBResult rowCount: `, res.rowCount);

    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    return res.rows[0];
}

exports.getColumnsMappingDescription = async function (clientPool, context)
{
    let sql = `select web_form_column, description from field_mapping order by id asc`;
    const res = await clientPool.query(sql);
    context.log(`[getColumnsMappingDescription](~) rowCount: `, res.rowCount);
    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    return res.rows || [];
}

exports.getOcrFields = async function(clientPool, state, context)
{
    const sql = `select a.id, a.web_form_column,a.description, c.description as category, a.order_no, a.ocr_field_type, a.ocr_field_hint from field_mapping a right join field_mapping_state b on (a.id = b.field_mapping_id) 
    inner join category c on (a.category_id=c.id) where a.category_id > 0 and b.state = '${state}' and b.deleted = false order by a.order_no asc`;
    
    // context.log(`[getOcrFields](~) DEBUG sql: `, sql);
    const res = await clientPool.query(sql); 
    context.log(`[getOcrFields](~) rowCount: `, res.rowCount);

    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }    
    return res;
}

exports.getOcrUpdateData = async function (clientPool, matterId, hash, context)
{    
    const sql  = `SELECT a.id,a.deal_id,a.subject,a.state,a.ocr_fields,a.form_fields,a.ocr_update,TO_CHAR(coalesce(b.created_at,a.created_at), 'DD-MM-YYYY') as mcdate,a.form_fields->>'ctype' as ctype,a.ocr_status,a.file_ref,a.form_fields->>'matterTypeName' as matterTypeName from web_forms a left join matters b ON (a.deal_id=b.deal_id) where a.subject = 'Signed Contract Updates' and a.form_fields->>'hash' = '${hash}' and a.form_fields->>'matterId' = '${matterId}' order by a.id desc limit 1`;
    //const sql2 = `SELECT id,deal_id,subject,state,ocr_fields,form_fields,ocr_update,TO_CHAR(created_at, 'DD-MM-YYYY') as mcdate,form_fields->>'ctype' as ctype,ocr_status,file_ref,form_fields->>'matterTypeName' as matterTypeName from web_forms where subject = 'Signed Contract Updates' and form_fields->>'hash' = '${hash}' and form_fields->>'matterId' = '${matterId}' order by id desc limit 1`;
    const res = await clientPool.query(sql);
    context.log(`[matterId: ${matterId}](~) getOcrUpdateData rowCount: `, res.rowCount);

    if (res.rowCount === 0) return false;
    if (!res.rows){
        throw Error("Error encountered in database!");
    }
    return res.rows[0];
}

exports.postOcrUpdates = async function (clientPool, matterId, oid, ocrDataUpdates, context)
{    
    const fileRef = ocrDataUpdates["file_reference"] || '';
    context.log(`[matterId: ${matterId} postOcrUpdates](~) objectId: ${oid} fileref: ${fileRef}`);
    const res = await clientPool.query(`UPDATE web_forms SET ocr_update=$1,file_ref=$2,updated_at = now() WHERE active=true and id=$3;`, [ocrDataUpdates, fileRef, oid]);
    context.log(`[[matterId: ${matterId} postOcrUpdates](-) rowCount: `, res.rowCount);
    return res;
}

exports.updateFileRef = async function(clientPool, matterId, oid, fileRef, context)
{
    context.log(`[matterId: ${matterId} updateFileRef](~) webFormId: ${oid} fileref: ${fileRef}`);
    const res = await clientPool.query(`UPDATE web_forms SET file_ref=$1,updated_at = now() WHERE active=true and id=$2;`, [fileRef, oid]);
    context.log(`[[matterId: ${matterId} updateFileRef](-) rowCount: `, res.rowCount);
}
