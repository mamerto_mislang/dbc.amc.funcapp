const {execFastDBCalcu, query} = require('./callapi');

exports.createFastMatter = async function(m = {}, context = undefined)
{
    const sql = `insert into fast_pending_matters(id, matter_id, file_note, matter_type_id, matter_name,assignedto_id, client_id1, status) 
    values(${m.id}, ${m.matter_id}, '${m.file_note}', ${m.matter_type_id}, '${m.matter_name}', ${m.assignedto_id}, ${m.client_id1}, ${m.status})`;
    await execFastDBCalcu(sql, context);
}

exports.getMatterFromFAST = async function(matterId, context)
{
    const sql = `select id, matter_id,matter_type_id,matter_name from fast_pending_matters where matter_id=${matterId} order by id DESC limit 1`;
    const res = await query(sql, context) || [];
    return res[0];
}
