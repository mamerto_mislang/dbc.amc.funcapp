const mysql = require("mysql2/promise");

// let process = {
//     env : {
//         "STMT_CALCU_DATABASE" : "apps_staging",
//         "STMT_CALCU_HOST" : "staging.crxdanqzoljb.ap-southeast-2.rds.amazonaws.com",
//         "STMT_CALCU_PASSWORD" : "jRYF1aM244teDrqBQpVq",
//         "STMT_CALCU_USERNAME" : "admin_staging"
//     }    
// }

exports.execFastDBCalcu = async function(sql, context) 
{
    try {
        const con = await mysql.createConnection({
            host     : process.env.STMT_CALCU_HOST,
            database : process.env.STMT_CALCU_DATABASE,
            user     : process.env.STMT_CALCU_USERNAME,
            password : process.env.STMT_CALCU_PASSWORD,
            "port": 3306            
        });        
        const response = await con.query(sql);
        context.log("debug execFastDBCalcu", response);
        await con.end();
    }
    catch(ex)
    {
        console.error(ex)
    }

}

exports.query = async function(sql, context) 
{
    const con = await mysql.createConnection({
        host     : process.env.STMT_CALCU_HOST,
        database : process.env.STMT_CALCU_DATABASE,
        user     : process.env.STMT_CALCU_USERNAME,
        password : process.env.STMT_CALCU_PASSWORD,
        "port": 3306            
    });        
    const response = await con.query(sql);
    // context.log("debug query", response);
    await con.end();
    return response;
}