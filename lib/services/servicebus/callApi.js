
const { ServiceBusClient } = require('@azure/service-bus');

exports.send = async function (messages, topicName, context) 
{
  const correlationId = context.invocationId;
  const sbClient = new ServiceBusClient(`${process.env.AZURE_SERVICEBUS_CONNECTION}`);
  const sender = sbClient.createSender(topicName);
  const messagesToSend = messages || [];

  try {
    let batch = await sender.createMessageBatch();
    for (let i = 0; i < messagesToSend.length; i++) {
      const message = messagesToSend[i];
      const appProps = message.applicationProperties || {};
      context.log.info(`[${correlationId}] Sending ${appProps.source} ${appProps.type} Message: ${JSON.stringify(message)}`);

      if (!batch.tryAddMessage(message)) {
            await sender.sendMessages(batch);
            batch = await sender.createMessageBatch();
            if (!batch.tryAddMessage(message)) {
                throw new Error('Message too big to fit in a batch');
            }
      }
    }

    await sender.sendMessages(batch);
  } finally {
    sbClient.close();
  }
};

exports.sendMsgId = async function (messages, topicName, msgId, context) 
{
  const correlationId = context.invocationId;
  const sbClient = new ServiceBusClient(`${process.env.AZURE_SERVICEBUS_CONNECTION}`);
  const sender = sbClient.createSender(topicName);
  const messagesToSend = messages || [];

  try {
    let batch = await sender.createMessageBatch();
    for (let i = 0; i < messagesToSend.length; i++) {
      let message = messagesToSend[i];
      message.messageId = msgId;
      const appProps = message.applicationProperties || {};
      context.log.info(`[${correlationId}] Sending ${appProps.source} ${appProps.type} Message: ${JSON.stringify(message)}`);

      if (!batch.tryAddMessage(message)) {
            await sender.sendMessages(batch);
            batch = await sender.createMessageBatch();
            if (!batch.tryAddMessage(message)) {
                throw new Error('Message too big to fit in a batch');
            }
      }
    }

    await sender.sendMessages(batch);
  } finally {
    sbClient.close();
  }
};
