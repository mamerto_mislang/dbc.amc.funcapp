
const svcbus = require('./callApi');

exports.sendMessage = async function (messages, topicName, context) 
{
    await svcbus.send(messages, topicName, context);
};

exports.sendMessageId = async function (messages, topicName, msgId, context) 
{
    await svcbus.sendMsgId(messages, topicName, msgId, context);
};
