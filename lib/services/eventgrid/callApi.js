const axios = require('axios');

exports.sendEventTopic = async function (xdata, enableOcr=true) {
  const url = (enableOcr) ? `${process.env.AZURE_EVENT_GRID_ENDPOINT}` : `${process.env.AZURE_EVENT_GRID_ENDPOINT_NON_OCR}`;
  const options = {
    headers: {
      'aeg-sas-key': (enableOcr) ? process.env.AZURE_EVENT_GRID_SAS_KEY : process.env.AZURE_EVENT_GRID_SAS_KEY_NON_OCR,
      'Content-Type': 'application/json',
    }
  };

  const response = await axios.post(url, xdata, options);
  return response.data || {};
};

exports.sendContractUpdatesEventTopic = async function (xdata) {
  const url = `${process.env.AZURE_CONTACTUPDATES_EVENT_GRID_ENDPOINT}`;
  const options = {
    headers: {
      'aeg-sas-key': process.env.AZURE_CONTACTUPDATES_EVENT_GRID_SAS_KEY,
      'Content-Type': 'application/json',
    }
  };

  const response = await axios.post(url, xdata, options);
  return response.data || {};
};
