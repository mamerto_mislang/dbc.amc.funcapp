const teams = require('./callApi');

const prepareData = function(mData)
{
    let xrow = []; 
    xrow.push({ name: 'Matter Created with Ref:', value : `${mData.matterName}`});
    xrow.push({ name: 'Client:', value : `${mData.clientName}`});
    xrow.push({ name: '', value : `${mData.matterTypeName}`});
    xrow.push({ name: 'Client Services:', value : `${mData.concierge}`});
    xrow.push({ name: 'Postcode:', value : `${mData.postcode}`});
    return xrow;
}

exports.notifyTeamsOfMatter = async function(xdata, matterCountToday, context)
{
    const notifData = prepareData(xdata);
    const response = await teams.post(notifData, matterCountToday);
    return response;
}

const prepareData2 = function(mData)
{
    let xrow = []; 
    xrow.push({ name: 'Matter Id:', value : `${mData.matterId}`});
    xrow.push({ name: 'Client:', value : `${mData.clientName}`});
    xrow.push({ name: '', value : `${mData.matterTypeName}`});
    return xrow;
}

exports.notifyTeamsPSCEM  = async function(xdata, context)
{
    const notifData = prepareData2(xdata);
    const response = await teams.postPSCEMSuccess(notifData);
    return response;
}

exports.notifyTeamsAMCError  = async function(xdata, context)
{
    let dataFacts = [];
    if(xdata.matterId){
        dataFacts.push({ name: 'Matter Id: ',  value : `${xdata.matterId}`});
    }
    if (xdata.dealId){
        dataFacts.push({ name: 'Deal ID: ',  value : `${xdata.dealId}`});
    }
    const errData = [
            {name: 'Error: ',  value : `${xdata.errorMsg}`},
            {name: 'Stack: ',  value : `${xdata.errorStack}`}
        ];
        
    dataFacts = dataFacts.concat(errData);

    const url = `${process.env.TEAMS_WEBHOOK_ENDPOINT}/${process.env.TEAMS_AMC_ERROR_PATH}`;
    const card = {
        "@type"       : "MessageCard",
        "@context"    : "https://schema.org/extensions",
        "themeColor"    : "0075FF",
        "summary"       : "AMC Error",
        "sections"      : [{
            "title"   : `**@AMC Error Details**`,
            "facts"   : dataFacts
        }]
    };

    const response = await teams.post2(card, url);
    return response;
}

exports.notifyTeamsPSCEMError  = async function(xdata, context)
{
    let dataFacts = [];
    if(xdata.matterId){
        dataFacts.push({ name: 'Matter Id: ',  value : `${xdata.matterId}`});
    }
    if (xdata.dealId){
        dataFacts.push({ name: 'Deal ID: ',  value : `${xdata.dealId}`});
    }
    const errData = [
            {name: 'Error: ',  value : `${xdata.errorMsg}`},
            {name: 'Stack: ',  value : `${xdata.errorStack}`}
        ];
        
    dataFacts = dataFacts.concat(errData);

    const url = `${process.env.TEAMS_WEBHOOK_ENDPOINT}/${process.env.TEAMS_PSCEM_ERROR_PATH}`;
    const card = {
        "@type"       : "MessageCard",
        "@context"    : "https://schema.org/extensions",
        "themeColor"    : "0075FF",
        "summary"       : "AMC Error in PSCEM'",
        "sections"      : [{
            "title"   : `**@PSCEM Error**`,
            "facts"   : dataFacts
        }]
    };

    const response = await teams.post2(card, url);
    return response;
}
