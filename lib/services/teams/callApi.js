'use strict';
const axios = require('axios');

const dataFormat = function(dataFacts, matterNoToday, summary = 'Automatic Matter Creation')
{
    return {
        "@type"       : "MessageCard",
        "@context"    : "https://schema.org/extensions",
        "themeColor"    : "0075FF",
        "summary"       : summary,
        "sections"      : [{
            "title"   : `**@Channel #${matterNoToday}**`,
            "facts"   : dataFacts
        }]
    };
}

exports.post = async function (aData, matterNoToday) 
{
    const url = `${process.env.TEAMS_WEBHOOK_ENDPOINT}/${process.env.TEAMS_WEBHOOK_PATH}`;
    const card = dataFormat(aData, matterNoToday);
    	
    const options = {
        headers: {
            'content-type' : 'application/vnd.microsoft.teams.card.o365connector',
            'content-length' : `${JSON.stringify(card).toString().length}`
        }
    };
    const response = await axios.post(url, card, options);
    return response.data || {};
};

const dataFormat2 = function(dataFacts, summary = 'PSCEM')
{
    return {
        "@type"       : "MessageCard",
        "@context"    : "https://schema.org/extensions",
        "themeColor"    : "0075FF",
        "summary"       : summary,
        "sections"      : [{
            "title"   : `**@Processed Signed Contract Document**`,
            "facts"   : dataFacts
        }]
    };
}

exports.postPSCEMSuccess = async function (aData) 
{
    const url = `${process.env.TEAMS_WEBHOOK_ENDPOINT}/${process.env.TEAMS_PSCEM_SUCCESS_PATH}`;
    const card = dataFormat2(aData);
    	
    const options = {
        headers: {
            'content-type' : 'application/vnd.microsoft.teams.card.o365connector',
            'content-length' : `${JSON.stringify(card).toString().length}`
        }
    };
    const response = await axios.post(url, card, options);
    return response.data || {};
};

exports.post2 = async function (card, url) 
{
    const options = {
        headers: {
            'content-type' : 'application/vnd.microsoft.teams.card.o365connector',
            'content-length' : `${JSON.stringify(card).toString().length}`
        }
    };
    const response = await axios.post(url, card, options);
    return response.data || {};
};