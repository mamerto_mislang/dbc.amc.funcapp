const { 
    stringUp, 
    scanNameWithCondition, 
    money, 
    fetchEmails, 
    filterOcr, 
    removeAllButNumbers, 
    removeAllNumbers, 
    fxSelected,
    getOcrValue,
    isoDate,
    customIsoDate,
    fxBool
} = require("../../common/utils");
const { isDuplicatePartiesEmail, isDuplicateOCRPartiesName} = require('../tools');
const {logIt, logIt2} = require('../../common/logger');
const fa = require('../../common/share/loglevel');
const { propertydetails } = require("../../actionstep/schema/id");
const asfields = require('../common/asmapping');
const { FindGoalResponse } = require("pipedrive");
const moment = require('moment');
const pxcollect  = require("./processCollection");

const extractDetails = function(details)
{
    if(details){
        const pers = (details.toLowerCase()).split(' of ');
        if (pers.length > 1){
              return {
                personName  : stringUp(pers[0]),
                address : stringUp(pers[1])
              }
        }
    }
    return false;
}

const stripXchars = function(val)
{
    const i1 = (val||'').toLowerCase().indexOf(' and');
    const i2 = (val||'').toLowerCase().indexOf(' nominee');
    if (i1!=-1 && i2!=-1){
          return (val||'').substring(0, i1);
    }
    return val||'';
}

exports.extractBuyer1 = function (ocr, context)
{
    const state    = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let peers;
    let address  = ocr.buyer1_address;
    
    if (['NSW','WA'].includes(state)){
        peers = scanNameWithCondition(ocr.buyer1_details || ocr.buyer_details, state);
        if (peers && (peers.address||'').length > 0){
          address = ocr.buyer1_address || peers.address;
        }        
    }else{
        peers = scanNameWithCondition(ocr.buyer1_name ||ocr.buyer_name, state);
    }

    let address2  = '';
    let buyerName = (peers && peers.person1Name) ? peers.person1Name : '';

    let emailList = fetchEmails(ocr.buyer1_email);
    let email     = (emailList.length) ? emailList[0] : ocr.buyer1_email;

    let extra;
    
    if ((peers && peers.person2Name ) && (emailList && emailList[1])){
        extra = {person: peers.person2Name, email: emailList[1]};
    }else if (peers && peers.person2Name){
        extra = {person: peers.person2Name};
    }

    if (state === 'WA'){
        email = (ocr.buyer_email) ? ocr.buyer_email || ocr.buyer1_email : email;
        if ((peers && peers.person2Name.length == 0) || (!peers)){
          const buyerObj = extractDetails(ocr.buyer1_details || ocr.buyer_details);
          if(buyerObj){
              buyerName = buyerObj.personName;
              address   = (ocr.buyer1_address) ? address : buyerObj.address || '';
          }
      }
    }else if (state === 'SA'){
      
        address  = address || ocr.buyer1_address2;
        address2 = (address && ocr.buyer1_address2) ? ocr.buyer1_address2 : '';

        ocr.buyer1_suburb   = ocr.buyer1_suburb   || ocr.buyer_suburb; 
        ocr.buyer1_state    = ocr.buyer1_state    || ocr.buyer_state;
        ocr.buyer1_postcode = ocr.buyer1_postcode || ocr.buyer_postcode;
    }

    const buyer = {
      isCompany       : false,
      name            : (buyerName) ? buyerName : ocr.buyer1_name,
      email           : email,
      phone           : ocr.buyer1_mobile || ocr.buyer1_phone,
      address         : filterOcr(address),
      address2        : filterOcr(address2),
      state           : ocr.buyer1_state,
      suburb          : ocr.buyer1_suburb,
      postcode        : ocr.buyer1_postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : (ocr.buyer1_mobile) ? (ocr.buyer1_phone || '') : '',
      option_contact  : '',
      option_extra    : extra,
      option_tag      : 'buyer1'
    }

    logIt2(fa.info, context, '[extractBuyer1] debug >', [JSON.stringify(buyer)]);
    return (ocr.buyer1_name || buyerName) ? buyer : false; 
}

exports.extractBuyer2 = function (ocr, context)
{
    const state    = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let xbuyer = {name:'', postcode: '', state: '', suburb: '', address: '', address2: '', email: ''};
    
    xbuyer.name      = ocr.buyer2_name;
    xbuyer.suburb    = ocr.buyer2_suburb;
    xbuyer.postcode  = ocr.buyer2_postcode;
    xbuyer.state     = ocr.buyer2_state;
    xbuyer.email     = ocr.buyer2_email;
    xbuyer.mobile    = ocr.buyer2_mobile || ocr.buyer2_phone;
    xbuyer.address   = ocr.buyer2_address;

    if (['QLD', 'SA'].includes(state)){
       if(state === 'SA'){
        xbuyer.name      = ocr.buyer2_name;
        if (!ocr.ocrReview){
            xbuyer.suburb    = ocr.buyer1_suburb;
            xbuyer.postcode  = ocr.buyer1_postcode;
            xbuyer.state     = ocr.buyer1_state;
            xbuyer.address   = ocr.buyer1_address;
            xbuyer.address2  = ocr.buyer1_address2;
            xbuyer.email     = ocr.buyer2_email;
            xbuyer.mobile    = ocr.buyer2_mobile || ocr.buyer2_phone;      
        }else{           
            xbuyer.suburb    = ocr.buyer2_suburb    || ocr.buyer1_suburb;
            xbuyer.postcode  = ocr.buyer2_postcode  || ocr.buyer1_postcode;
            xbuyer.state     = ocr.buyer2_state     || ocr.buyer1_state;
            xbuyer.address   = ocr.buyer2_address   || ocr.buyer1_address;
            xbuyer.address2  = ocr.buyer2_address2  || ocr.buyer1_address2;
            xbuyer.email     = ocr.buyer2_email;
            xbuyer.mobile    = ocr.buyer2_mobile    || ocr.buyer2_phone;      
        }
          
       }
       
       if(isDuplicatePartiesEmail(xbuyer.email, [ocr.buyer1_email])){
          xbuyer.email = '';
       }

       const buyer1Name = (ocr.buyer1_name || '').toLowerCase();
       if (isDuplicateOCRPartiesName(xbuyer.name, [buyer1Name], context )){
          xbuyer.name = '';
       }
    }

    const buyer = {
      isCompany       : false,
      name            : stripXchars(xbuyer.name),
      email           : xbuyer.email,
      phone           : xbuyer.mobile,
      address         : xbuyer.address,
      address2        : xbuyer.address2,
      state           : xbuyer.state,
      suburb          : xbuyer.suburb,
      postcode        : xbuyer.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : (ocr.buyer2_mobile) ? (ocr.buyer2_phone || '') : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'buyer2'
    }
    return (ocr.buyer2_name) ? buyer : false;
}

exports.extractBuyer3 = function (ocr, context)
{
    const state    = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let xbuyer = {name:'', postcode: '', state: '', suburb: '', address: '', address2: '', email: ''};
    
    if (ocr.ocrReview){
        xbuyer.name      = ocr.buyer3_name;
        xbuyer.email     = ocr.buyer3_email;
        xbuyer.address   = ocr.buyer3_address  ||ocr.buyer1_address;
        xbuyer.address2  = ocr.buyer3_address2 ||ocr.buyer1_address2;
        xbuyer.state     = ocr.buyer3_state    ||ocr.buyer1_state;
        xbuyer.postcode  = ocr.buyer3_postcode ||ocr.buyer1_postcode;
        xbuyer.suburb    = ocr.buyer3_suburb   ||ocr.buyer1_suburb;
    }

    if(state === 'SA' && !ocr.ocrReview){
        xbuyer.name      = ocr.buyer3_name;
        xbuyer.email     = ocr.buyer3_email;
        xbuyer.address   = ocr.buyer1_address;
        xbuyer.address2  = ocr.buyer1_address2;
        xbuyer.state     = ocr.buyer1_state;
        xbuyer.postcode  = ocr.buyer1_postcode;
        xbuyer.suburb    = ocr.buyer1_suburb;

        if (isDuplicatePartiesEmail(xbuyer.email, [ocr.buyer1_email, ocr.buyer2_email])){
            xbuyer.email = '';
        }
        
        const buyer1Name = (ocr.buyer1_name || '').toLowerCase();
        const buyer2Name = (ocr.buyer2_name || '').toLowerCase();
  
        if (isDuplicateOCRPartiesName(xbuyer.name, [buyer1Name, buyer2Name], context)){
           xbuyer.name = '';
        }
    }

    const buyer = {
      isCompany       : false,
      name            : stripXchars(xbuyer.name),
      email           : xbuyer.email,
      phone           : '',
      address         : xbuyer.address,
      address2        : xbuyer.address2,
      state           : xbuyer.state,
      suburb          : xbuyer.suburb,
      postcode        : xbuyer.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'buyer3'
    }
    return (ocr.buyer3_name) ? buyer : false; 
}

exports.extractBuyer4 = function (ocr, context)
{
    const state    = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let xbuyer = {name:'', postcode: '', state: '', address: '', address2 : '', email: '', suburb : ''};

    if (ocr.ocrReview){
        xbuyer.name      = ocr.buyer4_name;
        xbuyer.email     = ocr.buyer4_email;
        xbuyer.address   = ocr.buyer4_address;
        xbuyer.address2  = ocr.buyer4_address2;
        xbuyer.state     = ocr.buyer4_state;
        xbuyer.postcode  = ocr.buyer4_postcode;
        xbuyer.suburb    = ocr.buyer4_suburb;
    }

    if (state === 'SA'){
        xbuyer.name      = ocr.buyer4_name;
        xbuyer.email     = ocr.buyer4_email;

        if (!ocr.ocrReview){
            xbuyer.address   = ocr.buyer1_address;
            xbuyer.address2  = ocr.buyer1_address2;
            xbuyer.state     = ocr.buyer1_state;
            xbuyer.postcode  = ocr.buyer1_postcode;
            xbuyer.suburb    = ocr.buyer1_suburb;
        }else{
            xbuyer.address   = ocr.buyer4_address  || ocr.buyer1_address;
            xbuyer.address2  = ocr.buyer4_address2 || ocr.buyer1_address2;
            xbuyer.state     = ocr.buyer4_state    || ocr.buyer1_state;
            xbuyer.postcode  = ocr.buyer4_postcode || ocr.buyer1_postcode;
            xbuyer.suburb    = ocr.buyer4_suburb   || ocr.buyer1_suburb;
        }
      if(isDuplicatePartiesEmail(xbuyer.email, [ocr.buyer1_email, ocr.buyer2_email, ocr.buyer3_email])){
          xbuyer.email = '';
      }
      
      const buyer1Name = (ocr.buyer1_name || '').toLowerCase();
      const buyer2Name = (ocr.buyer2_name || '').toLowerCase();
      const buyer3Name = (ocr.buyer3_name || '').toLowerCase();

      if (isDuplicateOCRPartiesName(xbuyer.name, [buyer1Name, buyer2Name, buyer3Name], context)){
         xbuyer.name = '';
      }
    }


    const buyer = {
      isCompany       : false,
      name            : stripXchars(xbuyer.name),
      email           : xbuyer.email,
      phone           : '',
      address         : xbuyer.address,
      address2        : xbuyer.address2,
      state           : xbuyer.state,
      suburb          : xbuyer.suburb,
      postcode        : xbuyer.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'buyer4'
    }
    return (ocr.buyer4_name) ? buyer : false; 
}

exports.extractSeller1 = function(ocr)
{
    const state    = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let address    = ocr.seller1_address || ocr.seller1_address1 || ocr.seller_address;
    let peers      = scanNameWithCondition(ocr.seller1_name || ocr.seller_name, state);
    let sellerName = (peers && peers.person1Name) ? stringUp(peers.person1Name) : '';
    let emailList  = fetchEmails(ocr.seller1_email || ocr.seller_email);
    let email      = (emailList.length) ? emailList[0] : ocr.seller1_email;
    let extra;
    let address2   = '';

    if ((peers && peers.person2Name ) && (emailList && emailList[1])){
        extra = {person: peers.person2Name, email: emailList[1], address: address};
    }else if (peers && peers.person2Name){
        extra = {person: peers.person2Name, email:'', address: address};
    }
    
    if (state === 'WA'){
        email = (email) ? email : ocr.seller_email || ocr.seller1_email;
        peers  = scanNameWithCondition(ocr.seller_details || ocr.seller1_details, state);
        if (!peers){
            const bstObject = extractDetails(ocr.seller_details || ocr.seller1_details);
            if(bstObject){
                sellerName = bstObject.personName;
                address   = (address) ? address : bstObject.address || '';
            }
        }else{
            sellerName = (peers && peers.person1Name) ? stringUp(peers.person1Name) : '';
            if (!address){
                address = (peers && peers.address) ? peers.address : '';
            }
        }

    }else if (state === 'SA'){
      
        address  = address || ocr.seller1_address2;
        address2 = (address && ocr.seller1_address2) ? ocr.seller1_address2 : '';
        ocr.seller1_suburb   = ocr.seller1_suburb   || ocr.seller_suburb; 
        ocr.seller1_state    = ocr.seller1_state    || ocr.seller_state;
        ocr.seller1_postcode = ocr.seller1_postcode || ocr.seller_postcode;
    }

   const seller = {
      isCompany       : false,
      name            : (sellerName) ? sellerName : ocr.seller1_name || ocr.seller_name,	
      email           : email,	
      phone           : ocr.seller1_mobile || ocr.seller1_phone,
      address         : address,
      address2        : address2,
      state           : ocr.seller1_state,	
      suburb          : ocr.seller1_suburb,	
      postcode        : ocr.seller1_postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : (ocr.seller1_mobile) ? (ocr.seller1_phone || '') : '',
      option_contact  : '',
      option_extra    : extra,
      option_tag      : 'seller1'
   }
   return (ocr.seller1_name || ocr.seller_name || sellerName) ? seller : false; 
}

exports.extractSeller2 = function(ocr, context)
{
   const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
   let xseller = {name:'', suburb: '', postcode: '', state: '', email: '', mobile: '', address: '', address2: ''};

   xseller.name      = ocr.seller2_name;
   xseller.suburb    = ocr.seller2_suburb;
   xseller.postcode  = ocr.seller2_postcode;
   xseller.state     = ocr.seller2_state;
   xseller.email     = ocr.seller2_email;
   xseller.mobile    = ocr.seller2_mobile || ocr.seller2_phone;
   xseller.address   = ocr.seller2_address;

   if (state === 'QLD' || state === 'SA'){

      if (state === 'SA'){
        xseller.name      = ocr.seller2_name;
        xseller.email     = ocr.seller2_email;
        xseller.mobile    = ocr.seller2_mobile || ocr.seller2_phone;

        xseller.suburb    = ocr.seller1_suburb;
        xseller.postcode  = ocr.seller1_postcode;
        xseller.state     = ocr.seller1_state;        
        xseller.address   = ocr.seller1_address ||'';
        xseller.address2  = ocr.seller1_address2 ||'';
      }   

      if (ocr.seller1_email && isDuplicatePartiesEmail(xseller.email, [ocr.seller1_email])){
          xseller.email = '';
      }
      if (ocr.seller1_name && isDuplicateOCRPartiesName(xseller.name, [(ocr.seller1_name).toLowerCase()], context)){
          xseller.name = '';
      }
   }

   const seller = {
      isCompany       : false,
      name            : stripXchars(xseller.name),	
      email           : xseller.email,	
      phone           : xseller.mobile,	
      address         : xseller.address || '',
      address2        : xseller.address2 || '',
      state           : xseller.state,	
      suburb          : xseller.suburb,	
      postcode        : xseller.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : (ocr.seller2_mobile) ? (ocr.seller2_phone || '') : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'seller2'
   }
   return (ocr.seller2_name) ? seller : false; 
}

exports.extractSeller3 = function(ocr, context)
{
    const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let xseller = {name:'', postcode: '', state: '', suburb: '', address: '', address2: '', email: ''};
    
    if (state === 'SA'){
      xseller.name      = ocr.seller3_name;
      xseller.email     = ocr.seller3_email;
      xseller.address   = ocr.seller1_address;
      xseller.address2  = ocr.seller1_address2;
      xseller.state     = ocr.seller1_state;
      xseller.postcode  = ocr.seller1_postcode;
      xseller.suburb    = ocr.seller1_suburb;

      const seller1Name = (ocr.seller1_name || '').toLowerCase();
      const seller2Name = (ocr.seller2_name || '').toLowerCase();

      if (isDuplicateOCRPartiesName(xseller.name, [seller1Name, seller2Name], context)){
        xseller.name = '';
      }
      if (ocr.seller3_email && isDuplicatePartiesEmail(xseller.email, [ocr.seller1_email, ocr.seller2_email])){
        xseller.email = '';
      }
    }

   const seller = {
      isCompany       : false,
      name            : stripXchars(xseller.name),	
      email           : xseller.email || '',	
      phone           : '',	
      address         : xseller.address ||'',
      address2        : xseller.address2 ||'',
      state           : xseller.state,
      suburb          : xseller.suburb,
      postcode        : xseller.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'seller3'
   }
   logIt2(fa.debug, context, '[extractSeller3] debug >', [JSON.stringify(seller)]);
   return (ocr.seller3_name) ? seller : false; 
}

exports.extractSeller4 = function(ocr, context)
{
   const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
   let xseller = {name:'', postcode: '', state: '', suburb: '', address: '', address2: '', email: ''};

    if (state === 'SA'){
      xseller.name      = ocr.seller4_name;
      xseller.email     = ocr.seller4_email;
      xseller.address   = ocr.seller1_address;
      xseller.address2  = ocr.seller1_address2;
      xseller.state     = ocr.seller1_state;
      xseller.postcode  = ocr.seller1_postcode;
      xseller.suburb    = ocr.seller1_suburb;

      const seller1Name = (ocr.seller1_name || '').toLowerCase();
      const seller2Name = (ocr.seller2_name || '').toLowerCase();
      const seller3Name = (ocr.seller3_name || '').toLowerCase();

      if (isDuplicateOCRPartiesName(xseller.name, [seller1Name, seller2Name, seller3Name], context)){
          xseller.name = '';
      }
      if (ocr.seller4_email && isDuplicatePartiesEmail(xseller.email, [ocr.seller1_email, ocr.seller2_email, ocr.seller3_email])){
        xseller.email = '';
      }
    }

   const seller = {
      isCompany       : false,
      name            : stripXchars(xseller.name),
      email           : xseller.email,	
      phone           : '',
      address         : xseller.address ||'',
      address2        : xseller.address2 ||'',
      state           : xseller.state,
      suburb          : xseller.suburb,
      postcode        : xseller.postcode,
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'seller4'
   }

   logIt2(fa.debug, context, '[extractSeller4] debug >', [JSON.stringify(seller)]);
   return (ocr.seller4_name) ? seller : false; 
}

exports.extractAgentOnSell = function (ocr, context)
{  
    const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    let agent_address  = ocr.s_agent_address1;
    const postcode     = ocr.s_agent_postcode || ocr.agent_postcode;
    let agent_address2;

    
    if (state === 'SA'){
        ocr.s_agent_name    =  ocr.s_agent_ref    || ocr.s_agent_name  || ocr.agent_company;
        ocr.s_agent_email   =  ocr.s_agent_email  || ocr.agent_email;
        ocr.s_agent_mobile  =  ocr.s_agent_mobile || ocr.s_agent_phone || ocr.agent_phone;
        ocr.s_agent_state   =  ocr.s_agent_state  || ocr.agent_state;
        ocr.s_agent_suburb  =  ocr.s_agent_suburb || ocr.agent_suburb
        
        agent_address  = ocr.s_agent_address1;
        agent_address2 = (agent_address && ocr.s_agent_address2) ? ocr.s_agent_address2 : '';
    }

    const agent = {
      isCompany       : true,
      name            : ocr.s_agent_name,
      email           : ocr.s_agent_email,
      phone           : ocr.s_agent_mobile || ocr.s_agent_phone,
      address         : agent_address || ocr.s_agent_address,
      address2        : agent_address2 || '',
      state           : ocr.s_agent_state,
      suburb          : ocr.s_agent_suburb,
      postcode        : postcode || '',
      option_license  : ocr.s_agent_license,
      option_faxno    : ocr.s_agent_fax,
      option_abn      : ocr.s_agent_ABN,
      option_mobile   : ocr.s_agent_phone,
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'agentOnSell'
    }
    logIt2(fa.info, context, '[extractAgentOnSell] debug >', [JSON.stringify(agent)]);
    return (ocr.s_agent_name) ? agent : false; 
}

exports.extractAgentOnBuy = function (ocr)
{
    const agent = {
        isCompany       : true,
        name            : ocr.b_agent_name,
        email           : ocr.b_agent_email,
        phone           : ocr.b_agent_phone,
        address         : (ocr.b_agent_address1) ? ocr.b_agent_address1 : ocr.b_agent_address,
        address2        : '',
        state           : ocr.b_agent_state,
        suburb          : ocr.b_agent_suburb,
        postcode        : ocr.b_agent_postcode,
        option_license  : ocr.b_agent_license,
        option_faxno    : ocr.b_agent_fax,
        option_abn      : ocr.b_agent_ABN,
        option_mobile   : '',
        option_contact  : '',
        option_extra    : '',
        option_tag      : 'agentOnBuy'
      }
      return (ocr.b_agent_name) ? agent : false; 
}

exports.extractSolicitorOnSell = function (ocr)
{
    const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    if (state === 'TAS'){
        ocr.s_sol_name    = ocr.s_sol_name || ocr.s_sol_person;
        ocr.s_sol_address = ocr.s_sol_address || ocr.s_sol_address;
    }
 
    const solicitor = {
      isCompany       : true,
      name            : ocr.s_sol_name,
      email           : ocr.s_sol_email,
      phone           : ocr.s_sol_mobile || ocr.s_sol_phone,
      address         : ocr.s_sol_address,
      address2        : ocr.s_sol_address2 || '',
      state           : ocr.s_sol_state,
      suburb          : ocr.s_sol_suburb,
      postcode        : ocr.s_sol_postcode,
      option_license  : '',
      option_faxno    : ocr.s_sol_fax,
      option_abn      : '',
      option_mobile   : ocr.s_sol_phone,
      option_contact  : ocr.s_sol_contact,
      option_extra    : '',
      option_tag      : 'solicitorOnSell'
    }
    return (ocr.s_sol_name) ? solicitor : false; 
}

exports.extractSolicitorOnBuy = function (ocr)
{
    const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
    if (state === 'TAS'){
    //   ocr.b_sol_name    = ocr.b_sol_firm; //    || ocr.b_sol_name    || ocr.b_sol_person;
      ocr.b_sol_address = ocr.b_sol_address;
    }

    const solicitor = {
        isCompany       : true,
        name            : ocr.b_sol_name,
        email           : ocr.b_sol_email,
        phone           : ocr.b_sol_mobile || ocr.b_sol_phone,
        address         : ocr.b_sol_address,
        address2        : ocr.b_sol_address2 || '',
        state           : ocr.b_sol_state,
        suburb          : ocr.b_sol_suburb,
        postcode        : ocr.b_sol_postcode,
        option_license  : '',
        option_faxno    : ocr.b_sol_fax,
        option_abn      : '',
        option_mobile   : ocr.b_sol_phone,
        option_contact  : ocr.b_sol_contact,
        option_extra    : '',
        option_tag      : 'solicitorOnBuy'
      }
      return (ocr.b_sol_name) ? solicitor : false; 
}

exports.extractKeyDates = function(ocr)
{
    const keydates = {
        contract_date   : ocr.contract_date,
        settlement_date : ocr.settlement_date || ocr.settement_date
      }
      return (ocr.contract_date || ocr.settlement_date) ? keydates : false; 
}

exports.extractGenericData = function(ocr, context)
{
  let xcouncil;
  if (ocr.ocrReview){
    
    const response = {
        council : (ocr.property_council_area) ? ocr.property_council_area : false
    }
    logIt2(fa.info, context, '[extractGenericData] debug review >', [JSON.stringify(response)]);
    return response;

  }else{
    if (ocr.property_local_gov && ocr.property_council_area){
        if ((ocr.property_council_area).toLowerCase().lastIndexOf('residential') !=-1){
             xcouncil = ocr.property_local_gov;
        }
    }
  }
  
  const response = {
    council : (ocr.property_council_area) ?  (xcouncil || ocr.property_council_area) : ocr.property_local_gov || false 
  };
  
  logIt2(fa.info, context, '[extractGenericData] debug >', [JSON.stringify(response)]);
  return response;
}

exports.extractOCRMatterCollection = function(ocr, context)
{
    context.log("DEBUG extractOCRMatterCollection > ", ocr);
    const state = ocr.dealState || '';    
    const response = pxcollect.processCollectionFields(ocr, state, context) || {};
    console.log("DEBUG processCollectionFields >>", response);
    // pscem.previewASCollection(response);
    return {...response, dealState : state};
}
