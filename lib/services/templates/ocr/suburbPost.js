const suburbPostfixCollectionList = [
    'PARK',
    'HILLS',
    'HILL',
    'DAM',
    'GROVE',
    'CREEK',
    'BEACH',
    'ISLAND',
    'HALL',
    'RIDGES',
    'KING',
    'VALLEY',
    'VALE',
    'MOUNTAIN',
    'HEIGHTS',
    'WATERS',
    'RIDGE',
    'MILL',
    'MILE',
    'MOUNT',
    'NORTH',
    'CROSSING',
    'GARDENS',
    'FLAT',
    'PLAINS',
    'BAY',
    'REEF',
    'RIVER',
    'SOUTH',
    'BRIDGE',
    'WEST',
    'EAST',
    'VIEW',
    'LAKE',
    'ALBURY',
    'ROCK',
    'POINT',
    'FERRY',
    'SOUTHGATE',
    'GULLY',
    'ROCKS',
    'TOP',
    'BUILDINGS',
    'RIVERVIEW',
    'YARDS',
    'OAK',
    'DOCK',
    'FERRY',
    'CAMP',
    'MANGROVE',
    'LAGOON',
    'HAVEN',
    'JUNCTION',
    'LODGE',
    'PONDS',
    'COVE',
    'TOWN',
    'FOREST',
    'HEAD',
    'HEADS',
    'SPRINGS',
    'SANDS',
    'JUNCTION',
    'BROOK',
    'HARBOUR',
    'PLAIN',
    'RANGE',
    'VILLAGE',
    'DOWNS',
    'FARM',
    'FALLS',
    'ARM',
    'GULLY',
    'FORRESTFIELD'
];

module.exports = Object.freeze(suburbPostfixCollectionList);