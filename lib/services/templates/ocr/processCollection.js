const moment = require('moment');
const { 
    stringUp, 
    money, 
    removeAllButNumbers, 
    removeAllNumbers, 
    fxSelected,
    getOcrValue,
    dateRangeLogic,
    fxs, 
    fx,
    capitalize,
    isoDate,
    getISODate,
    customIsoDate,
    fxBool,
    isoDateFormat,
    filterNumericFormat,
    dateFilterApply,
    stringFilterApply,
    customIsoDateAdd,
    removeAllSpace,
    fetchLogicDate,
    fixDate,
    numericFormat
} = require("../../common/utils");
const asmapping = require("../common/asmapping");
const keyMonth = require("../common/month");

const getTitleRef = function(property_vol, property_folio, state='VIC')
{
    let vol = {data1: '', data2: ''};
    let fol = {data1: '', data2: ''};
    let res = '';

    if (['WA','SA'].includes(state)){
        const titleref_vol = (property_vol)   ? `Volume ${property_vol}` : '';
        const titleref_fol = (property_folio) ? `Folio ${property_folio}` : '';
        const newval = `${titleref_vol} ${titleref_fol}`; 
        return fxs((newval||'').trim());

    }else if (state == 'VIC'){
        if (property_vol){
            const propVol = (property_vol||'').toUpperCase().trim();
            if (propVol.indexOf('VOLUME') !=-1 && propVol.indexOf('FOLIO') !=-1){
                const arrPropVol = propVol.split(' ');
                if (arrPropVol.length === 4 && arrPropVol[0] == 'VOLUME' && arrPropVol[2] == 'FOLIO'){
                    return property_vol;
                }
            }
          
            const arrVol = property_vol.toLowerCase().split('volume') || [];
            vol.data1 = removeAllButNumbers(arrVol[0]) ? removeAllButNumbers(arrVol[0]) : ((arrVol.length > 0) ? removeAllButNumbers(arrVol[1]) : '');
            if (arrVol.length > 2){
                vol.data2 =removeAllButNumbers(arrVol[2]);
            }
        }
        if (property_folio){
            const arrFol = property_folio.toLowerCase().split('folio');
            fol.data1 = removeAllButNumbers(arrFol[0]) ? removeAllButNumbers(arrFol[0]) : ((arrFol.length > 0) ? removeAllButNumbers(arrFol[1]) : '');
            if (arrFol.length > 2){
              fol.data2 = removeAllButNumbers(arrFol[2]);
            }
        }

        if (vol.data1 && fol.data1){
            res = `Volume ${vol.data1} Folio ${fol.data1}`
        }else if (vol.data1 || fol.data1){
            res = vol.data1 ? `Volume ${vol.data1}` : `Folio ${fol.data1}`;
        }
        if (vol.data2 && fol.data2){
            res = res.concat(` and Volume ${vol.data2} Folio ${fol.data2}`);
        }
    }else if (state == 'NSW'){
        const aVal = (property_vol||'').split(' ');
        for (let i = 0; i < aVal.length; i++) {
            const el = aVal[i];
            if ((el||'').indexOf('/') !=-1){
                res = el;
                break;
            }
        }        
    }
    return res;
}

const getSelectedPlanType = function(ocr, propComList = [])
{
    let newValue;
    for (let i = 0; i < propComList.length; i++) {
        const el = propComList[i];
        if (i === 0 && fxSelected(ocr[el]) == 'Yes'){
            newValue = 'BUP';
        }else if (i === 1 && fxSelected(ocr[el]) == 'Yes'){
            newValue = 'GTP';
        }else if (i === 2 && fxSelected(ocr[el]) == 'Yes'){
            newValue = 'SP';
        }
    }
    return newValue;
}

const getPlanType = function(ocr)
{
    let newValue;
    let cTypeTrace = 1;
    const pcomlist = ['property_com_BUP','property_com_GTP','property_com_SP'];

    if (ocr.ctype == 'resi'){
        newValue = removeAllNumbers(ocr.property_desc_on ||'');
    }else{
        cTypeTrace = 2;       
        newValue = getSelectedPlanType(ocr, pcomlist);
    }
    if (!newValue){
        if (cTypeTrace === 2){
            newValue = removeAllNumbers(ocr.property_desc_on ||'');
        }else{
            newValue = getSelectedPlanType(ocr, pcomlist);
        }
    }
    return newValue;
}

const getFieldValue = function(ocrtext, fld, state)
{
    //"Folio Identifier 15/SP101409"
    if (!ocrtext){
        return false;
    } 
    const arrTxt = (ocrtext||'').split('/') || [];

    if (fld === 'lotno' || fld === 'lotno2'){
        if (state == 'VIC' || state == 'SA'){
            let arrTxt2;
            if ((ocrtext||'').indexOf('&') !=-1){
                arrTxt2 = (ocrtext||'').split('&') || [];                    
            }else{
                arrTxt2 = (ocrtext||'').split(' ') || []; 
            }
            return (arrTxt2.length > 0 && fld === 'lotno') ? arrTxt2[0] : ((arrTxt2.length > 1 && fld === 'lotno2') ? arrTxt2[1] : '');
        }
        
        if (arrTxt.length === 2){
            if (state == 'TAS'){
                return removeAllButNumbers(arrTxt[1]);
            }
            const arrRet  = (arrTxt[0]||'').split(' ') || [];
            const idxSize = arrRet.length;
            return (idxSize > 0) ? removeAllButNumbers(arrRet[idxSize-1]) : ocrtext;
        }
        return ocrtext ||'';

    }else if (fld === 'plantype'){
        if (arrTxt.length === 2){
            const ret  = (arrTxt[1]||'');
            return (ret) ? removeAllNumbers(ret) : ocrtext;
        }
        return removeAllNumbers(arrTxt[0]||'');

    }else if (fld === 'planno' || fld === 'planno2'){
        
        if (state == 'VIC'){
            const arrTxt = (ocrtext||'').split(' ') || [];
            return (arrTxt.length > 0 && fld === 'planno') ? arrTxt[0] : ((arrTxt.length > 1 && fld === 'planno2') ? arrTxt[1] : '');

        }else if (['WA','SA','QLD'].includes(state)){
            return removeAllButNumbers(arrTxt[0]||'');
            //return arrTxt[0]||'';
        }
        // "LOT 28 IN STRATA PLAN 10531 28/SP10531"
        if (arrTxt.length === 2){
            const ret  = (state=='TAS') ? (arrTxt[0]||'') : (arrTxt[1]||'');            
            return (ret) ? removeAllButNumbers(ret) : ocrtext;
        }else if (arrTxt.length === 3){
            const ret  = arrTxt[2] || arrTxt[1] || '';
            return (ret) ? removeAllButNumbers(ret) : ocrtext;
        }
    }
    return ocrtext;
}

// day of December
const searchMonth = function(targetMonth, monthList)
{
    for (const mon in monthList){
        const y = (targetMonth||'').toLowerCase().indexOf(mon.toLocaleLowerCase());
        if (y!=-1){
            return monthList[mon];
        }
    }
    return false;
}

const buildDateFrom = function(ocrday, ocrmonth, ocryear, enableWorkDays=false, context=null)
{
    const monthNumbers = keyMonth.keyNumberDatesMonth;
    const ocrMon = parseInt(ocrmonth);
    const kmon   = (isNaN(ocrMon) || !ocrMon) ? searchMonth(ocrmonth, monthNumbers) : ocrMon;
    const kyear  = dateFilterApply(ocryear);
    context.log("buildDateFrom debug 0: ", ocrMon, ocrday, kmon, kyear);

    const xday  = parseInt(ocrday   || '0') || removeAllButNumbers(ocrday);
    const xmon  = parseInt(kmon || '0');
    const xyear = parseInt(kyear  || '0');
    const sp1 = (xday) ? '/' : '';
    const sp2 = (xmon) ? '/' : '';
    const xdate = `${xday}${sp1}${xmon}${sp2}${xyear}`.trim();
    context.log("buildDateFrom debug xdate: ", xdate, enableWorkDays);

    return (xday && xyear) ? getISODate(xdate, enableWorkDays) : false;
}

const getMaxDateFromList = function(ocr, dateList=[])
{
    const newDateList = dateList.map(function(dt) {
        if (ocr && ocr[dt]){
            const sDate = ocr[dt];
            if (sDate){
                const dateFmt = ['DD/MM/YYYY', 'DD-MM-YYYY', 'DDMMMMY', 'MMMMDDY','Do MMMM YYYY'];
                const isDateValid = moment(sDate, dateFmt).isValid();
                if (isDateValid){
                    return moment(sDate, dateFmt);
                }                
            }
            return false;
        }
    });    
    const myDateList = (newDateList||[]).filter( d => d);
    if ( myDateList.length > 0 ){
        return moment.max(myDateList).format();
    }
    return false;
}


const getOcrFieldValue = function(ocr, fld, state, context)
{
    let docList = [];

    if (fld === 'pool_on_property'){
        if (state=='SA'){
            docList = ['pool_safety_pre_July','pool_safety_post_July','pool_safety_not_compliant','pool_safety_specify'];
        }else{
            docList = ['document_27','document_28','document_29','document_30','document_31'];
        }
                
        for (let i = 0; i < docList.length; i++) {
            const el = docList[i];
            if(fxSelected(ocr[el]) == 'Yes'){
                return 'Yes';
            }
        }        
        return 'No';
    }
    return false;
}

const getOcrDateFieldValue = function(ocr, fld, state, context)
{    
    if (state == 'VIC'){
        const buyer1_signature_date = dateFilterApply(ocr.buyer1_signature_date);
        const seller1_signature_date = dateFilterApply(ocr.seller1_signature_date);

        const buyer_date  = isoDate(buyer1_signature_date);
        if (fld === 'contract_date'){                  
            const seller_date = isoDate(seller1_signature_date);
            if (buyer_date && seller_date){
                return (buyer_date > seller_date) ? buyer_date : seller_date;
            }
            return seller_date || buyer_date || false;

        }else if (fld === 'cooloffdate'){
            
            context.log("DEBUG9999 ", state, buyer_date, ocr.cop);
            if (buyer_date){                
                return customIsoDateAdd(buyer_date, 3, context);
            }            
        }else if (fld === 'inidepdate'){            
            
            context.log("<<<<<< DEBUG kdate >>>>>>>> ", ocr.initial_deposit, ocr.cdate);
            const deposit = numericFormat(ocr.initial_deposit);
            return ((ocr.initial_deposit) && parseInt(deposit) > 0)  ? ocr.cdate || '' : '';
        }
    }else if (state == 'TAS'){

        if (fld === 'contract_date'){
            const cdate = buildDateFrom(ocr.contract_date_day, ocr.contract_date_month, ocr.contract_date_year, false, context);
            context.log("<<<<<< getOcrDateFieldValue DEBUG kdate/cdate >>>>>>>> ",ocr.contract_date_day, ocr.contract_date_month, ocr.contract_date_year, cdate);
            return cdate || '';

        }else if (fld === 'cooloffdate'){            
            if (fxSelected(ocr.cooling_off_waives) == 'Yes'){
                return '';
            }
            if (fxSelected(ocr.cooling_off_confirms) == 'Yes'){
                
                context.log("<<<<<< getOcrDateFieldValue DEBUG cooling_off_confirms-0 >>>>>>>> ", ocr.cdate);
                const cooldate = customIsoDateAdd(ocr.cdate, 3, context);
                context.log("<<<<<< getOcrDateFieldValue DEBUG cooling_off_confirms-1 >>>>>>>> ", cooldate);
                return cooldate;
            }

        }else if (fld === 'finance_date'){
            const duradays = parseInt(ocr.finance_duration_days||'0');
            const fdate    = buildDateFrom(ocr.finance_date_day, ocr.finance_date_month, ocr.finance_date_year, true, context);            
            if (fdate){
                return fdate;
            }else if (duradays){
                return customIsoDate(ocr.cdate, duradays,'customized', context);
            }
            return false;

        }else if (fld === 'inidepdate'){
            // if payment_other_date is checked then get value of payment_other_date_specifics

            if (fxSelected(ocr.payment_on_contract_date) == 'Yes'){
                return ocr.cdate;

            }else if (fxSelected(ocr.payment_other_date) == 'Yes'){
                return isoDate(ocr.payment_other_date_specifics);
            }
        }

    }else if (state == 'WA'){

        if (fld === 'contract_date'){
            const dateList = ['seller1_signature_date','seller2_signature_date','seller3_signature_date','seller4_signature_date'];
            return getMaxDateFromList(ocr, dateList);

        }else if (fld === 'inidepdate'){
            context.log("<<<<<< DEBUG kdate >>>>>>>> ", ocr.initial_deposit, ocr.cdate);
            return ((ocr.initial_deposit) && parseInt(ocr.initial_deposit) > 0)  ? ocr.cdate || '' : '';
        }        
        
    }else if (state == 'SA'){

        if (fld == 'smtdateonly'){
            if (fxSelected(ocr.settlement_duration) == 'Yes'){
                return '';
            }else if (fxSelected(ocr.settlement_specific) == 'Yes'){
                return buildDateFrom(ocr.settlement_period_day, ocr.settlement_period_month, ocr.settlement_period_year, true, context);
            }
        }else if(fld == 'contract_date'){

            const dateList = ['seller1_signature_date','seller2_signature_date','seller3_signature_date','seller4_signature_date','buyer1_signature_date','buyer2_signature_date','buyer3_signature_date','buyer4_signature_date'];
            return getMaxDateFromList(ocr, dateList);            
        }
    }
    return false;
}

const getChatelList = function(listChatel, ocr, state, context)
{
    let chatelList = [];   
    let newVal;
    for (let i = 0; i < listChatel.length; i++) {
        const el = listChatel[i];
        newVal = '';
        if (fxSelected(ocr[el]) == 'Yes'){
            
            if (state=='NSW'){
                if (el === 'incl_others' && ocr.incl_other_details){
                    newVal = ocr.incl_other_details || '';
                }else{                
                    newVal = (el||'').substring(5);
                }

            }else if (state=='SA'){
                newVal = (el||'').substring(5);                
            } 
            chatelList.push((newVal||'').replace(/[\_]/gm," "));
        }
        if (state == 'SA' && (el == 'excl_others_details' && ocr.excl_others_details)){
            chatelList.push((ocr.excl_others_details||'').replace(/[\_]/gm," "));
        }
    }    
    const retval = (chatelList||[]).join(', ');
    return capitalize(retval);
}

const getChatelList2 = function(chatelListText, context)
{
    let chatelList = [];           
    const listChatel = (chatelListText||'').split(',');
    context.log("getChatelList2(+) DEBUG >", chatelListText);

    for (let i = 0; i < listChatel.length; i++) {
        const el = (listChatel[i]).trim();
        // chatelList.push(capitalize(el||''));
        chatelList.push(el||'');
    }    
    const retval = (chatelList||[]).join(', ');
    context.log("getChatelList2(-) DEBUG >", retval);
    return capitalize(retval);
}

const getIncludedExcludeChatel = function(ocr, state, ref, context)
{    
    if (state == 'SA' && (ref=='excfix_applicable' || ref=='excfix')){
        const listChatel = [
            'excl_personal_effects',
            'excl_diswasher',
            'excl_loose_floor_cover',
            'excl_garden_ornaments',
            'excl_rubbish_bins',
            'excl_freestanding_furniture',
            'excl_others_details'
        ];
        return getChatelList(listChatel, ocr, state, context);
    }
    
    if (state == 'SA'){
        const listChatel = [
            'incl_built_in_furniture',
            'incl_fixed_floor_covering',
            'incl_dishwasher',
            'incl_light_fittings',
            'incl_window_treatment',
            'incl_rubbish_bins'
        ];
        return getChatelList(listChatel, ocr, state, context);

    }else{

        const listChatel = [
            'incl_blinds',
            'incl_dishwasher',
            'incl_light_fittings',
            'incl_stove',
            'incl_built_in_wardrobes',
            'incl_fixed_floor_coverings',
            'incl_range_hood',
            'incl_pool_equipment',
            'incl_clothes_line',
            'incl_insect_screens',
            'incl_solar_panels',
            'incl_tv_antenna',
            'incl_curtains',
            'incl_others'
        ];
        return getChatelList(listChatel, ocr, state, context);
    }
}

const getPropertyDetails = function(ocr, state, ref='', context)
{
    if (ref==='titleref'){
        if (['WA','SA','VIC'].includes(state)){
            return getTitleRef(ocr.property_vol, ocr.property_folio, state);        
        }else if(state == 'NSW'){            
            return getTitleRef(ocr.property_other_desc, '', state);        
        }
        return stringFilterApply(ocr.property_title_ref || '');

    }else if (ref==='lotno' || ref==='lotno2'){
        const lotNoMapping  = asmapping.customFields.lotno;        
        const srcIndex      = lotNoMapping[state];
        const lotNumber     = getFieldValue( ocr[srcIndex] ||'', ref, state);
        return fxs((lotNumber||'').replace('L', ''));

    }else if (ref==='planno' || ref==='planno2'){
        
        if (state == 'TAS'){
            return getFieldValue(ocr.property_title_ref, 'planno', state);

        }else if (['VIC','WA','SA'].includes(state)){
            return getFieldValue(ocr.property_plan_no, ref, state);            
        }else if (state == 'NSW'){
            return getFieldValue(ocr.property_other_desc, ref, state);
        }else if (state == 'QLD'){
            return getFieldValue(ocr.property_desc_on, ref, state);
        }
        return removeAllButNumbers((ocr && ocr.property_desc_lot )||'');
        
    }else if (ref==='numberoflots'){
        return ocr.numberoflots;

    }else if (ref==='ctsnum'){
        return ocr.property_com_title_scheme||'';

    }else if (ref==='ctsnam'){
        return ocr.property_com_scheme || '';

    }else if (ref==='excfix_applicable'){
        if (state == 'SA'){
            const chatel = getIncludedExcludeChatel(ocr, state, ref, context);
            return ((chatel||'').length > 0 ) ? 'Yes' : 'No';
        }else{
            if (state == 'VIC') return 'No';
            return ((ocr.property_excl_fixtures||'').length > 0 ) ? 'Yes' : 'No';
        }        
        
    }else if (ref==='excfix'){
        if (state == 'SA'){
            
            return getIncludedExcludeChatel(ocr, state, ref, context);
        }else{
            return (state == 'VIC') ? '' : capitalize(ocr.property_excl_fixtures||'');
        }
        
    }else if (ref==='pool_on_property'){
        if (['NSW','SA'].includes(state)){
            return getOcrFieldValue(ocr, 'pool_on_property', state, context);
        }
        return fxSelected(ocr.pool_existing_yes||''); 

    }else if (ref==='incchat_applicable'){
        if (state == 'NSW' || state == 'SA'){
            const chatel = getIncludedExcludeChatel(ocr, state, ref, context);
            const chatels02 = ocr.property_incl_chattel || '';
            if (state == 'SA'){
                return ((chatel||chatels02||'').length > 0 ) ? 'Yes' : 'No';    
            }
            return ((chatel||'').length > 0 ) ? 'Yes' : 'No';
        } 
        return ((ocr.property_incl_chattel||'').length > 0 ) ? 'Yes' : 'No';

    }else if (ref==='incchat'){
        if (state == 'NSW' || state == 'SA'){
            const chatels = getIncludedExcludeChatel(ocr, state, ref, context);
            if (state == 'SA'){
                const chatels02 = (ocr.property_incl_chattel || '').replace(/[\_]/gm," ").toLowerCase();
                return (chatels && chatels02) ? `${chatels}, ${chatels02}` : ((chatels) ? chatels : capitalize(chatels02));
            }
            return chatels;
        }
        return getChatelList2(ocr.property_incl_chattel||'', context);
    
    }else if (ref==='tenants_apply'){
        const tenantsMapping = asmapping.customFields.tenants_apply;
        const srcIndex = tenantsMapping[state];
        context.log("DEBUG srcIndex > tenants_apply ", srcIndex, " ==>>", ocr[srcIndex]);

        if (state == 'TAS'){
            return (fxSelected(ocr[srcIndex]) == 'Yes') ? 'No' : ((fxSelected(ocr.receive_rent_profit) == 'Yes') ? 'Yes' : 'No');
        }else if (state == 'QLD'){
            return ((ocr.tenant_name||'').length > 0 ) ? 'Yes' : 'No';
        }
        return fxSelected(ocr[srcIndex]);
        
    } 
    return false;
}

const getTransDetails = function(ocr, state, asfield, context)
{
    if (asfield=='initdep'){
        let deposit;
        if (['TAS','NSW','SA'].includes(state)){
            deposit = numericFormat(ocr.deposit_amt_numbers);
        }else{
            deposit = numericFormat(ocr.initial_deposit);
        }        
        return money(deposit);

    }else if (asfield=='purprice'){
        
        if (state== 'SA'){
            return money(ocr.total_purchase_price);
        }
        return money(ocr.purchase_price_numbers);

    }else if (asfield=='baldep'){
        let balance = '';
        if (state == 'VIC'){
            const fullDeposit = filterNumericFormat(ocr.deposit_amt_numbers);
            const initDeposit = filterNumericFormat(ocr.initial_deposit);
            balance = (fullDeposit - initDeposit);

        }else if (['QLD','WA'].includes(state)){
            balance = money(ocr.balance_deposit);
        }        
        return balance || '';

    }else if (asfield=='smtloc'){
        return stringUp(fx(ocr.place_for_settlement ||''));

    }else if (asfield=='amount_of_loan'){
        return (state=='VIC') ? money(ocr.loan_amt) : '';
    
    }else if (asfield=='depamount'){        
        if (['WA','NSW','SA','VIC'].includes(state)){
            return money(ocr.deposit_amt_numbers || ocr.initial_deposit);
        }        
    }else if (asfield=='depositbond'){
        return (state=='VIC') ? fxBool(ocr.general_condition_15) : '';

    }else if (asfield=='kreceived'){
        return  'Yes - Signed Contract';

    }else if (state == 'SA' && ['spc1det','spc2det','spc3det','spc4det'].includes(asfield)){                
        if (fxSelected(ocr.special_conditions) == 'Yes'){
            return '';            
        }else{        
            let retVal = '';    
            const spConAnnex = (fxSelected(ocr.subject_to_other_sp_condition_yes) == 'Yes') ? (ocr.sp_condition_annex||'') : '';
            const annexOcr = getSpAnnex(ocr, spConAnnex);

            if (asfield == 'spc1det' && fxSelected(ocr.sale_price_75k_yes) == 'Yes'){
                retVal = `Foreign Resident Withholding Regime ${annexOcr['sale_price_75k_yes']||''}`;
            }else if (asfield == 'spc2det' && fxSelected(ocr.subject_to_finance_yes) == 'Yes'){
                retVal = `Sale Subject to Finance ${annexOcr['subject_to_finance_yes']||''}`;
            }else if (asfield == 'spc3det' && fxSelected(ocr.subject_to_sale_and_settlement_yes) == 'Yes'){
                retVal =  `Sale Subject to Sale and Settlement of Purchaser's property ${annexOcr['subject_to_sale_and_settlement_yes']||''}`;
            }else if (asfield == 'spc4det'){                
                retVal = (fxSelected(ocr.subject_to_settlement_yes) == 'Yes') ? `Sale Subject to Settlement of Purchaser's property ${annexOcr['subject_to_settlement_yes']||''}` : '';
            }
            return (retVal) ?  retVal : ((asfield == 'spc1det' && annexOcr.special) ? annexOcr.special : '');
        }

    }else if (state == 'QLD' && ['spc1det'].includes(asfield)){
        return ocr.special_conditions || '';        
    }
    return false;
}

const getSpAnnex = function(ocr, txt)
{
    let annex = {};
    if (fxSelected(ocr.sale_price_75k_yes) == 'Yes'){
        annex['sale_price_75k_yes'] = txt;
    }
    if (fxSelected(ocr.subject_to_finance_yes) == 'Yes'){
        delete annex['sale_price_75k_yes'];
        annex['subject_to_finance_yes'] = txt;
    }
    if (fxSelected(ocr.subject_to_sale_and_settlement_yes) == 'Yes'){
        delete annex['sale_price_75k_yes'];
        delete annex['subject_to_finance_yes'];
        annex['subject_to_sale_and_settlement_yes'] = txt;
    }
    if (fxSelected(ocr.subject_to_settlement_yes) == 'Yes'){
        delete annex['sale_price_75k_yes'];
        delete annex['subject_to_finance_yes'];
        delete annex['subject_to_sale_and_settlement_yes'];        
        annex['subject_to_settlement_yes'] = txt;
    }
    if ((txt||'').length > 0 && Object.keys(annex).length === 0){
        annex['special'] = `Annexure ${txt}`
    }
    return annex;
}

const getKeyDates = function(ocr, state, asfield, contractDate, context)
{   
    //mcdate => used in VIC,SA as instructionDate >> AS.Collection||instdate    
    let newval = '';
    try {
        
        context.log(`[getKeyDates](+) ${state}: contract_date: ${contractDate} target: ${asfield}`);

        switch (asfield) {
            case 'baldepdate':
                if (state == 'VIC'){
                    newval = dateFilterApply(ocr.deposit_by || '', asfield);
                }else if (state == 'WA'){
                    const dpDay = ocr.dp_specific_date_day || 0;
                    newval = ocr['cdate'];
                    if (dpDay > 0){
                        newval = customIsoDateAdd(ocr['cdate'], dpDay, context);
                    }
                }else if(state == 'QLD'){
                    newval = fetchLogicDate(ocr.balance_deposit_instruction||'', context);
                }else{
                    newval = ocr.balance_deposit_instruction||'';
                }
                break;
            case 'findate':                
                newval = (state == 'VIC' || state == 'NSW') ? fixDate(ocr.approval_date) :  ((state == 'TAS') ?  getOcrDateFieldValue(ocr, 'finance_date', state, context) : fixDate(ocr.finance_date));
                break;
            case 'pooldate':
                newval = ocr.pool_inspection_date;
                break;
            case 'bpdate':
                newval = fetchLogicDate(ocr.pest_inspection_date || ocr.inspection_date, context);
                break;
            case 'cooloffdate':
                newval = (state =='WA') ? '' : getOcrDateFieldValue(ocr, asfield, state, context);
                break;
            case 'uncondate':
                newval = (state == 'VIC') ? ocr.approval_date : '';
                break;
            case 'instdate':
                newval = (['VIC','NSW','SA'].includes(state)) ? ocr.mcdate : '';
                break;
            case 'inidepdate':            
                newval = ['TAS','VIC','WA'].includes(state) ? getOcrDateFieldValue(ocr, asfield, state, context) : ((state=='NSW') ? isoDate(ocr.contract_date) : '');
                break;
            case 'smtdateonly':
                if (state == 'VIC'){
                    newval =  ocr.settlement_due_date;
                }else if(state=='SA'){
                    newval = getOcrDateFieldValue(ocr, asfield, state, context);
                }else{
                    newval = ocr.settlement_date || ocr.settement_date || '';
                }
                break;
            case 'kdate':
                if (['NSW','QLD'].includes(state)){
                    // newval = fixDate(ocr.contract_date);
                    newval = fetchLogicDate(ocr.contract_date, context);
                    context.log("=======================",newval,"---------------------", state);
                }else{
                    newval = getOcrDateFieldValue(ocr, 'contract_date', state, context);    
                }
                break;
            case 'stampdue':
                newval = (state=='TAS') ? '' : '';
                break;            
                
        }
        
        if (['smtdateonly','pooldate','findate','bpdate','baldepdate','uncondate'].includes(asfield) && 
            ((newval||'').toLowerCase().indexOf('contract') !=-1)){

            context.log(`[getKeyDates](~) pre-[[[ ${asfield} >>]]]`, newval);
            const dateWithFormat = isoDate(newval);
            if (!dateWithFormat || dateWithFormat == 'Invalid date' || dateWithFormat == undefined){
                const logicDate = dateRangeLogic(newval, contractDate, asfield, context) || '';
                context.log(`[getKeyDates](~) post-[[[ ${asfield} >>]]]`, newval, '>>', logicDate);
                return logicDate;
            }
            context.log(`[getKeyDates](~) post2-[[[ ${asfield} >>]]]`, newval, dateWithFormat);
            return dateWithFormat||'';                        
        }    
        
        context.log(`[getKeyDates](~)  ${asfield} >>`, newval);
        const retdate = ((newval||'').indexOf("T")!=-1) ? newval : isoDate(newval);
        context.log(`[getKeyDates](~) result => ${retdate} ${asfield}=${newval}  contract_date: ${contractDate}`);
        return retdate;

    } catch (err) {
        const errMsg   = err.message || 'error';
        context.log(`[getKeyDates](-) ERROR  (${asfield}, ${newval}) >>`,errMsg);
        throw new Error(err.message);
    }
}

const processCollectionFields = function(ocr, state, context)
{
    const pathState    = (state||'').toLowerCase();
    const baseCollection = require(`../../templates/states/mapping/${pathState}`);
        
    let newAS = {};
    const dateTypes   = asmapping.actionstepFieldsType.dateType;
    const boolTypes   = asmapping.actionstepFieldsType.booleanType;
    const moneyTypes  = asmapping.actionstepFieldsType.moneyType;
    const camelTypes  = asmapping.actionstepFieldsType.camelCase;
    const tfboolTypes = asmapping.actionstepFieldsType.boolTypeTF;
        
    const propDetails  = ['titleref','lotno','ctsnum', 'ctsnam', 'excfix_applicable','excfix','pool_on_property','planno','incchat_applicable','incchat','tenants_apply','lotno2','planno2','numberoflots'];
    const transDetails = ['initdep','purprice','baldep','smtloc','amount_of_loan','depamount','depositbond','kreceived','spc1det','spc2det','spc3det','spc4det','spc5det','spc6det','spc7det'];
    const keyDates     = ['kdate','smtdateonly','baldepdate','findate','bpdate','pooldate','uncondate','stampdue','cooloffdate','instdate','inidepdate'];
    const workDayList  = ['smtdateonly','baldepdate','findate','bpdate','pooldate','uncondate','stampdue','cooloffdate','instdate','inidepdate'];

    const contractDate = getKeyDates({...ocr}, state, 'kdate', false, context);
    if (contractDate){
        ocr['cdate'] = contractDate;
    }
    
    //mack debug only
    // ocr['ocrReview'] = false;

    for (const asfield in baseCollection){
        const rowData = baseCollection[asfield];
        const reviewField = rowData['uiTag'];

        let newValue = '';
        if (!rowData) continue;

        if (ocr.ocrReview){
            if (dateTypes.includes(asfield)){               
                const reviewData = ocr[reviewField];
                const ccData = workDayList.includes(asfield) ? isoDateFormat(reviewData, true) : isoDateFormat(reviewData);
                newAS[reviewField] = ccData;
                context.log(`DEBUG processCollectionFields ocrReview=true ${asfield} @ newAS[${reviewField}] = [${ccData}] >> [${reviewData}]` );

            }else if (boolTypes.includes(asfield)){
                newAS[reviewField] = fxSelected(ocr[reviewField]);

            }else if (moneyTypes.includes(asfield)){
                newAS[reviewField] = filterNumericFormat(ocr[reviewField]);
            }else if (tfboolTypes.includes(asfield)){
                newAS[reviewField] = fxBool(ocr[reviewField]);
            }else{
                newAS[reviewField] = (asfield=='depositbond') ? fxBool(ocr[reviewField]) : ocr[reviewField];
            }
            continue;
        }

        if (propDetails.includes(asfield)){
            if (ocr.ctype == 'resi' && state == 'QLD' && (asfield=='ctsnam' || asfield=='ctsnum')){
                newValue  = '';
            }else{
                newValue  = getPropertyDetails(ocr, state, asfield, context);
                if (['lotno','lotno2'].includes(asfield) && newValue && parseInt(newValue||0)){
                    ocr['numberoflots'] = (asfield === 'lotno2') ? '2' : '1';
                }
            }
            context.log(`processCollectionFields(~) propDetails: ${reviewField} / ${asfield} => ${newValue}`);

        }else if(transDetails.includes(asfield)){            
            if (state == 'QLD' && ['spc2det','spc3det','spc4det','spc5det','spc6det','spc7det'].includes(asfield)){
                continue;
            }
            newValue  = getTransDetails(ocr, state, asfield, context);            
            if (moneyTypes.includes(asfield)){
                newValue  = filterNumericFormat(newValue);
            }            
            context.log(`processCollectionFields(~) getTransDetails: ${reviewField} / ${asfield} => ${newValue}`);
            if (asfield == 'spc1det' && newValue.length > 155){                
                const spSentences = newValue.split('. ');                
                if (spSentences.length <= 7){
                    for (let i = 0; i < spSentences.length; i++){
                        const el        = spSentences[i];
                        const ocrIndex  = `special_condition_${i+1}`;
                        newAS[ocrIndex] = el;
                    }
                }
                continue;
            }

        }else if(keyDates.includes(asfield)){
            newValue  = getKeyDates({...ocr}, state, asfield, contractDate, context) || '';
            context.log(`processCollectionFields(~) getKeyDates: ${reviewField} / ${asfield} => ${newValue}`);

        }else if (asfield === 'plantype'){
            if (state === 'QLD'){                
                newValue = getPlanType(ocr);
            }else if(state === 'NSW'){
                newValue = getFieldValue(ocr.property_other_desc, asfield, state);
            }else if(state === 'WA'){
                newValue = getFieldValue(ocr.property_plan_no, asfield, state);
            }
        
        }else if(asfield === 'Pool_Cert'){
            if (fxSelected(ocr.pool_comp_exempt_yes) == 'Yes'){
                newValue = 'Has Pool Certificate';
            }else if (fxSelected(ocr.pool_comp_exempt_no) == 'Yes' && fxSelected(ocr.pool_safe_cert_yes) == 'Yes'){
                newValue = 'No Certificate but has notice';
            }else if (fxSelected(ocr.pool_comp_exempt_no) == 'Yes' && fxSelected(ocr.pool_safe_cert_no) == 'Yes'){
                newValue = 'No Certificate and no notice';
            }else if (fxSelected(ocr.pool_comp_exempt_no) == 'Yes'){
                newValue = 'No Pool Certificate';
            }
        }else if(asfield === 'safetyswitch' ||asfield === 'safe_switch_inform'){                    
            newValue = (ocr && fxSelected(ocr.safety_switch_installed_yes)==='Yes') ? 'Yes' : (fxSelected(ocr.safety_switch_installed_no) === 'Yes' ? 'No' : '');
        }else if(asfield === 'smokealarm' || asfield === 'smoke_alarm_inform'){
            newValue = (ocr && fxSelected(ocr.smoke_alarm_installed_yes)==='Yes') ? 'Yes' : (fxSelected(ocr.smoke_alarm_installed_no) === 'Yes' ? 'No' : '');
        }else if(asfield === 'neighbour_dispute'){
            newValue = (ocr && fxSelected(ocr.ND_Act2011_affected_yes)==='Yes') ? 'Yes' : (fxSelected(ocr.ND_Act2011_affected_no) === 'Yes' ? 'No' : '');
        }
        newAS[reviewField] = newValue;
    }    
    context.log(`DEBUG processCollectionFields newAS-OCR >`, newAS);
    return newAS;
}

const previewASCollection = function(ocrPage={}, state, context)
{
    let asCollection = {};    
    const pathState    = (state||'').toLowerCase();    
    const baseCollection = require(`../../templates/states/mapping/${pathState}`);
    
    if(context){
        context.log("previewASCollection DEBUG > ", ocrPage);
    }else{
        console.log("previewASCollection DEBUG > ", ocrPage);
    }
    
    for (const asIndex in baseCollection){
        const rowData     = baseCollection[asIndex];
        const reviewField = rowData['uiTag'];
        const xdata       = ocrPage[reviewField];
        asCollection[asIndex] = xdata;
    }
    return asCollection;
}

const getASCollectionById = function(ocrPage={}, state, idx, context)
{
    const pathState    = (state||'').toLowerCase();
    const baseCollection = require(`../../templates/states/mapping/${pathState}`);
    
    const uiIndex = baseCollection[idx]['uiTag'];
    const retval  = ocrPage[uiIndex];
    context.log(`getASCollectionById ${state} DEBUG value ${idx}-${uiIndex} > `, retval);
    return retval;
}

module.exports = {
    processCollectionFields     : processCollectionFields,
    previewASCollection         : previewASCollection,
    getASCollectionById         : getASCollectionById
}
