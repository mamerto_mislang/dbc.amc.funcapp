'use strict';

const actionstepFieldsType = {
    "dateType" : [
        "kdate",
        "smtdateonly",
        "baldepdate",
        "findate",
        "bpdate",
        "pooldate",
        "uncondate",
        "stampdue",
        "cooloffdate",
        "instdate",
        "inidepdate"
    ],    
    "booleanType" : [
        "pool_on_property",
        "tenants_apply",
        "excfix_applicable",
        "incchat_applicable",
        "safetyswitch",
        "safe_switch_inform",
        "smokealarm",
        "smoke_alarm_inform",
        "neighbour_dispute"
    ],
    "moneyType" : [
        "purprice",
        "initdep",
        "baldep",
        "depamount",
        "amount_of_loan"
    ],
    "camelCase" : [
        "incchat",
        "excfix"
    ],
    "boolTypeTF" : [
        "depositbond"
    ]    
}


const specialLogicFields = {
    "property_council_area" : "property_local_gov"
}

const customFields = {
    tenants_apply : {
        'QLD' : 'tenant_name',
        'TAS' : 'vacant_property', 
        'NSW' : 'property_leasehold',
        'VIC' : 'purchaser_vacant_posession_prop',
        'SA'  : 'existing_tenancy_yes',
        'WA'  : false
    },
    lotno: {
        'QLD' : 'property_desc_lot',
        'TAS' : 'property_title_ref', 
        'NSW' : 'property_other_desc',
        'VIC' : 'property_desc_lot',
        'SA'  : 'property_AllotSecUnitLot',
        'WA'  : 'property_desc_lot'
    }
}

const actionstepFields = {
    actionstepFieldsType,
    specialLogicFields,
    customFields
}
module.exports = Object.freeze(actionstepFields);

