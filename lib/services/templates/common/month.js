const keyDatesMonth = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC'
];

const keyNumberDatesMonth = {
    'JANUARY' : 1,
    'FEBRUARY' : 2,
    'MARCH' : 3,
    'APRIL' : 4,
    'MAY' : 5,
    'JUNE' : 6,
    'JULY' : 7,
    'AUGUST' : 8,
    'SEPTEMBER': 9,
    'OCTOBER' :10,
    'NOVEMBER' : 11,
    'DECEMBER' : 12,
    'JAN' : 1,
    'FEB' : 2,
    'MAR' : 3,
    'APR' : 4,
    'MAY' : 5,
    'JUN' : 6,
    'JUL' : 7,
    'AUG' : 8,
    'SEP' : 9,
    'OCT' : 10,
    'NOV' : 11,
    'DEC' : 12
};


const keyMappingDates = {
    keyDatesMonth,
    keyNumberDatesMonth
}
module.exports = Object.freeze(keyMappingDates);
