const { isDuplicatePartiesEmail, isDuplicatePartiesName} = require('../tools');

exports.extractSeller1 = function(ocr, context)
{
    let firstName  = ocr.firstName  || '';
    let middleName = ocr.middleName || '';
    let lastName   = ocr.lastName   || '';
        
    const isCompany = (ocr.ownershipType && ((ocr.ownershipType).toLowerCase() == 'individual' || ocr.ownershipType.toLowerCase().indexOf('private') !=-1)) ? false : true;                      
    let sellerName = (isCompany) ? (ocr.companyName || '').trim() : `${firstName} ${middleName} ${lastName}`.trim();
    let address = '';

    if (ocr.liveInProperty && ((ocr.liveInProperty).trim()).toLowerCase() == 'yes'){
         address = ocr.property_address1 || '';
    }else{
         address = ocr.residential || '';
    }
   
   const seller = {
      isCompany       : isCompany,
      name            : sellerName,
      email           : ocr.email,	
      phone           : ocr.mobile || ocr.phone,	
      address         : address,
      address2        : '',
      state           : '',
      suburb          : '',	
      postcode        : '',
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'seller1'
   }
   context.log('DEBUG extractSeller1', JSON.stringify(seller));
   return (sellerName) ? seller : false; 
}

exports.extractSeller2 = function(ocr, context)
{
   const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
  
   let xseller = {name:'', email: '', mobile: ''};
   const fname = ocr.seller2_fname || '';
   const mname = ocr.seller2_mname || '';
   const lname = ocr.seller2_lname || '';

    xseller.name      =  (fname && mname && lname) ? `${fname} ${mname} ${lname}` : `${fname} ${lname}`.trim();
    xseller.email     = ocr.seller2_email || '';
    xseller.mobile    = ocr.seller2_mobile || '';
       
   if (isDuplicatePartiesEmail(xseller.email, [ocr.email])){
      xseller.email = '';
   }

   if (isDuplicatePartiesName(xseller.name, ocr)){
      xseller.name = '';
   }

   const seller = {
      isCompany       : false,
      name            : xseller.name,	
      email           : xseller.email,	
      phone           : xseller.mobile,	
      address         : '',
      address2        : '',
      state           : '',	
      suburb          : '',	
      postcode        : '',
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'seller2'
   }
   
   context.log('DEBUG extractSeller2', JSON.stringify(seller));
   return (xseller.name) ? seller : false; 
}

exports.extractSeller3 = function(ocr)
{
   let xseller = {name:'', email: '', mobile: ''};
   const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
   const fname = ocr.seller3_fname || '';
   const mname = ocr.seller3_mname || '';
   const lname = ocr.seller3_lname || '';

    xseller.name      =  (fname && mname && lname) ? `${fname} ${mname} ${lname}` : `${fname} ${lname}`.trim();
    xseller.email     = ocr.seller3_email || '';
    xseller.mobile    = ocr.seller3_mobile || '';
    
    if (isDuplicatePartiesEmail(xseller.email, [ocr.email, ocr.seller2_email])){
      xseller.email = '';
    }

    if (isDuplicatePartiesName(xseller.name, ocr)){
      xseller.name = '';
    }


   const seller = {
       isCompany       : false,
       name            : xseller.name,	
       email           : xseller.email,	
       phone           : xseller.mobile,	
       address         : xseller.address,
       address2        : '',
       state           : '',	
       suburb          : '',	
       postcode        : '',
       option_license  : '',
       option_faxno    : '',
       option_abn      : '',
       option_mobile   : '',
       option_contact  : '',
       option_extra    : '',
       option_tag      : 'seller3'
    }
    return (xseller.name) ? seller : false; 
}

exports.extractSeller4 = function(ocr)
{
   const state = ocr.dealState && (ocr.dealState).trim().toUpperCase();
   let xseller = {name:'', email: '', mobile: ''};
   const fname = ocr.seller4_fname || '';
   const mname = ocr.seller4_mname || '';
   const lname = ocr.seller4_lname || '';

    xseller.name      =  (fname && mname && lname) ? `${fname} ${mname} ${lname}` : `${fname} ${lname}`.trim();
    xseller.email     = ocr.seller4_email || '';
    xseller.mobile    = ocr.seller4_mobile || '';

    if (isDuplicatePartiesEmail(xseller.email, [ocr.email, ocr.seller2_email, ocr.seller3_email])){
         xseller.email = '';
    }
    
    if (isDuplicatePartiesName(xseller.name, ocr)){
         xseller.name = '';
    }

    const seller = {
       isCompany       : false,
       name            : xseller.name,	
       email           : xseller.email,	
       phone           : xseller.mobile,	
       address         : '',
       address2        : '',
       state           : '',	
       suburb          : '',	
       postcode        : '',
       option_license  : '',
       option_faxno    : '',
       option_abn      : '',
       option_mobile   : '',
       option_contact  : '',
       option_extra    : '',
       option_tag      : 'seller4'
    }
    return (xseller.name) ? seller : false; 
}

exports.extractAgentOnSell = function (ocr)
{  
    
   let agentx = {name: '', email : '', phone: ''};
   if (ocr.agent_name || ocr.agentName){
      agentx.name    =  ocr.agent_name || ocr.agentName || '';
      agentx.email   =  ocr.agent_email || ocr.agentEmail || '';
      agentx.phone   =  ocr.agent_phonenumber || ocr.agentPhoneNumber || ocr.agentMobile ||'';
   }
   
    const agent = {
      isCompany       : false,
      name            : agentx.name,
      email           : agentx.email,
      phone           : agentx.phone,
      address         : '',
      address2        : '',
      state           : '',
      suburb          : '',
      postcode        : '',
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'agentOnSell'
    }
    return (ocr.agentHelping && ((ocr.agentHelping).trim()).toLowerCase() == 'yes' ) ? agent : false; 
}


exports.extractCorporateBody = function (ocr)
{  
    
   let corp = {name: '', email : '', phone: ''};
   
   if (ocr.strataOwnersCompanyName){
      corp.name    = ocr.strataOwnersCompanyName || '';
      corp.email   =  ocr.strataOwnersCompanyEmail || '';
      corp.phone   =  ocr.strataOwnersCompanyMobile || '';
   }
   
    const corporate = {
      isCompany       : true,
      name            : corp.name,
      email           : corp.email,
      phone           : corp.phone,
      address         : '',
      address2        : '',
      state           : '',
      suburb          : '',
      postcode        : '',
      option_license  : '',
      option_faxno    : '',
      option_abn      : '',
      option_mobile   : '',
      option_contact  : '',
      option_extra    : '',
      option_tag      : 'bodyCorporate'
    }
    return (ocr.strataOwnersCompanyName) ? corporate : false; 
}