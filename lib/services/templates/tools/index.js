exports.isDuplicatePartiesEmail = function(currEmail, partiesEmail =[] )
{
   if (currEmail){
        const partiesListEmail = partiesEmail || [];
        if (partiesListEmail.includes((currEmail.trim()).toLowerCase())){
            return true;
        }
   }
   return false;
}

exports.isDuplicatePartiesName = function(currName, ocr)
{
   let fname  = ocr.firstName  || '';
   let mname  = ocr.middleName || '';
   let lname  = ocr.lastName   || '';
   const fullname  =  (fname && mname && lname) ? `${fname} ${mname} ${lname}` : `${fname} ${lname}`;

   if ((fullname.trim()).toLowerCase() == ((currName||'').trim()).toLowerCase()){
      return true;
   }
   return false;
}

exports.isDuplicateOCRPartiesName = function(currentName, otherPartiesName =[], context )
{
   if (currentName){
        const otherListPartiesName = otherPartiesName || [];
        context.log(`DEBUG isDuplicateOCRPartiesName: ${currentName} `, otherListPartiesName);
        if (otherListPartiesName.includes((currentName.trim()).toLowerCase())){
            return true;
        }
   }
   return false;
}