const ocr = require('../../templates/ocr/participant');
const pdHelper = require('../../pipedrive/paramHelpers');

const getClientInListFormat = function(bst, dataList, matterParams, context)
{     
    let clientList = [];
    context.log(`[dealId::${matterParams.dealId}][${bst}].getClientInListFormat >`, JSON.stringify(dataList));

    for (let i = 0; i < dataList.length; i++) {
        const participant = dataList[i];
        if(participant){            
            const clientParam = pdHelper.createOCRParticipantParam(participant);
            if (clientParam){
                clientList.push(clientParam);
            }            
        }
    }
    // return (clientList.length > 0) ? {matterId: matterParams.matterId, clients : clientList } : false;
    return (clientList.length > 0) ? clientList : false;
}

const getClients = function(modeTrans, ocrData, materParams, context)
{   
    if (modeTrans === 'buyer'){
        let buyerx;

        const buyer1  = ocr.extractBuyer1(ocrData, context);
        const buyer2  = ocr.extractBuyer2(ocrData, context);
        const buyer3  = ocr.extractBuyer3(ocrData, context);
        const buyer4  = ocr.extractBuyer4(ocrData, context);
        
        if (!buyer2 && (buyer1 && buyer1.option_extra && buyer1.option_extra.person)){
            buyerx = {...buyer1};
            buyerx.name  = (buyer1.option_extra && buyer1.option_extra.person) ? buyer1.option_extra.person : '';
            buyerx.email = (buyer1.option_extra && buyer1.option_extra.email)  ? buyer1.option_extra.email  : '';
        }
        const listFormat = getClientInListFormat(modeTrans, [buyer1,buyer2||buyerx,buyer3,buyer4], materParams, context);
        return listFormat;
    }

    if (modeTrans === 'seller'){
        let sellerx;

        const seller1 = ocr.extractSeller1(ocrData);
        const seller2 = ocr.extractSeller2(ocrData, context);
        const seller3 = ocr.extractSeller3(ocrData, context);
        const seller4 = ocr.extractSeller4(ocrData, context);

        if (!seller2 && (seller1 && seller1.option_extra && seller1.option_extra.person)){
            sellerx =  {...seller1};
            sellerx.name  = (seller1.option_extra && seller1.option_extra.person) ? seller1.option_extra.person : '';
            sellerx.email = (seller1.option_extra && seller1.option_extra.email)  ? seller1.option_extra.email  : '';
            sellerx.address = (seller1.option_extra && seller1.option_extra.address)  ? seller1.option_extra.address  : '';
        }        
        const listFormat = getClientInListFormat(modeTrans, [seller1,seller2||sellerx,seller3,seller4], materParams, context);
        return listFormat;
    }
    return false;
}

module.exports = {
    getClients   : getClients
}
