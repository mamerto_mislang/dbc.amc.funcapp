const actionstep = require('../actionstep');
const pdHelper =  require('../pipedrive/paramHelpers');
const serviceHelper  = require('../common/helpers');

exports.createPendingMatter = async function(deal, token, context)
{
    const matterParam = pdHelper.createInitMatterParam(deal);

    context.log(`[deal::${deal.dealId}].createPendingMatter matterParam >`, JSON.stringify(matterParam));
    const mData  = await actionstep.createMatter(matterParam, token, context);
    const matterId =  serviceHelper.getMatterId(mData, context);

    context.log(`[deal::${deal.dealId}].createPendingMatter response >> ${matterId}`);
    return matterId;
}
