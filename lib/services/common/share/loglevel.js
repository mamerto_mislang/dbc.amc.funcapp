'use strict';

let logLevel = {
    info        : 0,
    debug       : 1,
    warning     : 2,
    error       : 3
}

module.exports = Object.freeze(logLevel); // freeze prevents changes by users