'use strict';

let systemMsg = {
    'SYS_INFO1' : '<b>STATUS:</b> Automatic Matter Creation - Completed!',
    'SYS_ERROR1' : '<b>STATUS:</b> AMC encountered error!<br>Please contact IT team for Support!'
};

module.exports = Object.freeze(systemMsg); // freeze prevents changes by users
