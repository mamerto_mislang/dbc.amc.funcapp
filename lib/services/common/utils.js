const moment = require('moment');
const kmonth = require('../templates/common/month');
const asfields = require('../templates/common/asmapping');

const stringUp = function(pval='')
{
    let retVal='';
    if(pval){
        const val = pval.toLowerCase();
        const arr = val.split(" ");
        for (var i = 0; i < arr.length; i++) {
            const row = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
            retVal += row + ((i < (arr.length-1)) ? ' ' : '');
        }
    }
    return retVal;
}

exports.capitalize = function(s)
{
    const s2 = (s||'').toLowerCase();
    return s2 && s2[0].toUpperCase() + s2.slice(1);
}

exports.isArray = function(a) {
    return (!!a) && (a.constructor === Array);
};

exports.isObject = function(a) {
    return (!!a) && (a.constructor === Object);
};

exports.allNumber = function(str) 
{
    var pattern = /^\d+$/;
    return pattern.test(str);
}

exports.isNumeric =function(value) 
{
    return /^-?\d+$/.test(value);
}

exports.money =function(value) 
{
    const pval = value || '';
    const xval = (`${pval}`).replace(/[\\$,'"]/g, "");
    return (xval) ? parseFloat(xval).toFixed(2) : '';
}

exports.numericFormat =function(value) 
{
    const pval = value || '';
    return (`${pval}`).replace(/[^\d.-]/g, "");    
}

exports.filterNumericFormat =function(value) 
{
    const pval = value || '';
    const val = (`${pval}`).replace(/[^\d.]/g, "");    
    const retVal = parseFloat(val||'');
    return isNaN(retVal) ? 0 : retVal;
}


exports.fx = function(v, v2='')
{
    let v3 = (v) ? v : ((v2) ? v2 : '');
    v3 = v3.replace('’', '');
    v3 = v3.replace('Undefined', '');
    v3 = v3.replace('undefined', '');
    const v4 = v3.replace(/[\\$'"]/g, "\\$&");
    if (/^[a-zA-Z 0-9 .@]+$/.test(v4)){
        return v4.trim();
    }
    const v5 = v4.replace(/[^a-z 0-9 @?_+.;:-]/gi,'');
    return v5.trim();
}

exports.fxFormat = function(v, v2='')
{
    let v3 = (v) ? v : ((v2) ? v2 : '');
    v3 = v3.replace('’', '');
    v3 = v3.replace('Undefined', '');
    v3 = v3.replace('undefined', '');
    const pos = (v3||'').toLowerCase().indexOf(' mc');

    if (v3 && (v3.toLowerCase().charAt(0) === 'm' && v3.toLowerCase().charAt(1) === 'c')){
        return `Mc${stringUp(v3.slice(2))}`;
    }else if(pos !=-1){
        let arr = [...stringUp(v3)];
        arr[pos+3] = `${(arr[pos+3]||'').toUpperCase()}`;
        return arr.join('');
    }

    const v4 = v3.replace(/[\\$'"]/g, "\\$&");
    if (/^[a-zA-Z 0-9 .@]+$/.test(v4)){
        return stringUp((v4||'').trim());
    }
    const v5 = v4.replace(/[^a-z 0-9 @?_+.;:-]/gi,'');
    return stringUp((v5||'').trim());
}

exports.cleanAddress = function(v, v2='')
{
    let v3 = (v) ? v : ((v2) ? v2 : '');
    v3 = v3.replace('’', '');
    v3 = v3.replace('Undefined', '');
    v3 = v3.replace('undefined', '');
    const v4 = v3.replace(/[\\$'"]/g, "\\$&");
    if (/^[a-zA-Z 0-9 .@]+$/.test(v4)){
        return v4.trim();
    }
    const v5 = v4.replace(/[^a-z 0-9 @?_+.;/:-]/gi,'');
    return v5.trim();
}

exports.fxPhoneLabel = function(phone1)
{
    return (phone1 && (((phone1||'').trim()).substr(0,2)==='04')) ? 'Mobile' : 'Business';
}

exports.fxPhoneOption = function(option_mobile, defaultPhone)
{
    const VOID_PHONE = '';
    if (option_mobile && defaultPhone){
        return (option_mobile.length > 0 && option_mobile == defaultPhone) ? VOID_PHONE : option_mobile;
    }
    return VOID_PHONE;
}

exports.fxs = function(v, hint='')
{
    const ret = (v) ? `${v}` : '';
    if (hint == 'up'){
        return stringUp(ret);
    }
    return ret;
}

exports.fxSelected = function(v, v2='')
{
    const optionMarked = (v) ? `${v}` : ((v2) ? `${v2}` : '');
    const isSelected = ((optionMarked||'').trim().toLowerCase());
    return (isSelected == 'selected' || isSelected == 'yes') ? 'Yes' : ((isSelected == 'unselected' || isSelected == 'no') ? 'No' : '');    
}

exports.fxBool = function(v)
{
    const optionMarked = (v) ? `${v}` : '';
    if (['T','t','F','f'].includes(optionMarked)){
        return optionMarked;
    }
    const isSelected = ((optionMarked||'').trim().toLowerCase());
    return (isSelected == 'true' || isSelected == 'yes' || isSelected == 'selected') ? 'T' : ((isSelected == 'unselected' || isSelected == 'false') ? 'F' : '');
}

const checkBool = function(bool) 
{
    return typeof bool === 'boolean' || 
           (typeof bool === 'object' && 
            bool !== null            &&
           typeof bool.valueOf() === 'boolean');
}

exports.fxc = function(v, v2='')
{
    const val = (v) ? `${v}` : ((v2) ? `${v2}` : '');
    return val.replace(',', '');
}

exports.fillX = function(val, i)
{
    return (val[i]) ? val[i].trim() : '';
}

exports.htmltoText = function(html) 
{
    let text = html;
    text = text.replace(/\n/gi, "\n"); //adjusted
    text = text.replace(/<style([\s\S]*?)<\/style>/gi, "");
    text = text.replace(/<script([\s\S]*?)<\/script>/gi, "");
    text = text.replace(/<a.*?href="(.*?)[\?\"].*?>(.*?)<\/a.*?>/gi, " $2 $1 ");
    text = text.replace(/<\/div>/gi, "\n\n");
    text = text.replace(/<\/li>/gi, "\n");
    text = text.replace(/<li.*?>/gi, "  *  ");
    text = text.replace(/<\/ul>/gi, "\n\n");
    text = text.replace(/<\/p>/gi, "\n\n");
    text = text.replace(/<br\s*[\/]?>/gi, "\n");
    text = text.replace(/<[^>]+>/gi, " "); //adjusted
    text = text.replace(/^\s*/gim, " "); //adjusted
    text = text.replace(/ ,/gi, ",");
    text = text.replace(/ +/gi, " ");
    text = text.replace(/\n+/gi, "\n");//adjusted
    text = text.replace(/&nbsp;/g, "");
    text = text.replace(/&gt;/g, "");
    text = text.replace(/&lt;/g, "");
    return text;
};

const filterDate = function (xdate)
{
    const vdate = (xdate||'').toUpperCase();
    const kkmonth = kmonth.keyDatesMonth;    
    for (let idx = 0; idx < kkmonth.length; idx++) {
        const elMonth = kkmonth[idx];
        const mExist  = vdate.indexOf(elMonth);
        if (mExist !=-1){            
            const adate = vdate.split(' ');
            for (let idx2 = 0; idx2 < adate.length; idx2++) {
                const el  = adate[idx2];
                if ((el||'').indexOf(elMonth) !=-1){
                    const res = moment(el, ['Do MMMM YYYY']).format();
                    return ((res||'').indexOf('Invalid') ==-1) ? res : false;
                }                    
            }                
        }
    }
    return false;
}

exports.isoDate = function(v, hint='contract')
{
    const mformat = ['DD/MM/YYYY', 'DD-MM-YYYY'];
    let resDate;
    let vdate = (v||'').trim();

    try {
        if (!v) {
            return false;
        }
        if (v.lastIndexOf('T00:00') !=-1 || ((v.lastIndexOf('T') > 4) && (v.toLowerCase().indexOf('contract') ==-1))){
            return getWorkingDay(v);
        }
        
        resDate = moment(v, mformat, true).format();
        if (resDate.lastIndexOf('Invalid') !=-1){
            throw Error(resDate);
        }

    } catch (err) {
        console.log(`Invalid date. date: [${vdate}]`, JSON.stringify(err));
        let mformat2 = ['DDMMMMY', 'MMMMDDY','Do MMMM YYYY'];
        if (vdate.indexOf('/') !=-1 || 
            vdate.indexOf('.') !=-1 ||
            vdate.indexOf('-') !=-1){
            vdate = vdate.replace(/\s/g,'');
            mformat2 = ['DD/MM/YYYY', 'DD-MM-YYYY'];
        }
        resDate = moment(vdate, mformat2 ).format();
    }
    
    if ((!resDate || (resDate||'').lastIndexOf('Invalid') !=-1) || ((resDate||'').indexOf('0000-') === 0)){
        resDate = filterDate(v);
        if (!resDate){
            return resDate;
        }
    }
    
    if (!['mc','contractdate'].includes(hint)){
        resDate = getWorkingDay(resDate);
    }
    return resDate;
}

exports.isoDateFormat = function(v, enableWorkDays=false)
{
    const mformat = ['DD/MM/YYYY', 'DD-MM-YYYY'];
    let resDate;
    let vdate = (v||'').trim();

    try {
        if (!v) {
            return '';
        }
        if (v.lastIndexOf('T00:00') !=-1){            
            return (enableWorkDays) ? getWorkingDay(v) : v;
        }

        resDate = moment(v, mformat, true).format();
        if (resDate.lastIndexOf('Invalid') !=-1){
            throw Error(resDate);
        }

    } catch (err) {
        console.log(`Invalid date. date: [${vdate}]`, JSON.stringify(err));
        let mformat2 = ['DDMMMMY', 'MMMMDDY','Do MMMM YYYY'];
        if (vdate.indexOf('/') !=-1 || 
            vdate.indexOf('.') !=-1 ||
            vdate.indexOf('-') !=-1){
            vdate = vdate.replace(/\s/g,'');
            mformat2 = ['DD/MM/YYYY', 'DD-MM-YYYY'];
        }
        resDate = moment(vdate, mformat2 ).format();
    }
    
    if ((!resDate || (resDate||'').lastIndexOf('Invalid') !=-1) || ((resDate||'').indexOf('0000-') === 0)){
        resDate = filterDate(v);
        if (!resDate){
            return resDate;
        }
    }    
    return (enableWorkDays) ? getWorkingDay(resDate) : resDate;
}

const getWorkingDay = function(xdate)
{
    const weekday = moment(xdate).day();
    return (weekday===0) ? moment(xdate).add(1, 'days').format() : (weekday===6) ? moment(xdate).add(2, 'days').format() : xdate;
}

exports.getISODate = function(v, enableWorkDays= false)
{
    const mformat = ['DD/MM/YYYY', 'DD-MM-YYYY'];
    let resDate;
    let vdate = (v||'').trim();

    try {
        if (!v) {
            return false;
        }
        if (v.lastIndexOf('T00:00') !=-1 || ((v.lastIndexOf('T') > 4) && (v.toLowerCase().indexOf('contract') ==-1))){
            return enableWorkDays ? getWorkingDay(v) : v;
        }
        
        resDate = moment(v, mformat, true).format();
        if (resDate.lastIndexOf('Invalid') !=-1){
            throw Error(resDate);
        }

    } catch (err) {
        console.log(`Invalid date. date: [${vdate}]`, JSON.stringify(err));
        let mformat2 = ['DDMMMMY', 'MMMMDDY','Do MMMM YYYY'];
        if (vdate.indexOf('/') !=-1 || 
            vdate.indexOf('.') !=-1 ||
            vdate.indexOf('-') !=-1){
            vdate = vdate.replace(/\s/g,'');
            mformat2 = ['DD/MM/YYYY', 'DD-MM-YYYY'];
        }
        resDate = moment(vdate, mformat2 ).format();
    }
    
    if ((!resDate || (resDate||'').lastIndexOf('Invalid') !=-1) || ((resDate||'').indexOf('0000-') === 0)){
        resDate = filterDate(v);
        if (!resDate){
            return resDate;
        }
    }
    
    return enableWorkDays ? getWorkingDay(resDate) : resDate;
}

exports.customIsoDate = function(v, addDays=0, hint='contract', context)
{
    const mformat = ['DD/MM/YYYY', 'DD-MM-YYYY'];
    let resDate;
    let vdate = (v||'').trim();

    try {
        context.log(`customIsoDate(~) date: [${vdate}]`, v, addDays, hint);
        if (!v) {
            return false;
        }        
        resDate = moment(v, mformat, true).format();
        if (resDate.lastIndexOf('Invalid') !=-1){
            throw Error(resDate);
        }
    } catch (err) {
        context.log(`customIsoDate(~) Invalid date. date: [${vdate}]`, JSON.stringify(err));
        let mformat2 = ['DDMMMMY', 'MMMMDDY','Do MMMM YYYY'];
        if (vdate.indexOf('/') !=-1 || 
            vdate.indexOf('.') !=-1 ||
            vdate.indexOf('-') !=-1){
            vdate = vdate.replace(/\s/g,'');
            mformat2 = ['DD/MM/YYYY', 'DD-MM-YYYY'];
        }
        resDate = moment(vdate, mformat2 ).format();
    }
    
    if ((!resDate || (resDate||'').lastIndexOf('Invalid') !=-1) || ((resDate||'').indexOf('0000-') === 0)){
        resDate = filterDate(v);
        context.log("customIsoDate(~) ===============>>>[resDate]=============", resDate);
        if (!resDate){
            return resDate;
        }
    }    
    context.log("customIsoDate(~) ===============>>>[resDate2-0]=============", resDate);
    let weekday;
    weekday = moment(resDate).day();
    const noDays = addDays || 0;
    if (weekday===0){
        resDate = moment(resDate).add(noDays + 1, 'days').format();
    }else if (weekday===6){
        resDate = moment(resDate).add(noDays + 2, 'days').format();
    }else{
        resDate = moment(resDate).add(noDays, 'days').format();
    }
    context.log("customIsoDate(-) ===============>>>[resDate2-1]=============", resDate);
    return resDate;
}

exports.customIsoDateAdd = function(v, addDays=0, context)
{
    let resDate;
    let vdate = (v||'').trim();
    let weekday;

    try {        
        
        const noDays = addDays || 0;
        context.log(`customIsoDateAdd(~) date: [${vdate}]`, v, addDays);
        
        weekday = moment(v).day();
        
        if (weekday===0){
            resDate = moment(v).add(noDays + 1, 'days').format();
        }else if (weekday===6){
            resDate = moment(v).add(noDays + 2, 'days').format();
        }else{
            resDate = moment(v).add(noDays, 'days').format();
        }
        
        context.log("customIsoDateAdd(-) ===============>>>[resDate2-1]=============", resDate);
        return resDate;

    } catch (err) {
        context.log(`customIsoDateAdd(~) Invalid date. date: [${vdate}]`, JSON.stringify(err));
    }
    return false;
}

exports.dateRangeLogic = function(ocrDate='', contractDate = '', hint='', context)
{
    context.log(`[dateRangeLogic](+) ocrDate: ${ocrDate} contractDate: ${contractDate} hint: ${hint}`);
    const cdate  = (ocrDate || '').toLowerCase(); 
    let addDate = false;
    let ccdate;
    let weekday;

    try {
        if (!ocrDate || !contractDate){
            return false;
        }
        if ((cdate||'').toLowerCase().indexOf('contract') ==-1){
            throw new Error('Error#1 invalid date!');
        }
        if ((cdate||'').toLowerCase().indexOf(' from ') !=-1){
            addDate = true;
        }

        const aDays = cdate.split(' days ') || [];
        if (aDays.length <= 0){
            throw new Error('Error#2 invalid date!');
        }
        const xdata = (aDays[0].split(' ') || []);
        const xsize = xdata.length;
        const xdays = (xsize > 0) ? parseInt(xdata[xsize - 1]) : parseInt(xdata[xsize]);
        
        if (addDate){
            ccdate = moment(contractDate).add(xdays, 'days').format();
        }else{
            ccdate = moment(contractDate).subtract(xdays, 'days').format();
        }
        
        if (ccdate.lastIndexOf('Invalid') !=-1){
            throw new Error('Error#3 invalid date! debug => ' + ccdate);
        }
        
        if (addDate){
            weekday = moment(contractDate).add(xdays, 'days').day();
            if (weekday===0){
                ccdate = moment(contractDate).add(xdays + 1, 'days').format();
            }else if (weekday===6){
                ccdate = moment(contractDate).add(xdays + 2, 'days').format();
            }
        }else{
            weekday = moment(contractDate).subtract(xdays, 'days').day();
            if (weekday===0){
                ccdate = moment(contractDate).subtract(xdays - 1, 'days').format();
            }else if (weekday===6){
                ccdate = moment(contractDate).subtract(xdays - 2, 'days').format();
            }
        }

        context.log(`[dateRangeLogic](-) ocrDate: ${ocrDate} contractDate: ${contractDate} weekday: ${weekday} ret ccdate: ${ccdate} hint: ${hint} addDate:${addDate}`);
        return ccdate;
        
    } catch (err) {
        context.log.error(`[dateRangeLogic] Error Invalid date [${ocrDate}]. Hint: ${hint} AddDate:${addDate}`, err);
        return false;
    }
}

exports.stringUp = function(val)
{
    return stringUp(val);
}

exports.filterOcr = function(val, hint='')
{
    if(val && val.length > 2){
        if (hint=='email'){
            val = val.replace(/\Email+/g, '');
        }else if (hint=='address'){
            val = val.replace(/\Address+/g, '');
        }else if (hint=='name'){
            val = val.replace(/\.+/g, '').trim();
        }
        const dot2 = val.indexOf('..');
        let val2 = val;
        if (dot2 !=-1 && dot2 >= 0){
            val2 = (val||'').substring(0, dot2);
        }
        const xval = val2 && val2.replace(/\:+/g, '') || val2;
        return (xval||'').trim();
    }
    return val||'';
}


exports.fillUpName = function(longName = '')
{
    let person = {firstName : '', middleName : '', lastName : ''};
    const xname = (longName||'').split(' ');
    const nPos = xname.length;
    let firstName=middleName=lastName='';

    if (xname){
        firstName  = xname[0];
        lastName   = xname[1];
        if (nPos===2){
            lastName  = xname[1];
        }else if(nPos === 3){
            middleName = xname[1];
            lastName   = xname[2];
        }else if(nPos === 4){
            middleName = xname[1];
            lastName   = `${xname[2]} ${xname[3]}`;
        }else if(nPos > 4){
            firstName  =  `${xname[0]} ${xname[1]}`;
            middleName = xname[2];
            lastName   = `${xname[3]} ${xname[4]}`;
        }
    }
    
    person.firstName  = stringUp(firstName);
    person.middleName = stringUp(middleName);
    person.lastName   = stringUp(lastName);

    return person;
}

exports.fxAddress = function(ocrAddress, mode=1)
{
    let val;
    const v = (ocrAddress) ? (ocrAddress).split(' ') : false;
    console.log("<<<<DEBUG>>>>", v);
    
    if (!v){
        const xlength =  (v||'').length;
        if (xlength && xlength < 3 ){
            val = (mode===1) ? v[0] : ((v[1]) ? v[1] : v[0]);
        }else  if (xlength > 2){
            if (mode===1){
                val =  `${v[0]} ${v[1]}`;
            }else{
                val =  v[2];
                val =  (v[3]) ? `${val} ${v[3]}` : val;
                val =  (v[4]) ? `${val} ${v[4]}` : val;
            }            
        }
    }
    return (val) ? val : '';
}

exports.formatLine = function(person)
{
    if (person.unit_street_no && person.street_name && person.street_type){
        return `${person.unit_street_no} ${person.street_name}, ${person.street_type}`;
    } else {
        if (person.unit_street_no && person.street_name){
            return `${person.unit_street_no}, ${person.street_name}`;
        }else if (person.unit_street_no && !person.street_name && !person.street_type){
            return person.unit_street_no;
        }else if (!person.unit_street_no && person.street_name && !person.street_type){
            return person.street_name;
        }
    }
    return '';
}

exports.formatLine2 = function(pData)
{
    let unitAndStreetNumber = pData.street_no;
    let addr = {companyName: '', line1 : ''};

    if (pData.street_no && pData.unit_no){
        unitAndStreetNumber = `${pData.unit_no}/${pData.street_no}`
    }else if (!pData.street_no && pData.unit_no){
        unitAndStreetNumber = pData.unit_no;
    }else if (!pData.street_no && !pData.unit_no){
        unitAndStreetNumber = '';
    }

    if (unitAndStreetNumber && pData.street_name &&  pData.street_type && pData.suburb && pData.state && pData.post_code){
        addr.companyName = `${unitAndStreetNumber}, ${pData.street_name}, ${pData.street_type}, ${pData.suburb}, ${pData.state}, ${pData.post_code}`
    }else if (unitAndStreetNumber && pData.street_name &&  pData.street_type && pData.suburb && pData.state && pData.post_code){
        addr.companyName = `${unitAndStreetNumber}, ${pData.street_name}, ${pData.street_type}, ${pData.suburb}, ${pData.state}, ${pData.post_code}`
    }
    
    if (unitAndStreetNumber && pData.street_name && pData.street_type){
        line1 =  `${unitAndStreetNumber} ${pData.street_name}, ${pData.street_type}`;
    }else if (unitAndStreetNumber && pData.street_name){
        line1 =  `${unitAndStreetNumber} ${pData.street_name}`;
    }else if (unitAndStreetNumber && !pData.street_name && !pData.street_type){
        line1 =  unitAndStreetNumber;
    }else if (!unitAndStreetNumber && pData.street_name && !pData.street_type){
        line1 =  pData.street_name;
    }else if (!unitAndStreetNumber && pData.street_name && pData.street_type){
        line1 =  `${pData.street_name}, ${pData.street_type}`;
    }
    addr.line1 = line1;
    return addr;
}

exports.formatLine02 = function(pData)
{ 
    let addr = {companyName: '', line1 : ''};
    let companyLine = [];
    let line1 = [];
    let streetLine  = (pData.street_no && pData.street_name) ? `${pData.street_no} ${pData.street_name}` : pData.street_no || pData.street_name;
    let streetLine2 = (pData.street_type && streetLine) ? `${streetLine} ${pData.street_type}` : streetLine || pData.street_type;

    if (pData.unit_no && streetLine2){
        companyLine.push(`${pData.unit_no}/${streetLine2}`);
        line1.push(`${pData.unit_no}/${streetLine2}`);

    }else if(streetLine2){
        companyLine.push(streetLine2);
        line1.push(streetLine2);

    }else if(pData.unit_no){
        companyLine.push(pData.unit_no);
        line1.push(pData.unit_no);
    }

    if(pData.suburb){
        companyLine.push(pData.suburb);
    }
    if(pData.state){
        companyLine.push(pData.state);
    }
    if(pData.post_code){
        companyLine.push(pData.post_code);
    }
    addr.companyName = (companyLine.length > 0 ) ? companyLine.join(', ') : '';
    addr.line1       = (line1.length > 0 ) ? line1.join(', ') : '';
    return addr;
}

exports.formatCouncilName = function(companyName='')
{
    let xcompanyName = (companyName || '').trim();
    let name1=name2='';
    name1 = xcompanyName;

    if ((xcompanyName.toLowerCase()).lastIndexOf('council') ==-1){
        name1 = `${companyName} Council`;
    }
    if ((xcompanyName.toLowerCase()).lastIndexOf('council') ==-1 && (xcompanyName.toLowerCase()).lastIndexOf('regional') ==-1 ){
        name2 = `${companyName} Regional Council`;
    }else if ((xcompanyName.toLowerCase()).lastIndexOf('council') !=-1 && (xcompanyName.toLowerCase()).lastIndexOf('regional') ==-1 ){
        name2 = (xcompanyName.toLowerCase()).replace('council', 'Regional Council');
    }else if ((xcompanyName.toLowerCase()).lastIndexOf('council') !=-1 && (xcompanyName.toLowerCase()).lastIndexOf('regional') !=-1 ){
        name1 = (xcompanyName.toLowerCase()).replace('regional council', 'Council');
        name2 = xcompanyName;
    }
    return {name1: name1, name2: name2};
}

const patchCouncil = function(xcouncil)
{
    let aElem = [];
    let arrCouncil = [];
    const ref = ['regional','council'];

    const atext = (xcouncil || '').split(' ');
    for (let i = 0; i < atext.length; i++) {
       const el = (atext[i] || '').trim().toLowerCase();
       if (ref.includes(el)){
            continue;
       }
       aElem.push((el||'').trim());
    }
    const councilBase = stringUp(aElem.join(' '));
    arrCouncil.push(`${councilBase} Council`);
    arrCouncil.push(`${councilBase} Regional Council`);
    arrCouncil.push(`${councilBase}`);
    return arrCouncil || [];
}

exports.formatV2CouncilName = function(companyName='')
{
    const resourceList = patchCouncil((companyName || '').trim());
    return resourceList;   
}

exports.scanNameWithCondition = function(val, state='')
{
    if(val){
        const ref = [ 'and/or', 'and/ or', 'and /or', 'and/ or', ' and ', ' & '];        
        for (let i = 0; i < ref.length; i++) {
            const el = ref[i];
            const p=((val.trim()).toLowerCase()).indexOf(el);
            if (p!=-1){
                const person1 = (val.substring(0, p)||'').trim();
                const k=(el||'').length; 
                const add0or1 = ['NSW','WA','VIC'].includes(state) ? 0 : 1;
                let person2 = val.substring(p + k + add0or1);
                let address = '';

                if ((person2||'').toLowerCase().indexOf('nominee') !=-1){
                    person2 = '';
                }
                
                if (['NSW','WA'].includes(state)){
                    let thePerson2 = (person2||'').toUpperCase();
                    let arrP2 = [];
                    
                    if ((thePerson2||'').indexOf(' OF ') !=-1){                                                
                        if ((thePerson2||'').indexOf(state) ==-1){
                            thePerson2 = thePerson2+', '+state;
                        }

                        arrP2 = thePerson2.split(' OF ');
                        if (arrP2.length > 1){
                            person2 = (arrP2[0]||'').replace(' BOTH', '');
                            person2 = person2.replace('-', '').trim();
                            address = arrP2[1];
                        }
                    }else{

                        if ((thePerson2||'').indexOf(state) !=-1){                        
                            arrP2 = thePerson2.split(' ');
                            if (arrP2.length > 2){
                                person2 = `${arrP2[0]} ${arrP2[1]}`.trim();
                                address = arrP2.slice(2).join(" ");
                            }
                        }
                    }                    
                }
                return {person1Name : stringUp(person1), person2Name: stringUp(person2), address: address||''};
            }
        }
    }
    return false;
}

exports.fetchEmails = function(val)
{
    if(val){
        const nCount = (val.match(/@/g)||[]).length;
        if (nCount > 1){
            // const emails = val.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
            const emails = val.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gi);
            if ((emails||'').length > 1){
                return emails || [];
            }
            const dotPos = val.indexOf('.') + 4;
            const extI = (val.charAt(dotPos) === '.') ? 3 : 0;
            return [
                val.substring(0, dotPos + extI),
                val.substring(dotPos + extI)
            ]
        }
    }
    return [];
}

exports.testJSON = function(text){
    if (typeof text!=="string"){
        return false;
    }
    try{
        JSON.parse(text);
        return true;
    }
    catch (error){
        return false;
    }
}

exports.sleep = function(milliseconds) 
{
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

const removeAllButNumbers = function(val) 
{
    const pval = val||'';
    return (`${pval}`).replace(/\D/gm,"");
}

exports.removeAllButNumbers = function(val) 
{
    return removeAllButNumbers(val);
}

exports.removeAllNumbers = function(val) 
{
    const pval = val||'';
    return (`${pval}`).replace(/[0-9,\-.]/gm,"");
}

exports.removeAllSpace = function(val) 
{
    //ie: 12/ 09  /2021    => 12/09/2021
    //ie: Fix  that  bug ! => Fixthatbug!
    const pval = val||'';
    return (`${pval}`).replace(/\s/g,'');
}

exports.getOcrValue = function(extOcr, state, review=false)
{
    const tenants_apply = asfields.customFields.tenants_apply;
    let refIndex = (review) ? 1 : 0;
    const idx = tenants_apply[state][refIndex];
    if (review){
        console.log('get submitted pscem data');
        return extOcr[idx];
    }else{
        return (extOcr[idx]) ? 'selected' : 'unselected';
    }        
}

const getTitleRef = function(property_vol, property_folio, state='VIC')
{
    let vol = {data1: '', data2: ''};
    let fol = {data1: '', data2: ''};
    let res = '';

    if (state == 'VIC'){
        if (property_vol){
          
            const propVol = (property_vol||'').toUpperCase().trim();
            if (propVol.indexOf('VOLUME') !=-1 && propVol.indexOf('FOLIO') !=-1){
                const arrPropVol = propVol.split(' ');
                if (arrPropVol.length === 4 && arrPropVol[0] == 'VOLUME' && arrPropVol[2] == 'FOLIO'){
                    return property_vol;
                }
            }
          
            const arrVol = property_vol.toLowerCase().split('volume') || [];
            vol.data1 = removeAllButNumbers(arrVol[0]) ? removeAllButNumbers(arrVol[0]) : ((arrVol.length > 0) ? removeAllButNumbers(arrVol[1]) : '');
            if (arrVol.length > 2){
                vol.data2 =removeAllButNumbers(arrVol[2]);
            }
        }
        if (property_folio){
            const arrFol = property_folio.toLowerCase().split('folio');
            fol.data1 = removeAllButNumbers(arrFol[0]) ? removeAllButNumbers(arrFol[0]) : ((arrFol.length > 0) ? removeAllButNumbers(arrFol[1]) : '');
            if (arrFol.length > 2){
              fol.data2 = removeAllButNumbers(arrFol[2]);
            }
        }

        if (vol.data1 && fol.data1){
            res = `Volume ${vol.data1} Folio ${fol.data1}`
        }else if (vol.data1 || fol.data1){
            res = vol.data1 ? `Volume ${vol.data1}` : `Folio ${fol.data1}`;
        }
        if (vol.data2 && fol.data2){
            res = res.concat(` and Volume ${vol.data2} Folio ${fol.data2}`);
        }
    }
    return res;
}


//ie-date    11......22/2022 ABC => 12/22/2022
exports.dateFilterApply = function(val, hint='') 
{
    const ret = ((val||'').replace(/[^0-9 /]/gi,'')).replace(/\s/g,'');
    let i = 0;
    if (hint == 'baldepdate'){
        i = (ret||'').lastIndexOf("20");        
    }
    return (i> 5) ? ret.substring(0,i + 4) : ret;
}

//ie-reftitle 12345/6789 ABC => 12345/6789
exports.stringFilterApply = function(val) 
{
    return ((val||'').replace(/[^0-9 /]/gi,'')).replace(/\s/g,'');
}

exports.fixDate = function(val) 
{
    let isLogic = ((val||'').toLowerCase().indexOf('contract') !=-1);
    if (isLogic){
        return val;
    }
    // if (isWDateMonth(val)){
    //     return moment(val, 'Do MMMM YYYY').format();        
    // }    

    const cleanDate = ((val||'').replace(/[^0-9 -/]/gi,'')).replace(/\s/g,'');
    const dateFmt   = ['DD/MM/YYYY', 'DD-MM-YYYY', 'MM/DD/YYYY', 'DDMMMMY', 'MMMMDDY','Do MMMM YY','Do MMMM YYYY'];
    const isDate1   = moment(cleanDate, dateFmt, true).isValid();

    if (isDate1){
        return moment(cleanDate, dateFmt).format();
    }
    const isDate2 = moment(val, dateFmt).isValid();
    if(isDate2){
        const dfm = ['DD/MM/YYYY','DD-MMM-YYYY','DD MMM YYYY'];
        const theYear  = moment(val, dfm).format('YYYY');
        const theMonth = moment(val, dfm).format('MM');
        const theDay   = moment(val, dfm).format('DD');
        const isGoodDate = customValidate({year: theYear, month: theMonth, day: theDay});
        if (isGoodDate){
            return moment(val, dateFmt).format();
        }        
    }
    const fDate = filterDate(val);
    if (isDateValid(fDate)){
        return fDate;
    }
    return cleanDate;
}

const customValidate = function(oDate)
{
    const cMonth = kmonth.keyNumberDatesMonth;    
    const xmon   = (oDate.month||'').toUpperCase();
    const xyear  = parseInt(oDate.year||'0');
    const xday   = parseInt(oDate.day||'0');
    return ((cMonth[xmon]||'') && (xyear > 2000 && xyear < 2050) && (xday > 0 && xday < 32));
}

const validateDateObject = function(oDate)
{
    const xmon   = parseInt(oDate.month||'0');
    const xyear  = parseInt(oDate.year||'0');
    const xday   = parseInt(oDate.day||'0');
    return ((xyear > 2000 && xyear < 2050) && (xday > 0 && xday < 32) && (xmon > 0 && xmon < 13));
}


const isDateValid = function(val)
{    
    let xdate;
    let dateStrict = false;
    if (isWDateMonth(val)){
        xdate = val;
    }else{
        const dt = (val||'').split('T');
        xdate = dt[0];
        dateStrict = true;
    }
    const dateFmt   = ['DD/MM/YYYY', 'DD-MM-YYYY', 'YYYY-MM-DD', 'MM/DD/YYYY', 'DDMMMMY', 'MMMMDDY','Do MMMM YY','Do MMMM YYYY'];
    return moment(xdate, dateFmt, dateStrict).isValid();
}

const isWDateMonth = function(val)
{
    const vdate = (val||'').toUpperCase();
    const kkmonth = kmonth.keyDatesMonth;    
    for (let idx = 0; idx < kkmonth.length; idx++) {
        const elMonth   = kkmonth[idx];
        const monExist  = vdate.indexOf(elMonth);
        if (monExist !=-1){
            return true;
        }
    }
    return false;
}

const getDateMonth = function(val)
{
    const vdate = (val||'').toUpperCase();
    const objMonth = kmonth.keyNumberDatesMonth;        
    for (const mon in objMonth){
        const monVal = objMonth[mon];
        if (vdate.indexOf(mon) !=-1){
            return {mm: mon, m: monVal};
        }
    }
    return false;
}

const getDateYear = function(val, mon)
{
    const vdate     = (val||'').toUpperCase();
    const refMonth  = (mon.mm||'').toUpperCase();
    const idx       = vdate.indexOf(refMonth);
    if (idx!=-1){
        const yearInit = vdate.substring(idx + refMonth.length).trim();
        const yearPre  =  removeAllButNumbers(yearInit);
        if (yearPre.length === 2){
            return parseInt(`20${yearPre}`);
        }
        const yearPost = (yearPre.length === 2) ? parseInt(`20${yearPre}`) : parseInt(`${yearPre}`);
        return  (yearPost > 1999 &&  yearPost < 2050) ? yearPost : false;
    }
    return false;
}

const getDateDay = function(val, mon)
{
    const vdate     = (val||'').toUpperCase();
    const refMonth  = (mon.mm||'').toUpperCase();
    const idx       = vdate.indexOf(refMonth);
    if (idx!=-1){
        const dayInit = vdate.substring(0, idx).trim();
        const dayPre  =  removeAllButNumbers(dayInit);
        const dayPost = parseInt(`${dayPre}`);
        return  (dayPost > 0 &&  dayPost < 32) ? dayPost : false;
    }
    return false;
}

exports.fetchLogicDate = function(dateText, context)
{
    const mm = getDateMonth(dateText);
    context.log("[fetchLogicDate](+) DEBUG MM: ", mm, "@", dateText);
    if (mm){
        const yyyy = getDateYear(dateText, mm);
        context.log("[fetchLogicDate](+) DEBUG YYYY: ", yyyy, "@", dateText);
        if (yyyy){
            const dd = getDateDay(dateText, mm);
            context.log("[fetchLogicDate](+) DEBUG DD: ", dd, "@", dateText);
            if (dd){
                const theDate = `${dd}/${mm.m}/${yyyy}`;
                const dateFinal = moment(theDate,'DD/MM/YYYY').format();
                context.log("[fetchLogicDate](+) DEBUG dateFinal: ", dateFinal, "@", dateText);
                return dateFinal;
            }            
        }
    }else{
        const dateFmt   = ['DD/MM/YYYY', 'DD-MM-YYYY', 'MM/DD/YYYY', 'DDMMMMY', 'MMMMDDY','Do MMMM YY','Do MMMM YYYY'];
        const isDate2 = moment(dateText, dateFmt).isValid();
        if(isDate2){
            const dfm = ['DD/MM/YYYY','DD-MMM-YYYY','DD MMM YYYY'];
            const theYear  = moment(dateText, dfm).format('YYYY');
            const theMonth = moment(dateText, dfm).format('MM');
            const theDay   = moment(dateText, dfm).format('DD');
            const objDate  = {year: theYear, month: theMonth, day: theDay};
            const isGoodDate = validateDateObject(objDate);
            console.log("[fetchLogicDate](+) DEBUG nomonth isDate2: ", isDate2, "@", objDate, "dateText:", dateText, "isGoodDate:", isGoodDate);
            if (isGoodDate){
                return moment(dateText, dateFmt).format();
            }        
        }
    }
    return dateText;
}
