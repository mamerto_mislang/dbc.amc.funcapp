const listDocType = [
    { type: ".pdf", name: "application/pdf"},   
    { type: ".docx", name: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
    { type: ".doc", name: "application/msword"},
    { type: ".gif", name: "image/gif"},
    { type: ".jpeg", name: "image/jpeg"},
    { type: ".jpg", name: "image/jpeg"},
    { type: ".png", name: "image/png"},
    { type: ".pptx", name: "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
    { type: ".ppt", name: "application/vnd.ms-powerpoint"},
    { type: ".rtf", name: "application/rtf"},
    { type: ".txt", name: "text/plain"},    
    { type: ".csv", name: "text/csv"},  
    { type: ".csva", name: "application/csv"},  
    { type: ".xlsx", name: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
    { type: ".xls", name: "application/vnd.ms-excel"}
]

exports.getDocumentType = function(filename)
{
    const defa = "application/pdf";
    const doctype = listDocType.filter(function(row) {
        return filename.lastIndexOf(row.type) > 0;
    });
    return (doctype.length > 0) ? doctype[0].name : defa;
}
