'use strict';

let common = {
	PD_NO_SALE : 7,
	PD_CONTRACTREVIEW_BOOKED_STATUS : 5,
	PD_MATTER_CREATED_STATUS : 6,
	PD_WIP_STATUS : 17,
	PD_MCREADY_STATUS : 66,
	PD_MC_TESTING : 68,
	ASSIGN_TO_ID : 513,
	AS_CLIENT_TYPE : 71,
	AS_CLIENT_PRIMARY_TYPE : 72,
	AS_PROPERTY_ADDRESS_TYPE : 147,
	AS_SMS_GATEWAY_TYPE : 171,
	AS_BUYER_TYPE : 155,
	AS_SELLER_TYPE : 157,
	PARTICIPANT_TYPES : {
		'client' : 71,
		'client_primary' : 72,
		'property_address' : 147,
		'sms_gateway' : 171,
		'buyer' : 155,
		'seller' : 157,
		'agent_office' : 148,
		'agent_primary_contact' : 158,
		'solicitor' : 163,
		'solicitor_primary_contact' : 164,
		'council' : 125,
		'body_corporate' : 127,
		'sales_person' : 150
	},
	FO : 'TA',
	AS_CONVEYANCE_TYPE : {
		'1': "Purchase",
		'2': "Sale",
		'3': "Transfer"
	},
	STATES : [
		'QLD',
		'NSW',
		'VIC',
		'SA',
		'TAS',
		'WA',
		'ACT',
		'NT'
	],
    PD_STATE : {
		'11': "QLD",
		'12': "VIC",
		'13': "NSW",
		'14': "TAS",
		'15': "ACT",
		'16': "WA",
		'17': "SA",
		'18': "NT"
	},
	PIPEDRIVE_BST : {
		'1': "Buyer",
		'2': "Seller",
		'3': "Transfer"
	},
	KONEKTA_BST : {
		'1': "BUY",
		'2': "SEL",
		'3': "RPT"
	},
	PIPEDRIVE_PROPERTY_TYPE : {
	    '21': "Land Only",
	    '20': "Existing House",
	    '22': "Off the Plan",
		'19': "Apartment/Townhouse/Villa"
	},
	PD_MATTER_TYPE : [
		{id: 67,   state: 'QLD', bst: 'Buyer', konekta : 1},
		{id: 68,   state: 'NSW', bst: 'Buyer', konekta : 1},
		{id: 69,   state: 'VIC', bst: 'Buyer', konekta : 1},
		{id: 70,   state: 'SA', bst: 'Buyer', konekta : 1},
		{id: 71,   state: 'TAS', bst: 'Buyer', konekta : 1},
		{id: 72,   state: 'WA', bst: 'Buyer', konekta : 1},
		{id: 6701, state: 'QLD', bst: 'Seller', konekta : 1},
		{id: 6702, state: 'QLD', bst: 'Transfer', konekta : 1},
		{id: 6801, state: 'NSW', bst: 'Seller', konekta : 1},
		{id: 6802, state: 'NSW', bst: 'Transfer', konekta : 1},
		{id: 6901, state: 'VIC', bst: 'Seller', konekta : 1},
		{id: 6902, state: 'VIC', bst: 'Transfer', konekta : 1},
		{id: 7001, state: 'SA', bst: 'Seller', konekta : 1},
		{id: 7002, state: 'SA', bst: 'Transfer', konekta : 1},
		{id: 7101, state: 'TAS', bst: 'Seller', konekta : 1},
		{id: 7102, state: 'TAS', bst: 'Transfer', konekta : 1},
		{id: 7201, state: 'WA', bst: 'Seller', konekta : 1},
		{id: 7202, state: 'WA', bst: 'Transfer', konekta : 1}
	],
	PD_MATTER_TYPE_NEWFIELD : {
		'401' : 'Conveyance',
		'402' : 'Drafting',
		'403' : 'Review',
		'404' : 'Transfer A',
		'405' : 'Transfer B',
		'406' : 'Transfer C',
		'432' : 'Transfer D'
	},
	PD_CONVEYANCING_TYPE_NEWFIELD : {
		'411' : 'Sale',
		'412' : 'Purchase',
		'413' : 'Purchase - OTP',
		'414' : 'Transfer'
	},
	PRACTITIONER : [
		{state : 'QLD', value : 'CG'},
		{state : 'ACT', value : 'CG'},
		{state : 'WA',  value : 'DL'},
		{state : 'VIC', value : 'KL'},
		{state : 'NSW', value : 'AA'},
		{state : 'TAS', value : 'EB'},
		{state : 'SA',  value : 'JB'}
	],
	DEFAULT_PRACTITIONER : 'TT',
	PARTICIPANTS: [
		{typeId: 152, status: 1, name: 'Admin Staff', 		  	default : 513,   tag: 0, excluded: ''},
		{typeId: 151, status: 1, name: 'Conveyancer', 		  	default : 513,   tag: 0, excluded: ''}, //95275 old
		{typeId: 160, status: 1, name: 'Principal Solicitor', 	default : 8,     tag: 0, excluded: ''}, //21555 old, 8-CL-Think 91433-CL
		{typeId: 169, status: 1, name: 'Assigned Lawyer', 	  	default : 7049,  tag: 0, excluded: ''},
		{typeId: 141, status: 1, name: 'Trust Accountant',    	default : 60266, tag: 0, excluded: ''},
		{typeId: 148, status: 0, name: 'Agent (Office)', 	  	default : 46238, tag: 0, excluded: ''},
		{typeId: 158, status: 0, name: 'Agent Primary Contact', default : 46239, tag: 0, excluded: ''},
		{typeId: 155, status: 0, name: 'Buyer', 			  	default : 0, 	 tag: 1, excluded: ''},
		{typeId: 157, status: 0, name: 'Seller', 			  	default : 0, 	 tag: 2, excluded: ''},
		{typeId: 163, status: 0, name: 'Otherside Solicitor', 	default : 0, 	 tag: 0, excluded: ''},
		{typeId: 150, status: 0, name: 'Sales Person', 		  	default : 0, 	 tag: 0, excluded: ''},
		{typeId: 171, status: 0, name: 'SMS Gateway', 		  	default : 0, 	 tag: 0, excluded: ''},		
		{typeId: 173, status: 1, name: 'DBC Bot Triggers',	  	default : 134581, tag: 0, excluded: ''},
		{typeId: 174, status: 1, name: 'Concierge Team', 	  	default : 10623,  tag: 0, excluded: ''}
	],	
	PARTICIPANTS_OLD : [
		{typeId: 76,  status: 1, name: 'legal_assistant',     default : 513,   tag: 1, excluded: ''},
		{typeId: 105, status: 1, name: 'file_owner',          default : 95275, tag: 0, excluded: ''},
		{typeId: 138, status: 1, name: 'practice_manager',    default : 21555, tag: 0, excluded: ''},
		{typeId: 68,  status: 1, name: 'principal_lawyer',    default : 0,     tag: 2, excluded: ''},
		{typeId: 84,  status: 0, name: 'salesperson',         default : 0, 	   tag: 0, excluded: ''},
		{typeId: 141, status: 1, name: 'trust_accountant',    default : 60266, tag: 1, excluded: ''},
		{typeId: 140, status: 1, name: 'trust_administrator', default : 1005,  tag: 1, excluded: ''},
		{typeId: 142, status: 1, name: 'trust_signatory', 	  default : 513,   tag: 1, excluded: ''},
		{typeId: 93,  status: 1, name: 'branded_entity', 	  default : 98,    tag: 1, excluded: ''},
		{typeId: 128, status: 1, name: 'state_revenue',       default : 0, 	   tag: 2, excluded: ''},
		{typeId: 114, status: 0, name: 'other_party',         default : 0,     tag: 0, excluded: ''},
		{typeId: 119, status: 0, name: 'other_partycontact',  default : 0,     tag: 0, excluded: ''}
	],
	PRINCIPAL_LAWYERS : {
		'QLD' : 7049,
		'ACT' : 459,
		'WA'  : 37157,
		'VIC' : 513,
		'NSW' : 513,
		'TAS' : 36086,
		'SA'  : 81997,
	},
	ASSIGNED_LAWYERS : {
		'QLD' : 19486,  //Chris Gundi
		'WA'  : 37157,  //Deb Low
		'NSW' : 119549, //Adwar Alkhamesi = 123792
		'VIC' : 107601, //Keelyn Lim 107601 
		'TAS' : 76758,  //Ellen May Bennett
		'SA'  : 3827    //Jackie Blackwell
	},
	STATE_REVENUE_OFFICE : {
		'QLD' : 1377,
		'NSW' : 2895,
		'VIC' : 1741,
		'TAS' : 1938,
		'WA'  : 1377
	}
};
module.exports = Object.freeze(common); // freeze prevents changes by users
