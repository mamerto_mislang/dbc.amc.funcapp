const https = require('https');
let Duplex = require('stream').Duplex;

exports.bufferToStream = function(buffer) 
{
  let stream = new Duplex();
  stream.push(buffer);
  stream.push(null);
  return stream;
}

exports.streamToBuffer = function(stream) 
{
    return new Promise((resolve, reject) => {
      let buffers = [];
      stream.on('error', reject);
      stream.on('data', (data) => buffers.push(data))
      stream.on('end', () => resolve(Buffer.concat(buffers)))
    });
}

function streamToString (stream) {
    const chunks = [];
    return new Promise((resolve, reject) => {
      stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)));
      stream.on('error', (err) => reject(err));
      stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
    })
}

  
exports.bufferArrayToBuffer = function(ArrayBuff)
{
    const buffer = Buffer.from(ArrayBuff);
    return buffer;
}


exports.urlGetContent = async function(url)
{
    return new Promise((resolve, reject) => {
        let client = https;

        client.get(url, (resp) => {
            let dataBuffer = Buffer.from([]);

            resp.on('data', (chunk) => 
            {
                dataBuffer = Buffer.concat([dataBuffer, chunk]);
            });

            resp.on('end', () => 
            {
                 resolve(dataBuffer);
            });

        }).on("error", (err) => {
            reject(err);
        });
    });
};
