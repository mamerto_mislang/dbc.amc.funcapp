const common = require("../constant");
const { allNumber, stringUp, isNumeric } = require("../utils");
const streetList = require('../../templates/ocr/streetType');
const suburbPost = require('../../templates/ocr/suburbPost');

const process1stFragmentAddress = function(addressText='')
{
    let address          = {};
    const lastPart       = (addressText).split(' ');
    const nLen           = lastPart.length;
    const addressCollect = [];
    let statesFound      = suburbFound = false;
    let previousElem     = '';

    for (let i = 0; i < nLen; i++) {
        const elem = (lastPart[i]).trim();
        addressCollect.push(elem);
        if (streetList.includes(elem.toUpperCase())){
            addressCollect.pop(elem);
            addressCollect.pop(previousElem);
            address['street_name'] = `${previousElem} ${elem}`;
        }

        if ( common.STATES.includes(elem.toUpperCase())){
            addressCollect.pop(elem);
            statesFound = true;
        }
        if (!statesFound && suburbPost.includes(elem.toUpperCase())){
            addressCollect.pop(elem);
            addressCollect.pop(previousElem);
            suburbFound= true;
        }

        if(statesFound || suburbFound || address['street_name']){
            address['unit_street_no']  = addressCollect.join(' ');
        
            let ipos;
            if (address && address['street_name']){
                ipos = addressText.lastIndexOf(address['street_name']);
                address['street_name_pos'] = ipos + address['street_name'].length;
            }
            address['street_name'] =  address['street_name'] ?  address['street_name'] : elem;
            break;
        }
        previousElem = elem;
    }
    return address;
}

const process2ndFragmentAddress = function(val='', objAddr=undefined)
{
    let address = {};
    let raw = [];
    let country;
    let previousElem = '';

    const lastPart  = (val).split(' ');
    for (let i = 0; i < lastPart.length; i++) {
        const elem = (lastPart[i]).trim();
        if (elem.toUpperCase() == 'AUSTRALIA' || elem.toUpperCase() == 'AU'){
            country = elem;
            continue;
        }else{
            raw.push(elem);
        }
        
        if (common.STATES.includes(elem.toUpperCase())){
            address['state'] = elem.toUpperCase();
            raw.pop(elem);

            if(!objAddr || !objAddr['suburb']){
                const i2 = i-1;
                const i3 = i-2;

                if (lastPart[i2]){
                    if (lastPart[i3] && suburbPost.includes((lastPart[i2].trim()).toUpperCase())){
                        address['suburb'] = `${lastPart[i3]} ${lastPart[i2]}`;
                        raw.pop(lastPart[i3]);
                    }else{
                        address['suburb'] = lastPart[i2];
                        raw.pop(lastPart[i2]);
                    }
                }
            }else{
                address['suburb'] = (objAddr && objAddr['suburb']) ? objAddr['suburb'] : '';
            }

        }
        if (address['state'] && allNumber(elem)){
            address['post_code'] = elem;
            raw.pop(elem);
        }
        previousElem = elem;
    }

    if (raw.length > 0 ){
        let line1=line2=[];
        line1 = raw;

        if (raw[1]){
            line1 = raw.slice(0,2);
            address['rawAddress1'] = line1.join(' ');
        }
        if (raw[2]){
            line2 = raw.slice(2);
            address['rawAddress2'] = line2.join(' ');
        }
    }
    return address;
}

const validateAddress = function(address, ref = '')
{
    const refBase = [address['suburb'], address['state']];

    for (let i = 0; i < refBase.length; i++) {
        if (refBase[i].length > 0){
            const el = refBase[i].toLowerCase();
            const rval = address[ref].toLowerCase();            
            if (rval && rval.indexOf(el) !=-1){
                    const filterRval = rval.replace(el, '');
                    return filterRval.trim();
            }
        }
    }
    return false;
}

// exports.fixAddress = function(xaddress, ext='', state = '', ocr={})
const fixAddress = function(xaddress, ext='', state = '', ocr={})
{
    let theAddress = xaddress || '';
    let address = { unit_street_no : '', street_name : '', street_type : '', suburb : '', state : '', post_code : ''};
    if (xaddress && (xaddress.toLowerCase()).indexOf(state.toLowerCase()) ==-1){ 
        theAddress = (ext) ? `${xaddress} ${ext}` : xaddress;
    }
    const addressText = theAddress ? theAddress.replace(/[, ,]{2,}/g,',') : '';
    const arrMain = (addressText || '').split(',');
    const arrLen  = arrMain.length;
    let fragment1AddressResult = fragment2AddressResult = {};
    const mainTextAddr = arrMain[0] || '';

    if (arrLen === 1 && mainTextAddr.length > 0){
        fragment1AddressResult = process1stFragmentAddress(mainTextAddr);
        if (fragment1AddressResult.street_name_pos){
            const ipos = fragment1AddressResult.street_name_pos;
            fragment2AddressResult = process2ndFragmentAddress(addressText.substring(ipos));
        }else{
            fragment2AddressResult = process2ndFragmentAddress(addressText);
        }

    }else if (arrLen >= 2){
        fragment1AddressResult = process1stFragmentAddress(mainTextAddr);
        if (arrLen==2){
            fragment2AddressResult = process2ndFragmentAddress((arrMain.slice(1)).join(' '));
        }else{
            address['suburb'] = arrMain[1];
            fragment2AddressResult = process2ndFragmentAddress((arrMain.slice(2)).join(' '), address);
        }
    } 

    address['state']           = fragment2AddressResult.state     || '';
    address['suburb']          = fragment2AddressResult.suburb    || '';
    address['post_code']       = fragment2AddressResult.post_code || '';
    address['unit_street_no']  = fragment1AddressResult.unit_street_no ? fragment1AddressResult.unit_street_no : (fragment2AddressResult.rawAddress1 ? fragment2AddressResult.rawAddress1 : '');
    address['street_name']     = fragment1AddressResult.street_name    ? fragment1AddressResult.street_name    : (fragment2AddressResult.rawAddress2 ? fragment2AddressResult.rawAddress2 : '');

    let others = `${fragment2AddressResult.rawAddress1} ${fragment2AddressResult.rawAddress2}`

    others.replace(address['unit_street_no'], '');
    others.replace(address['street_name'], '');
    others.replace(address['state'], '');

    if (others && address['street_type']){
        address['street_type'] = others;
    }
    
    //fixing...
    let fixing = 0;
    if (ocr.state || state){
        address['state'] = ocr.state || state;
        fixing++;
    }
    if (ocr.suburb){
        address['suburb'] = ocr.suburb;
        fixing++;
    }
    if (ocr.post_code || ocr.postcode){
        address['post_code'] = ocr.post_code || ocr.postcode;
        fixing++;
    }

    //retry-fixing-1
    if (fixing===3 && (xaddress && 
        xaddress.indexOf(address['state']) ==-1 && 
        xaddress.indexOf(address['suburb']) ==-1 && 
        xaddress.indexOf(address['post_code']) ==-1 )){
        address['raw_line'] = xaddress;
    }else{

        //retry-fixing-2
        if ((!address['unit_street_no'] && !address['street_name']) && address['suburb'].length > 0 ){
            const pos1  = theAddress.indexOf(address['suburb']);
            if (pos1 !=-1){
                const addr1 = (theAddress.substring(0, pos1)).trim();
                const pos2  = addr1.length - 1;
                if (pos2){
                    if (addr1.charAt(pos2) === ','){
                        address['unit_street_no'] = addr1.substring(0, pos2);
                    }else{
                        address['unit_street_no'] = addr1.substring(0, pos2 + 1);
                    }
                }
            }

        }else{ //retry-fixing-3
            address['unit_street_no'] = validateAddress(address, 'unit_street_no');
            address['street_name']    = validateAddress(address, 'street_name');
        }
    }
    return address;
}

// exports.reFixAddress = function(address, suburb)
const reFixAddress = function(address, suburb)
{
    let unitStreetNo = '';
    let streetName = '';
    const xsub = suburb || '';
    const xsubpos = (address||'').indexOf(xsub);
    if (xsubpos !=-1){
        const st = ((address||'').substring(0,xsubpos)||'').trim();
        const arrSt = (st||'').split(' ') || [];
        if (arrSt.length > 1){
            unitStreetNo = arrSt[0];
            streetName   = arrSt.slice(1).join(' ');
        }else{
            streetName   = arrSt[0];
        }
    }
    return {unitStreetNo : unitStreetNo , streetName : streetName};
}


const getNextAddrText = function(arrAddr, index, cmd, step=0)
{
    const next = index + 1;
    const previous = index - 1;
    const backward = index - step;
    let retValue;

    if (cmd === 'next'){
        retValue = arrAddr[next];
    }else if (cmd === 'previous'){
        retValue = arrAddr[previous];
    }else if (cmd === 'backward'){
        retValue = arrAddr[backward];
    }
    return (retValue) ? stringUp((retValue||'').trim()) : false;
}

const getStreetValue = function(addressCollect=[])
{
    let address = {unit_street_no:'', street_name: ''};
    const nlen = (addressCollect||[]).length;
    let collect = [];

    for (let idx = 0; idx < nlen; idx++) {
        const el = addressCollect[idx].trim();
        collect.push(el);
        if (isNumeric(el.charAt(0))){
            address['unit_street_no'] = collect.join(' ').trim();
            address['street_name'] = addressCollect.slice(idx+1).join(' ');
            collect = [];
            break;            
        }
    }
    if (!address.unit_street_no){
        address['unit_street_no'] = addressCollect[0] || '';
        address['street_name']    = addressCollect.slice(1).join(' ').trim();
    }
    return address;
}

const simplifyAddress = function(addrTextParam, state='')
{
    let addrText = addrTextParam;
    if ((addrTextParam||'').toUpperCase().indexOf(state.toUpperCase()) ==-1){
        addrText = `${addrTextParam} ${state}`
    }
    const theAddr = (addrText||'').replace(/,/gi, "").toUpperCase();
    const arrAddr = theAddr.split(' ');
    const nLen   = arrAddr.length;
    let addressCollect = [];
    let previousElem;
    let address = {};    
    let resetAddressCollect = false;

    for (let i = 0; i < nLen; i++) {
        const elem = stringUp((arrAddr[i]).trim());

        addressCollect.push(elem);
        if (streetList.includes(elem.toUpperCase())){
            addressCollect.pop(elem);
            addressCollect.pop(previousElem);
            if (previousElem && isNumeric(previousElem.charAt(0))){
                address['unit_street_no'] = `${addressCollect.join(' ')} ${previousElem}`;
                address['street_name'] = `${elem}`;
            }else{
                address['street_name']    = `${previousElem} ${elem}`;
                address['unit_street_no'] = addressCollect.join(' ').trim();
            }
            addressCollect = [];
            resetAddressCollect = true;
        }

        // if (suburbPost.includes(elem.toUpperCase())){
        //     address['suburbx'] = elem;
        // }

        if ( common.STATES.includes(elem.toUpperCase())){
            addressCollect.pop(elem);
            let nextVal = getNextAddrText(arrAddr, i, 'next');
            let prevVal = getNextAddrText(arrAddr, i, 'previous');
            if (nextVal && allNumber(nextVal)){
                address['post_code'] = nextVal;
                address['suburb'] = prevVal;

            }if (prevVal && allNumber(prevVal)){
                address['post_code'] = prevVal;
                address['suburb']  = getNextAddrText(arrAddr, i, 'backward', 2);
            }                        
            address['state'] = elem.toUpperCase();

            if (!resetAddressCollect){
                const street = getStreetValue(addressCollect);
                address['unit_street_no'] = street.unit_street_no;
                address['street_name'] = street.street_name;
            }
        }
        previousElem = elem;
    }
    return address;
}

module.exports = {
    fixAddress: fixAddress,
    reFixAddress: reFixAddress,
    validateAddress: validateAddress,
    process2ndFragmentAddress : process2ndFragmentAddress,
    process1stFragmentAddress : process1stFragmentAddress,
    simplifyAddress : simplifyAddress
}
