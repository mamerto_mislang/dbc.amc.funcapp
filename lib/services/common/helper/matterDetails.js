const common = require("../constant");

exports.getMatterState = function(matterTypeId){
    const actionType = common.PD_MATTER_TYPE || [];
    const mtype = actionType.find((row) => {return (matterTypeId === row.id) ? row : null;});

    const matterKind = (mtype) ? {
        matterTypeName : `Conveyancing.com.au: ${mtype.state} - ${mtype.bst}`,
        state : mtype.state
    } : false;
    
    console.log(`[getMatterType] DEBUG >`, JSON.stringify(matterKind));
    return matterKind;
}
