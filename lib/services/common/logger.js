const logLevel = require('../common/share/loglevel');

// let process = {env: {AZFUNCLOG_CONTROL_VALUE: 8}}

const reverse = function(s)
{
    return (s === '') ? '' : reverse(s.substr(1)) + s.charAt(0);
}

exports.logIt = function(level=0, msg='', data=[], context=null)
{
    const logCtl  = process.env.AZFUNCLOG_CONTROL_VALUE || 13; //no debug, all enable
    const binCtl  = logCtl.toString(2);
    const mainCtl = reverse(binCtl.padStart(4,'0'));
    let u = {info:1, debug: 1, warning:1, error: 1};
    const dSize = data.length;
    const lData = (dSize === 1) ? data[0] : data;

    u.info    = parseInt(mainCtl.charAt(0));
    u.debug   = parseInt(mainCtl.charAt(1));
    u.warning = parseInt(mainCtl.charAt(2));
    u.error   = parseInt(mainCtl.charAt(3));

    if (level === logLevel.info    && u.info    || 
        level === logLevel.warning && u.warning ||
        level === logLevel.debug   && u.debug ){
        if(!context){
            if (dSize == 0){ 
                console.log('[logit]'+msg);
            }else{
                console.log('[logit]'+msg, lData);
            }            
        }else{            
            if (dSize == 0){
                context.log('[logit]'+msg);
            }else{
                context.log('[logit]'+msg, lData);
            }            
        }
        
    }else if (level === logLevel.error && u.error || 
        level === logLevel.critical && u.critical){       
        if(!context){
            console.log('[logit]'+msg, lData);
        }else{
            context.log.error('[logit]'+msg, lData);
        }
    } 
}

exports.logIt2 = function(level=0, context=null, msg='', data=[])
{
    const logCtl  = process.env.AZFUNCLOG_CONTROL_VALUE || 13; //no debug, all enable
    const binCtl  = logCtl.toString(2);
    const mainCtl = reverse(binCtl.padStart(4,'0'));
    let u = {info:1, debug: 1, warning:1, error: 1};
    const dSize = data.length;
    const lData = (dSize === 1) ? data[0] : data;

    u.info    = parseInt(mainCtl.charAt(0));
    u.debug   = parseInt(mainCtl.charAt(1));
    u.warning = parseInt(mainCtl.charAt(2));
    u.error   = parseInt(mainCtl.charAt(3));

    if (level === logLevel.info    && u.info    || 
        level === logLevel.warning && u.warning ||
        level === logLevel.debug   && u.debug){
        if(!context){
            if (dSize == 0){ 
                console.log('[logit]'+msg);
            }else{                
                console.log('[logit]'+msg, lData);
            }            
        }else{            
            if (dSize == 0){
                context.log('[logit]'+msg);
            }else{
                context.log('[logit]'+msg, lData);
            }            
        }
        
    }else if (level === logLevel.error    && u.error || 
              level === logLevel.critical && u.critical){       
        if(!context){
            console.log('[logit]'+msg, lData);
        }else{
            context.log.error('[logit]'+msg, lData);
        }
    } 
}