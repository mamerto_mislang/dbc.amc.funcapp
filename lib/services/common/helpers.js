const path = require('path');
var url  = require('url');
const camelcaseKeys = require( "camelcase-keys" );
const common = require('./constant');
const { testJSON } = require('./utils');

exports.getBlobName = function (originalName, req=undefined) 
{
    const blobName = originalName.replace(/ /g, "_");
    const docname = path.basename(blobName);
    const dealId = ((req.query.dealId) ? `${req.query.dealId}/` :  ((req.query.deal_id) ?  `${req.query.deal_id}/` : ''));
    return `${dealId}${docname}`;
 };

 exports.getContractUpdatesBlobName = function (originalName, matterId='') 
{
    const blobName = originalName.replace(/ /g, "_");
    const docname = path.basename(blobName);    
    const matterIdFolder = (matterId) ? `matter-${matterId}/` : '';
    return `${matterIdFolder}${docname}`;
 };

 exports.getBlobName2 = function (originalName) 
{
    const blobName = originalName.replace(/ /g, "_");
    const docname = path.basename(blobName);
    return docname;
 };

 const getUID = function () {
   return Date.now();
};

 exports.createEventData = function (req, container, blobName){
  
   
   const timeStamp = new Date().toISOString();
   let contract_fullpath;
   if (container){
      contract_fullpath = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
   }
   
   const customData = [{ 
          id : `${getUID()}`,  
          eventType : req.query.eventType,
          subject: req.query.subject,
          data : {
              deal_id : req.query.dealId, 
              state : req.query.state,
              transaction_type : req.query.transactionType,
              first_name: req.query.firstName,
              last_name: req.query.lastName,
              email_address : req.query.address,
              mobile : req.query.mobile,
              contractUrl : contract_fullpath
          },
         dataVersion: "1.0",
         metadataVersion: "1",
         eventTime: `${timeStamp}`,
         topic: `${process.env.AZURE_EVENT_GRID_TOPIC}`
      }];
      return customData;
} 

exports.createEventData2 = function (req, container, blobName, ocr=true, relevantFiles = false){
  
   const timeStamp = new Date().toISOString();
      var url_parts = url.parse(req.url, true);
   
   //remove NULL operator& convert into Camel
   var query = JSON.parse(JSON.stringify(url_parts.query));
   console.log("DEBUG createEventData2 =>", query);

  console.log("--------------------------------------------------------------")
  delete query["eventType"];
  delete query["subject"];
  if (relevantFiles){
      delete query["relevantFiles"];
      query["documents"] = relevantFiles;
  }
  
   if (container){
      const contract_fullpath = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
      query["contractUrl"] = contract_fullpath;
   }

   // data : camelcaseKeys(query), 
   const customData = [{ 
            id : `${getUID()}`,  
            eventType : req.query.eventType,
            subject: req.query.subject,
            data : query, 
            dataVersion: "1.0",
            metadataVersion: "1",
            eventTime: `${timeStamp}`,
            topic: (ocr) ? `${process.env.AZURE_EVENT_GRID_TOPIC}` : `${process.env.AZURE_EVENT_GRID_TOPIC_NON_OCR}`
      }];
      return customData;
} 

exports.createResponse = function (req, container, blobName)
{
   var url_parts = url.parse(req.url, true);
   var query = JSON.parse(JSON.stringify(url_parts.query));
   let subject = "No Document Uploaded";
   if (container){
      const contract_fullpath = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
      query["contractUrl"] = contract_fullpath;
      subject = "Document Uploaded";
   }

   // data : camelcaseKeys(query)
   const customData = { 
            subject: subject,
            data : query
      };
      return customData;
}

exports.createResponse2 = function (req, container, blobName, code)
{
   var url_parts = url.parse(req.url, true);
   var query = JSON.parse(JSON.stringify(url_parts.query));
   let subject = "Relevant File Upload";
   if (container){
      const contract_fullpath = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
      query["contractUrl"] = contract_fullpath;
      query["code"] = code;
      subject = "Relevant File Uploaded";
   }

   // data : camelcaseKeys(query)
   const customData = { 
            subject: subject,
            data : query
      };
      return customData;
}

exports.errorResponse = function(dealId, msg = 'Deal Id has already been being processed!')
{
   return {
      status: "Error",
      message : msg,
      data: {
         deal_id : dealId
      }
  };
}

exports.errorMatterResponse = function(matterId, msg = 'Matter Id has not been processed yet!')
{
   return {
      status: "Error",
      message : msg,
      data: {
         matter_id : matterId
      }
  };
}

exports.validResponse = function(dealId)
{
   return {
      status: "OK",
      message : "Deal Id is good to process!",
      data: {
         deal_id : dealId
      }
  };
}

const createRandomNumber = function(){
   const maxNumber = 99;
   const rand99 = Math.floor(Math.random() * maxNumber) + '';
   return rand99.padStart(2, '0');
};

exports.matterName = function(mp, matterTypeId) {
   const randomNo = createRandomNumber();
   let matterName;

   if (matterTypeId > 66 ){
      matterName = `${mp.state}-${mp.xref}-${mp.practitioner}-${mp.fileOwner}-${mp.clientCode}-${mp.matterId}${randomNo}`;
   }else{
      matterName = `${mp.state}-${mp.practitioner}-${mp.fileOwner}-${mp.clientCode}-${mp.matterId}${randomNo}`;
   }
   return matterName;
};

const getPractitionerList = function ()
{
    const practitionerList = process.env.DEFAULT_PRACTITIONERS || [];
    if (practitionerList && practitionerList.length > 1){
        const practitionerListBuf = Buffer.from(practitionerList, 'base64').toString('utf-8');
        const validJson = testJSON(JSON.stringify(practitionerListBuf));
        if (validJson){
            const pListJson = JSON.parse(practitionerListBuf);            
            console.log(`[getPractitionerList] DEBUG PRACTITIONER > `, pListJson );
            return pListJson;
        }        
    }
    return false;
}

exports.getPractitioner = function(deal)
{
   let xpractioner;     
   const practitioner2 = getPractitionerList() || false;
   if (practitioner2){      
      const data = practitioner2.find((row) => {return (deal.state === row.s) ? row : null;});
      console.log("DEBUG practitioner data > ", data);

      xpractioner = data.v || common.DEFAULT_PRACTITIONER;
   }else{
      const practitioner = common.PRACTITIONER || [];
      const data = practitioner.find((row) => {return (deal.state === row.state) ? row : null;});
      xpractioner = (data.value) ? data.value : common.DEFAULT_PRACTITIONER;
   }      
   console.log("DEBUG practitioner", xpractioner);
   return xpractioner;
}

exports.getFO = function()
{ 
   return common.FO;
};

exports.getKonektaReference = function(deal)
{
   return common.KONEKTA_BST[deal.bst]
}

exports.getMatterId = function(matterData, context)
{
   if (!matterData.actioncreate || !matterData.actioncreate.id){
      context.log("[getMatterId]  >>", matterData);
   }
   return matterData.actioncreate.id;
}

exports.clientMatterParam = function(matterId, clientId, participantType = common.AS_CLIENT_TYPE)
{
   const clientParam = {
      actionparticipants : {
          links : {
              action : matterId,
              participantType : participantType,
              participant : clientId
         }
      }
   };
   return clientParam;
}

exports.participantMatterParam = function(matterId, clientId, participantType = common.AS_CLIENT_TYPE)
{
   const clientParam = {
      actionparticipants : {
          links : {
              action : matterId,
              participantType : participantType,
              participant : clientId
         }
      }
   };
   return clientParam;
}

exports.jsonResponse = function(customData, status=200)
{
   const bodyData = (status===200) ? { data: customData } :  { message: customData };
   const contextBody = {
      status  : status,
      body    : JSON.stringify(bodyData), 
      headers : {
         'Content-Type': 'application/json'
      }
   };
   return contextBody;
}

exports.jsonCustomResponse = function(customData, status=200)
{
   const contextBody = {
      status  : status,
      body    :  JSON.stringify(customData),
      headers : {
         'Content-Type': 'application/json'
      }
   };
   return contextBody;
}

exports.jsonBusParams = function(dealBody, context)
{
   const correlationId = context.invocationId;
   const payload = [{
      contentType : 'application/json',
      body : dealBody,
      applicationProperties: {
          source  : 'AMCPipedrive',
          type    : 'MCReadyPipeline',
          version : 1,
          CorrelationId : correlationId
      }                
   }];
   return payload;
}

exports.jsonBusCalculatorParams = function(dealBody, context)
{
   const correlationId = context.invocationId;
   const payload = [{
      contentType : 'application/json',
      body : dealBody,
      applicationProperties: {
          source  : 'AMCData',
          type    : 'StmtCalculatorReady',
          version : 1,
          CorrelationId : correlationId
      }                
   }];
   return payload;
}

exports.enableOcr = function(subject)
{
   const formSource = {
      'ContractReviewForm'   : true,
      'ContractUploadForm'   : true,
      'PropertyTransferForm' : false,
      'ContractDraftingForm' : false
   }
   return (formSource[subject] == undefined) ? true : formSource[subject];
}

exports.createContractUpdatesEventData = function (req, container, blobName, state='', dealId='', matterId=''){
  
   const timeStamp = new Date().toISOString();
      var url_parts = url.parse(req.url, true);
   
   //remove NULL operator& convert into Camel
   var query = JSON.parse(JSON.stringify(url_parts.query));
   console.log("DEBUG createContractUpdatesEventData =>", query);

  delete query["eventType"];
  delete query["subject"];
  delete query["code"];
  delete query["matter_id"];
       
   if (container){
      const contract_fullpath = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
      query["contractUrl"] = contract_fullpath;
      query["state"]       = state;
      query["dealId"]      = `${dealId}`;
      query["matterId"]    = `${matterId}`;
   }

   const customData = [{ 
            id : `${getUID()}`,  
            eventType : req.query.eventType || 'Event Grid',
            subject: req.query.subject || 'Signed Contract Updates',
            data : query, 
            dataVersion: "1.0",
            metadataVersion: "1",
            eventTime: `${timeStamp}`,
            topic: `${process.env.AZURE_CONTACTUPDATES_GRID_TOPIC}`
      }];
      return customData;
} 

exports.getStateMatterType = function(matterTypeId)
{
   const matterTypeList = common.PD_MATTER_TYPE || [];   
   const mtype = matterTypeList.find((row) => {return (matterTypeId === row.id) ? row : null;});
   return (mtype.id > 0) ? mtype.state : '';
}

exports.getPlanTypeList = function (context)
{
    const planList = process.env.PLAN_TYPE || [];
    if (planList && planList.length > 1){
        const planListBuf = Buffer.from(planList, 'base64').toString('utf-8');
        const validJson = testJSON(JSON.stringify(planListBuf));
        if (validJson){
            const pListJson = JSON.parse(planListBuf);            
            context.log(`[getPlanTypeList] DEBUG PlanType > `, JSON.stringify(pListJson));
            return pListJson;
        }        
    }
    return false;
}