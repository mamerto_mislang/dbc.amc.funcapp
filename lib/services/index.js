const actionstep = require('./actionstep');
const { htmltoText, formatV2CouncilName, filterOcr, stringUp, removeAllNumbers,removeAllButNumbers, fxSelected } = require('./common/utils');
const common = require('./common/constant');
const pdHelper = require('./pipedrive/paramHelpers');
const serviceHelper  = require('./common/helpers');
const { getClientCodeMatterName } = require('./actionstep/clientCode');
const participant  = require('./actionstep/participants');
const pipedrive = require('./pipedrive');
const { getParticipants, pdDealDetails } = require('./pipedrive/deal');
const client = require('./actionstep/client');
const teams = require('./teams');
const database = require('./database');
const ocr = require('../services/templates/ocr/participant');
const nonocr = require('../services/templates/nonocr/participant');
const {logIt2} = require('./common/logger');
const fa = require('./common/share/loglevel');
const matterHelper = require('./common/helper/matterDetails');
const path = require('path');
const collection = require('./actionstep/collections');
const asfields = require('./templates/common/asmapping');
const {getClients} = require('./templates/tools/review');
const pxcollect  = require("./templates/ocr/processCollection");

exports.updateMatterFileNotes = async function (matterId, notes, token, context) 
{
    for (let i = 0; i < notes.length; i++) {
        const textNote = htmltoText(notes[i]);
        if (textNote.indexOf('AMC SYSTEM GENERATED') !=-1 || textNote.indexOf('A.M.C SYSTEM GENERATED') !=-1){
            continue;
        }

        if (textNote.length > 5 ){
            const note = {
                filenotes : {
                    text : textNote,
                    links : {
                        action : matterId
                    }
                }
            }
            const response = (await actionstep.newFileNotes(note, token, context));
        }
    }
}

exports.createMatterFileNote = async function (matterId, notes, token, context) 
{    
    const textNote = htmltoText(notes);
    if (textNote.length > 0 ){        
        const note = {
            filenotes : {
                text : textNote,
                links : {
                    action : matterId
                }
            }
        }
        
        logIt2(fa.debug, context, `[matter::${matterId}].createMatterFileNote >`, [JSON.stringify(note)]);
        await actionstep.newFileNotes(note, token, context);
    }
}


exports.updateMatterName = async function (matterId, matterName, token, context)
{
    const matterNameParam = {
      actions : {
          name : matterName, 
          links : {
              assignedTo : common.ASSIGN_TO_ID
          }
      }            
    }
    logIt2(fa.debug, context, `[matter::${matterId}].updateMatterName matterNameParam >>`, [JSON.stringify(matterNameParam)] );
    const response = (await actionstep.updateMatter(matterId, matterNameParam, token, context));
    logIt2(fa.debug, context, `[matter::${matterId}].updateMatterName response >>`, [JSON.stringify(response.actions)] );
    return response;
}

exports.processClientParticipant = async function (deal, person, token, context)
{
    let clientCodeHint;
    const clientParam = pdHelper.createClientParam(person);
    logIt2(fa.info, context, `[deal::${deal.dealId}].processClientParticipant clientParam >>`, [JSON.stringify(clientParam)] );

    const res = await actionstep.searchClient(clientParam, token, context);
    let cli = pdHelper.getClientCredential(res);
    if (!cli){
        const clientParam2 = client.createClientFromPD(deal, person);
        logIt2(fa.info, context, `[deal::${deal.dealId}].processClientParticipant clientParam2 >>`, [JSON.stringify(clientParam2)]);
        const res2  = await actionstep.createClient(clientParam2, token, context);
        cli = pdHelper.getClientCredential(res2);
        if (!cli){
            throw new Error('CLIENT_CREDENTIAL_CODE_ERROR');
        }
        clientCodeHint =`${cli.clientCode}01`;
    }else{        
        clientCodeHint = await getClientCodeMatterName(cli.clientId, cli.clientCode, token, context);
    }
    const initMatterParam = pdHelper.createInitMatterParam(deal);
    const response = {
        nextClientCode : clientCodeHint,
        initMatterParam : initMatterParam,
        clientIdCode : cli
    }
    logIt2(fa.debug, context, `[deal::${deal.dealId}].processClientParticipant response >>`, [JSON.stringify(response)]);
    return response;
}

exports.createMatter = async function(deal, client, token, context)
{
    logIt2(fa.debug, context, `[deal::${deal.dealId}].createMatter client.initMatterParam >`, [JSON.stringify(client.initMatterParam)]);
    const mData  = await actionstep.createMatter(client.initMatterParam, token, context);
    const matterId =  serviceHelper.getMatterId(mData, context);

    const matterParams = {
        state : deal.state,
        practitioner : serviceHelper.getPractitioner(deal),
        fileOwner :  serviceHelper.getFO(deal),
        clientCode : client.nextClientCode,
        matterId : matterId,
        xref : serviceHelper.getKonektaReference(deal),
    }
    logIt2(fa.info, context, `[deal::${deal.dealId}].createMatter matterParams >`, [JSON.stringify(matterParams)] );

    const matterName = serviceHelper.matterName(matterParams, deal.matterTypeId);
    const response = {
        matterId : matterId,
        matterName : matterName
    }

    logIt2(fa.info, context, `[deal::${deal.dealId}].createMatter response >>`, [JSON.stringify(response)]);
    return response;
}

exports.updateServiceBusMatter = async function(deal, matterId, client, token, context)
{
    context.log(`[deal::${deal.dealId}][matterId::${matterId}].updateServiceBusMatter(+)`);

    const matterParams = {
        state : deal.state,
        practitioner : serviceHelper.getPractitioner(deal),
        fileOwner :  serviceHelper.getFO(deal),
        clientCode : client.nextClientCode,
        matterId : matterId,
        xref : serviceHelper.getKonektaReference(deal),
    }
    context.log(`[deal::${deal.dealId}].updateServiceBusMatter matterParams >`, JSON.stringify(matterParams));
    const matterName = serviceHelper.matterName(matterParams, deal.matterTypeId);
    const response = {
        matterId : matterId,
        matterName : matterName
    }
    context.log(`[deal::${deal.dealId}].updateServiceBusMatter response >>`, JSON.stringify(response));
    return response;
}

exports.createPropertyAddressParticipant = async function(matterId, address, person, token, context)
{
    const newClientId = (await participant.createPropertyAddressContact(address, person, token, context));
    const newPropertyAddressClientParam = serviceHelper.participantMatterParam(matterId, newClientId, common.PARTICIPANT_TYPES['property_address']);
    const response = (await actionstep.updateMatterParticipant(newPropertyAddressClientParam, token, context));
    return response;
}

exports.createClientPrimaryParticipant = async function(matterId, clientId, token, context)
{
    if (!clientId) return false;   
    const newClientParam = serviceHelper.participantMatterParam(matterId, clientId, common.PARTICIPANT_TYPES['client_primary']);
    const response = (await actionstep.updateMatterParticipant(newClientParam, token, context));
    return response;
}

exports.createSMSGatewayParticipant = async function(matterId, token, context)
{
    //const newClientId = (await participant.createSMSGateway(token, context));
    //if (!newClientId) throw Error('Failed to create new client Id from actionstep');
    const newClientId = process.env.SMS_GATEWAY_ID || 117379;
  
    context.log(`[matter::${matterId}].createSMSGatewayParticipant.createSMSGateway newClientId >>`, newClientId);
    const newSMSGatewayParam = serviceHelper.participantMatterParam(matterId, newClientId, common.PARTICIPANT_TYPES['sms_gateway']);
    const response = await actionstep.updateMatterParticipant(newSMSGatewayParam, token, context);
    context.log(`[matter::${matterId}].createSMSGatewayParticipant.updateMatterParticipant response >>`, JSON.stringify(response));
    return response;
}

exports.createMatterParticipants = async function(matterId, deal, clientId, token, context)
{
    const clientParam = serviceHelper.participantMatterParam(matterId, clientId, common.PARTICIPANT_TYPES['client_primary']);
    context.log(`[deal::${deal.dealId}].createMatterParticipants clientParam >>`, JSON.stringify(clientParam));

    const res = (await actionstep.updateMatterParticipant(clientParam, token, context));
    context.log(`[deal::${deal.dealId}].createMatterParticipants updateMatterParticipant response >>`, JSON.stringify(res));

    const response = (await participant.updateMatter(matterId, deal, token, context));

    context.log(`[deal::${deal.dealId}].createMatterParticipants participant.updateMatter >>`, JSON.stringify(response));
    return response;
};

exports.updatePipedriveStatus = async function(matter, dealId, context)
{
    const xdata = {
        "405427b2fe4ab0809e258b370e9323db6e50d172" : matter.matterName,
        stage_id : common.PD_MATTER_CREATED_STATUS,
        status : "won"
    };
    const res = await pipedrive.updatePipedrive(dealId, xdata, context);
    return res;
}

exports.resetPipedriveAMCLabel = async function(dealId, context)
{
    const xdata = {
        label : ""
    };
    const res = await pipedrive.updatePipedrive(dealId, xdata, context);
    return res;
}

const updateParticipantOCR = async function(ocrParticipant, matterParams, participantType, token, context, objIdx=0)
{    
    logIt2(fa.info, context, `[deal::${matterParams.dealId}].updateParticipantOCR DEBUG ocrParticipant[${participantType}] >`, [JSON.stringify(ocrParticipant)]);
    const searchParam = pdHelper.createOCRParticipantParam(ocrParticipant);
    const res    = await actionstep.searchClient(searchParam, token, context);
    let cli = pdHelper.getClientCredential(res);
    if (!cli){                      
        logIt2(fa.info, context, `[deal::${matterParams.dealId}].updateParticipantOCR param[${participantType}] does-not-exist >>`, [JSON.stringify(searchParam)]);
        const createParam = client.createParticipantFromOCR(ocrParticipant, matterParams.state, participantType, context);                      
        logIt2(fa.info, context, `[deal::${matterParams.dealId}].updateParticipantOCR createParam[${participantType}] >`, [JSON.stringify(createParam)]);

        if (!createParam) {
            return false;
        }

        const res2  = await actionstep.newParticipant(createParam, token, context);
        cli = pdHelper.getClientCredential(res2);
        if (!cli){
            throw new Error('CLIENT_CREDENTIAL_CODE_ERROR');
        }
    }else{
        logIt2(fa.info, context, `[deal::${matterParams.dealId}].updateParticipantOCR Search [${participantType}] response >>`, [JSON.stringify(cli), JSON.stringify(searchParam)]);
    }

    await deleteMatterParticipant(cli.clientId, participantType, matterParams, token, context, objIdx);
    const particpParam  = serviceHelper.participantMatterParam(matterParams.matterId, cli.clientId, participantType);        
    const response      = await actionstep.updateMatterParticipant(particpParam, token, context);
    logIt2(fa.debug, context, `[deal::${matterParams.dealId}].updateParticipantOCR.updateMatterParticipant response[${participantType}]  >>`, [JSON.stringify(response)]);

    const response2 = {
        matterId : matterParams.matterId,
        participantId : cli.clientId,
        client : {...searchParam, participantId : cli.clientId}
    }
    logIt2(fa.debug, context, `[deal::${matterParams.dealId}].updateParticipantOCR.updateMatterParticipant return response[${participantType}]  >`, [JSON.stringify(response2)]);
    return response2;
}

const updateParticipantOCRReview = async function(ocrParticipant, matterParams, participantType, token, context, objIdx=0)
{    
    logIt2(fa.info, context, `[deal::${matterParams.dealId}].updateParticipantOCRReview DEBUG ocrParticipant[${participantType}] >`, [JSON.stringify(ocrParticipant)]);
    const searchParam = pdHelper.createOCRParticipantParam(ocrParticipant);
    
    const response2 = {
        matterId : matterParams.matterId,
        client : {...searchParam, participantId : cli.clientId}
    }
    logIt2(fa.debug, context, `[deal::${matterParams.dealId}].updateParticipantOCRReview.updateMatterParticipant return response[${participantType}]  >`, [JSON.stringify(response2)]);
    return response2;
}

const companySearchId = async function(companyName, matterParams, token, context)
{  
    const companyParam = pdHelper.createCompanyParam(companyName);
    context.log(`[deal::${matterParams.dealId}].companySearchId createCompanyParam >`, JSON.stringify(companyParam));

    // const response = await actionstep.searchClient(companyParam, token, context);
    const response = await actionstep.searchCouncil(companyParam, token, context);    
    const clientId = pdHelper.getActionstepId(response);

    context.log(`[deal::${matterParams.dealId}].companySearchId > searchClient response >>`, JSON.stringify(response));
    return (clientId) ? {matterId : matterParams.matterId, participantId : clientId} : false;
}

const createMatterParticipantsFromOCR = async function(xconfig, participantTypeId, token, context)
{
    const particpParam = serviceHelper.participantMatterParam(xconfig.matterId, xconfig.participantId, participantTypeId);
    const response = await actionstep.updateMatterParticipant(particpParam, token, context);
    context.log(`[matter::${xconfig.matterId}].createMatterParticipantsFromOCR response[${participantTypeId}] >>`, JSON.stringify(response));
}

const createMatterParticipantsFromOCRAllClients = async function(xconfig, participantTypeId, token, context)
{
    const clients = xconfig.clients || [];
    logIt2(fa.info, context, `[matter::${xconfig.matterId}].createMatterParticipantsFromOCRAllClients debug xconfig >`, [JSON.stringify(xconfig)]);
    
    try {
        for (let i = 0; i < clients.length; i++) {
            const el = clients[i];
            const particpParam = serviceHelper.participantMatterParam(xconfig.matterId, el.participantId, participantTypeId);
            const response = await actionstep.updateMatterParticipant(particpParam, token, context);
            context.log(`[matter::${xconfig.matterId}].createMatterParticipantsFromOCRAllClients response[${participantTypeId}] >>`, JSON.stringify(response));    
        }
    } catch (err) {        
        const errMsg   = err.message || 'error';
        logIt2(fa.error, context, `[matterId::${xconfig.matterId}].createMatterParticipantsFromOCRAllClients error response[${participantTypeId}] >`, [errMsg]);
    }
}

const createBstParticipants = async function(bst, dataList, matterParams, token, context)
{ 
    let response;
    const participantType = (bst==='buyer') ? common.PARTICIPANT_TYPES['buyer'] : common.PARTICIPANT_TYPES['seller'];
    let clientList = [];

    context.log(`[dealId::${matterParams.dealId}][${participantType}].createBstParticipants DEBUG`, JSON.stringify(dataList));

    for (let i = 0; i < dataList.length; i++) {
        const participant = dataList[i];
        if(participant){
            const objIndex = i + 1;
            const res = await updateParticipantOCR(participant, matterParams, participantType, token, context, objIndex);
            if(i===0){
                response = res;                
            }
            if (res) {
                clientList.push(res.client);
            }
        }
    }
    return (response && response.matterId) ? {matterId: response.matterId, participantId: response.participantId, clients : clientList } : false;
}

exports.processOCR = async function(matter, deal, token, ocrX, context)
{
    const ocrUpdate = ocrX.ocrUpdate;
    const oid = ocrX.oid;    
    const delParties = ocrX.sourceDeleteParticipants;
    
    const matterParams = {
        matterId  : matter.matterId, 
        dealId    : deal.dealId, 
        state     : deal.state,
        ocrUpdate : ocrUpdate,
        delParties: delParties
    };
    
    let keyDates;
    let genericData;
    let dailyMatterCount = 0;

    const bstText = (deal.bstText||'').toLowerCase(); //Buyer/Seller/Transfer
    const pool = await database.createPoolConnection();
    const xpending = {
        dealId : deal.dealId,
        matterId : matter.matterId, 
        matterName : matter.matterName, 
        matterType    : deal.matterTypeId,
        pipedriveDone : true
    };

    logIt2(fa.info, context, `[deal::${matterParams.dealId}].processOCR bst:${bstText} debug ocrX >`, [JSON.stringify(ocrX)]);
    if (!ocrUpdate){
        await database.updatePendingMatter(pool, xpending);
        dailyMatterCount = await database.createDailyMatter(pool, matter.matterId, matter.matterName);
    }
    //ocr_update
    const xOcrData = await database.retrieveOcrDB(pool, deal.dealId, oid, context);
    const nonOcr   = (xOcrData.subject && (xOcrData.subject == 'ContractDraftingForm' || xOcrData.subject == 'PropertyTransferForm')) ? true : false;
    let ocrData;
    let ocrMCFields = false;
    const dbFileRef = (xOcrData && xOcrData.file_ref) || '';
    const webFormId = (xOcrData && xOcrData.id) || 0;

    // await database.releasePoolConnection(pool);
    logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR debug xOcrData (nonOcr :${nonOcr}) >`, [JSON.stringify(xOcrData)]);

    if (xOcrData){
        if(nonOcr){
            ocrData  =  {...xOcrData.form_fields, dealState : deal.state};
        }else{            

            const ocrFieldsDebug = Object.keys(xOcrData.ocr_update||{});
            const ocrFields = (ocrUpdate && Object.keys(xOcrData.ocr_update||{}).length > 0) ?  xOcrData.ocr_update : xOcrData.ocr_fields;           

            logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR debug xOcrData ocrFields >`, [JSON.stringify(ocrFields), JSON.stringify(ocrFieldsDebug)]);
            ocrData  =  {...ocrFields, dealState : deal.state, mcdate : xOcrData.mcdate, ocrReview : ocrUpdate};
            if (deal.state == 'QLD' && ocrUpdate){
                ocrData  =  {...ocrFields, dealState : deal.state, ctype : xOcrData.form_fields.ctype || '', mcdate : xOcrData.mcdate, ocrReview : ocrUpdate};

            }else if (deal.state == 'VIC'){                
                ocrData = myFilterOcr(ocrData);
                logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR debug ocrData (nonOcr :${nonOcr}) filterOcr >`, [JSON.stringify(ocrData)]);
            }
        }        
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR debug ocrData Final >`, [JSON.stringify(ocrData)]);

        let buyer1,buyer2,buyer3,buyer4;
        let buyerx, sellerx;
        let seller1,seller2,seller3,seller4,agentOnSell;
        let solicitorOnBuy,solicitorOnSell,agentOnBuy;
        let corporate;
        
        if (nonOcr){
                
            seller1          = nonocr.extractSeller1(ocrData, context);
            seller2          = nonocr.extractSeller2(ocrData, context);
            seller3          = nonocr.extractSeller3(ocrData);
            seller4          = nonocr.extractSeller4(ocrData);
            agentOnSell      = nonocr.extractAgentOnSell(ocrData);
            corporate        = nonocr.extractCorporateBody(ocrData);            
        }else{
            buyer1           = ocr.extractBuyer1(ocrData, context);
            buyer2           = ocr.extractBuyer2(ocrData, context);
            buyer3           = ocr.extractBuyer3(ocrData, context);
            buyer4           = ocr.extractBuyer4(ocrData, context);

            seller1          = ocr.extractSeller1(ocrData);
            seller2          = ocr.extractSeller2(ocrData, context);
            seller3          = ocr.extractSeller3(ocrData, context);
            seller4          = ocr.extractSeller4(ocrData, context);
            agentOnSell      = ocr.extractAgentOnSell(ocrData, context);
            keyDates         = ocr.extractKeyDates(ocrData);
            genericData      = ocr.extractGenericData(ocrData, context);

            solicitorOnBuy  = ocr.extractSolicitorOnBuy(ocrData);
            agentOnBuy      = agentOnSell;
            solicitorOnSell = ocr.extractSolicitorOnSell(ocrData);
            ocrMCFields     = ocr.extractOCRMatterCollection(ocrData, context);
        }
        
        let matterResponse1,matterResponse11;
        
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR DEBUG@0 BUYER1|2`, [JSON.stringify(buyer1), JSON.stringify(buyer2)]);
        
        if (!buyer2 && (buyer1 && buyer1.option_extra && buyer1.option_extra.person)){
            buyerx = {...buyer1};
            buyerx.name  = (buyer1.option_extra && buyer1.option_extra.person) ? buyer1.option_extra.person : '';
            buyerx.email = (buyer1.option_extra && buyer1.option_extra.email)  ? buyer1.option_extra.email  : '';
        }
        
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR DEBUG@1`, [JSON.stringify(buyerx), JSON.stringify(buyer3), JSON.stringify(buyer4), JSON.stringify(seller1), JSON.stringify(seller2) ]);

        if (!seller2 && (seller1 && seller1.option_extra && seller1.option_extra.person)){
            sellerx =  {...seller1};
            sellerx.name  = (seller1.option_extra && seller1.option_extra.person) ? seller1.option_extra.person : '';
            sellerx.email = (seller1.option_extra && seller1.option_extra.email)  ? seller1.option_extra.email  : '';
            sellerx.address = (seller1.option_extra && seller1.option_extra.address)  ? seller1.option_extra.address  : '';
        }
        
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR DEBUG@0 sellerX|seller3|seller4`, [JSON.stringify(sellerx), JSON.stringify(seller3), JSON.stringify(seller4)]);       
        // aoS-agentOnSell|sosB-solicitorOnBuy|soS-solicitorOnSell
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR DEBUG@0 aoS|soB|soS|corp`, [JSON.stringify(agentOnSell), JSON.stringify(solicitorOnBuy), JSON.stringify(solicitorOnSell), JSON.stringify(corporate)]);
        logIt2(fa.debug, context, `[deal::${matterParams.dealId}].processOCR DEBUG@0 kDates|generic|ocrextend`, [JSON.stringify(keyDates), JSON.stringify(genericData), JSON.stringify(ocrMCFields)]);

        if(!nonOcr){
            matterResponse1  = await createBstParticipants('buyer',  [buyer1,buyer2||buyerx,buyer3,buyer4],     matterParams, token, context);
        }
        matterResponse11 = await createBstParticipants('seller', [seller1,seller2||sellerx,seller3,seller4], matterParams, token, context);        
        
        const matterParamRef = {buyer: (matterResponse1 && matterResponse1.clients) || {}, seller: (matterResponse11 && matterResponse11.clients) || {}, transfer: {}};
        const fileReference  = await updateMatterReference(matter.matterId, matterParamRef, bstText, dbFileRef, ocrUpdate, token, context);
        if (!ocrUpdate && webFormId){
            await database.updateFileRef(pool, matter.matterId, webFormId, fileReference, context);
        }
        await database.releasePoolConnection(pool);

        if(bstText === 'buyer'){
            if (matterResponse1 && matterResponse1.matterId){
                await deleteMatterParticipantAll(matterResponse1, common.PARTICIPANT_TYPES['client'], matterParams, token, context);
                await createMatterParticipantsFromOCRAllClients(matterResponse1, common.PARTICIPANT_TYPES['client'], token, context);
            }
            if (agentOnBuy){
                const matterResponse2 = await updateParticipantOCR(agentOnBuy, matterParams, common.PARTICIPANT_TYPES['agent_office'], token, context);
                if (matterResponse2 && matterResponse2.matterId){
                    await deleteMatterParticipant(matterResponse2.participantId, common.PARTICIPANT_TYPES['agent_primary_contact'], matterParams, token, context);
                    await createMatterParticipantsFromOCR(matterResponse2, common.PARTICIPANT_TYPES['agent_primary_contact'], token, context);
                }
            }
            // solicitorOnBuy (otherside --> Selling)
            if (solicitorOnSell){
                const matterResponse3 =await updateParticipantOCR(solicitorOnSell, matterParams, common.PARTICIPANT_TYPES['solicitor'], token, context);
                if (matterResponse3.matterId){
                    await deleteMatterParticipant(matterResponse3.participantId, common.PARTICIPANT_TYPES['solicitor_primary_contact'], matterParams, token, context);
                    await createMatterParticipantsFromOCR(matterResponse3, common.PARTICIPANT_TYPES['solicitor_primary_contact'], token, context);
                }
            }
        }else  if(bstText === 'seller'){
            if (matterResponse11 && matterResponse11.matterId){
                await deleteMatterParticipantAll(matterResponse11, common.PARTICIPANT_TYPES['client'], matterParams, token, context);
                await createMatterParticipantsFromOCRAllClients(matterResponse11, common.PARTICIPANT_TYPES['client'], token, context);
            }
            if (agentOnSell){
                const matterResponse12 = await updateParticipantOCR(agentOnSell, matterParams, common.PARTICIPANT_TYPES['agent_office'], token, context);
                if (matterResponse12 && matterResponse12.matterId){
                    await deleteMatterParticipant(matterResponse12.participantId, common.PARTICIPANT_TYPES['agent_primary_contact'], matterParams, token, context);
                    await createMatterParticipantsFromOCR(matterResponse12, common.PARTICIPANT_TYPES['agent_primary_contact'], token, context);
                }
            }
            // solicitorOnBuy (otherside --> Buying)
            if (solicitorOnBuy){
                const matterResponse13 = await updateParticipantOCR(solicitorOnBuy, matterParams, common.PARTICIPANT_TYPES['solicitor'], token, context);
                if (matterResponse13.matterId){

                    await deleteMatterParticipant(matterResponse13.participantId, common.PARTICIPANT_TYPES['solicitor_primary_contact'], matterParams, token, context);
                    await createMatterParticipantsFromOCR(matterResponse13, common.PARTICIPANT_TYPES['solicitor_primary_contact'], token, context);
                }
            }

            if (corporate){
                await updateParticipantOCR(corporate, matterParams, common.PARTICIPANT_TYPES['body_corporate'], token, context);
            }
        }
        
        if (genericData && genericData.council){
            let matterCouncil;
           
            const councilWideSearch = formatV2CouncilName(genericData.council);
            context.log("process.OCR DEBUG COUNCIL-INSIDE > ", genericData.council, councilWideSearch);

            for (let idx2 = 0; idx2 < councilWideSearch.length; idx2++) {
                const xCouncilName = councilWideSearch[idx2];
                matterCouncil = await companySearchId(xCouncilName, matterParams, token, context);
                if (matterCouncil){
                    await deleteMatterParticipant(matterCouncil.participantId, common.PARTICIPANT_TYPES['council'], matterParams, token, context);
                    await createMatterParticipantsFromOCR(matterCouncil, common.PARTICIPANT_TYPES['council'], token, context);
                    break;
                }
            }
        }
    }
    
    const ocrResponse = {
        dailyMatterCount : dailyMatterCount,
        keyDates         : (keyDates)    ? keyDates : false,
        genericData      : (genericData) ? genericData : false,
        ocrExtended      : ocrMCFields,
        ocrReview        : ocrUpdate
    }
    logIt2(fa.info, context, `[deal::${matterParams.dealId}].processOCR dailyMatterCount::${dailyMatterCount} > ocrResponse >`, [JSON.stringify(ocrResponse)]);    
    return ocrResponse;
}

exports.notifyTeams = async function(matter, deal, address, dailyMatterCount, context)
{
    const dev_testing_env = `${process.env.DEV_TESTING_ENV}` || 'disable';
    const salesPerson = deal.owner || 'OPEN';
    const xdata = {
        matterName      : matter.matterName,
        matterId        : matter.matterId, 
        clientName      : deal.personName,
        matterTypeName  : deal.matterTypeName,
        concierge       : salesPerson,
        postcode        : address.post_code
    };
    if (!dailyMatterCount || dailyMatterCount===undefined || dailyMatterCount==null){
        throw Error('Failed fetching Database records for Matter Counter for the day!');
    }
    
    logIt2(fa.info, context, `[notifyTeams] for matterId: ${matter.matterId}`,[dev_testing_env]);
    await teams.notifyTeamsOfMatter(xdata, dailyMatterCount, context);
}

exports.notifyTeamsPscem = async function(matter, deal, context)
{
    const xdata = {
        matterId        : matter.matterId, 
        clientName      : deal.personName,
        matterTypeName  : deal.matterTypeName,
    };
    
    logIt2(fa.info, context, `[notifyTeamsPscem] for matterId: ${matter.matterId}`,[xdata]);
    await teams.notifyTeamsPSCEM(xdata, context);
}

exports.notifyTeamsPscemError = async function(dealId, matterId, errMsg, errStack, context)
{   
    const xdata = {
        matterId    : matterId,
        dealId      : dealId,
        errorMsg    : errMsg,
        errorStack  : errStack || ''
    };
    await teams.notifyTeamsPSCEMError(xdata, context);
}

exports.notifyTeamsAMCError = async function(dealId, matterId, errMsg, errStack, context)
{   
    const xdata = {
        matterId    : matterId,
        dealId      : dealId,
        errorMsg    : errMsg,
        errorStack  : errStack || ''
    };   
    await teams.notifyTeamsAMCError(xdata, context);
}

const uploadedDocument = function(res, matterId, context)
{
    logIt2(fa.debug, context, `[matter::${matterId}].uploadedDocument status >>`, [JSON.stringify(res.data)] );
    if (res.data.files.status == 'Uploaded'){
        return res.data.files.id;
    }else{
        throw Error("Failed uploading file in Actionstep!");
    }
}

exports.uploadDocsPipedriveToActionstep = async function(matterId, files, token, context)
{
    logIt2(fa.debug, context, `[matter::${matterId}].uploadDocsPipedriveToActionstep files >>`, [JSON.stringify(files)] );
    files.forEach(async docX => {
        const payload = await pipedrive.getData(docX.fileId, context);
        const response = await actionstep.uploadDocument(docX, payload, token, context);
        const actionstepFileId = uploadedDocument(response, matterId, context);
        await actionstep.linkDocumentToMatter(matterId, actionstepFileId, docX.filename, token, context);
    });   
}

exports.processDealExist = async function(dealId, context)
{
    const pool = await database.createPoolConnection();
    const dealExist = await database.checkDealMatterPending(pool, dealId);
    logIt2(fa.info, context, `[deal::${dealId}].processDealExist isDealExist => ${dealExist}`);
    if (dealExist===false){
        await database.createDealPendingMatter(pool, dealId);
    }
    await database.releasePoolConnection(pool);
    return dealExist;
}

exports.processDealwPayload = async function(dealId, payload, context)
{
    const pool = await database.createPoolConnection();
    const dealExist = await database.checkDealMatterPending(pool, dealId);    
    logIt2(fa.info, context, `[deal::${dealId}].processDealwPayload isDealExist => ${dealExist}`);

    if (dealExist===false){
        await database.createDealPendingMatterPayload(pool, dealId, payload);
    }
    await database.releasePoolConnection(pool);
    return dealExist;
}

exports.getDealLastStage = async function(dealId, context)
{
    const pool = await database.createPoolConnection();
    const mcInfo = await database.checkDealLastStage(pool, dealId);
    logIt2(fa.info, context, `[deal::${dealId}].getDealLastStage mcinfo =>`, [JSON.stringify(mcInfo)]);

    if (!mcInfo){
        await database.createDealPendingMatter(pool, dealId);
    }
    await database.releasePoolConnection(pool);
    return mcInfo;
}

exports.getMatterSource = async function(dealId, context)
{
    const pool = await database.createPoolConnection();
    const mcSource = await database.getMatterSource(pool, dealId);
    logIt2(fa.info, context, `[deal::${dealId}].getMatterSource mcSource =>`, [JSON.stringify(mcSource)]);
    await database.releasePoolConnection(pool);
    return mcSource;
}

exports.createDocumentMap = async function(blobName, container, req)
{
    const docUrl = `${process.env.AZURE_STORAGE_ENDPOINT}/${container}/${blobName}`
    const document = serviceHelper.getBlobName2(docUrl);
    const pool = await database.createPoolConnection();
    let dealId = (req.body && req.body.dealid) || req.body.deal_id;
    const code = await database.createDocumentMap(pool, dealId, document, docUrl);
    await database.releasePoolConnection(pool);
    return `${code}`;
}

exports.sendMCNotesToPipedrive = async function(matter, deal, person, address, context)
{
    const content = `Matter Created with Ref: ${matter.matterName}
                    Client: ${person.firstName} ${person.lastName}
                    ${deal.matterTypeName}
                    Concierge: OPEN
                    Postcode: ${address.post_code}
                    **** A.M.C SYSTEM GENERATED ****`;
    const xdata = {
        deal_id : deal.dealId,
        content : content,
        pinned_to_deal_flag : 1
    }
    const response = await pipedrive.postNotes(xdata, context);
    logIt2(fa.debug, context, `[deal::${deal.dealId}].sendMCNotesToPipedrive response >>`, [JSON.stringify(response)] );
}

exports.sendNotificationToPipedrive = async function(matterId, dealId,  status, message, context)
{   
    const content = message;
    const xdata = {
        deal_id : dealId,
        content : content,
        pinned_to_deal_flag : 1
    }
    const response = await pipedrive.postNotes(xdata, context);
    logIt2(fa.debug, context, `[deal::${dealId}].sendNotificationToPipedrive response >>`, [JSON.stringify(response)]);
}

exports.getMapDocuments = async function(relevantFiles, context)
{
    let xdocs = [];
    const pool = await database.createPoolConnection();
    const docs = await database.getMapDocumentURL(pool, relevantFiles, context);
    await database.releasePoolConnection(pool);
    if (docs){
        for (let i = 0; i < docs.length; i++) {
            const doc = docs[i];
            xdocs.push(doc.url);
        }
    }
    return (xdocs.length > 0) ? xdocs : false;
}

exports.createSalesPersonParticipant = async function(matterId, deal, token, context)
{
    const res1 = await pipedrive.getPipedriveDeal(deal.dealId, context);
    logIt2(fa.debug, context, `[deal::${deal.dealId}].createSalesPersonParticipant response >>`, [(res1.user_id) ? res1.user_id : res1]);

    if (res1.user_id){
        const person = {
            isCompany: 'F',
            companyName: '',
            firstName: '',
            lastName: '',
            middleName: '',
            email: res1.user_id.email
        };
        const personParam = pdHelper.createClientParam(person);
        logIt2(fa.debug, context, `[deal::${deal.dealId}].createSalesPersonParticipant personParam >>`, [JSON.stringify(personParam)] );

        const res2 = await actionstep.searchClient(personParam, token, context);
        if (res2){
            let salesPerson = pdHelper.getClientCredential(res2, true);
            if (salesPerson && salesPerson.clientId){
                const particpParam = serviceHelper.participantMatterParam(matterId, salesPerson.clientId, common.PARTICIPANT_TYPES['sales_person']);
                const res3 = await actionstep.updateMatterParticipant(particpParam, token, context);
                logIt2(fa.debug, context, `[deal::${deal.dealId}].createSalesPersonParticipant response >>`, [JSON.stringify(res3)] );
            }
        }
    }
}


exports.resetDailyMatter = async function(context)
{   
    let dailyMatterStatus = {rc1 : 0, rc2 : 0};
    const pool = await database.createPoolConnection();    
    const rc1  = await database.getDailyMatterCount(pool);
    context.log("[resetDailyMatter] getDailyMatterCount(+) rc1 count:", rc1);

    await database.resetPendingMatter(pool);
    const rc2 = await database.getDailyMatterCount(pool);     
    context.log("[resetDailyMatter] getDailyMatterCount(+) rc2 count:", rc2);
    
    await database.releasePoolConnection(pool);    
    dailyMatterStatus.rc1 = +rc1;
    dailyMatterStatus.rc2 = +rc2;

    return dailyMatterStatus;
}

exports.updateMCStep = async function(dealId, stepId, mcInfo, context)
{
    const pool = await database.createPoolConnection();    
    const res = await database.updateStepInPendingMatter(pool, dealId, stepId, mcInfo);
    await database.releasePoolConnection(pool);
    return res;
}

const isDeleteOcrParticipants = function(idSource, ocrListParty=[])
{
    const arrId = (idSource) ? idSource.split('--') : false;
    if (arrId && arrId.length === 3){
        const partiTypeId = parseInt(arrId[1]);
        if (ocrListParty.includes(partiTypeId)){
            return true;
        }
    }
    return false;
}

const isDeleteOcrParticipantSource = function(idSource, ocrListParty=[], participantNumber=1)
{
    const arrId = (idSource) ? idSource.split('--') : false;
    if (arrId && arrId.length === 3){
        const partiTypeId = parseInt(arrId[1]);
        if (ocrListParty.includes(partiTypeId)){
            return {'partyType' : partiTypeId, 'participantId' : parseInt(arrId[2]), 'participantNumber' : participantNumber };
        }
    }
    return false;
}

exports.deleteMatterParticipants = async  function(matterId, token, context, ocrParticipantRoles=[])
{    
    const res = await actionstep.matterParticipants(matterId, token, context);    
    logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipants DEBUG PARTICIPANTS >>`, [res.actionparticipants] );
    const listParties = (res && res.actionparticipants) || [];
    for (let i = 0; i < listParties.length; i++) {
        const el = listParties[i];
        const matterParticipantSrc = ((el.id).indexOf('--') !=-1) ? el.id : false;

        const deleteParty = (ocrParticipantRoles.length > 0) ? isDeleteOcrParticipants(el.id, ocrParticipantRoles) : false;

        if (matterParticipantSrc && (ocrParticipantRoles.length === 0) || deleteParty){
            const res = await actionstep.deleteParticipantByString(matterParticipantSrc, token, context);
            logIt2(fa.debug, context, `[matterId::${matterId}].deleteMatterParticipants DEBUG id: ${el.id} rc: response >>`, [res.status] );
        }
    }
}

exports.deleteMatterParticipantSource = async  function(matterId, token, context, ocrParticipantRoles=[])
{    
    let deleteSource = [];
    const res = await actionstep.matterParticipants(matterId, token, context);    
    logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipants DEBUG PARTICIPANTS >>`, [res.actionparticipants] );
    const listParties = (res && res.actionparticipants) || [];
    for (let i = 0; i < listParties.length; i++) {
        const el = listParties[i];
        const deleteParty = isDeleteOcrParticipantSource(el.id, ocrParticipantRoles, el.participantNumber);
        if (deleteParty){
            deleteSource.push(deleteParty);
        }
    }
    logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipants DEBUG deleteSource >`, [JSON.stringify(deleteSource)]);
    return deleteSource;
}

 exports.checkMatterData = async function(matterId, context)
 {
    const pool = await database.createPoolConnection();    
    const res = await database.checkMatterContractUpdates(pool, matterId)
    context.log("DEBUG checkMatterData => ", res)
    await database.releasePoolConnection(pool);
    return res;
 }
 
 exports.getDBMatterDetails = async function(matterId, context)
 {
    const pool = await database.createPoolConnection();    
    const res  = await database.dbMatterDetails(pool, matterId);
    let mtInfo = {matterId: parseInt(matterId), AMCMatter: false, msg : 'Matter not found in AMC created matters!'};
    await database.releasePoolConnection(pool);

    if (res){
        const mt = matterHelper.getMatterState(res.matterType);
        mtInfo = {matterId: parseInt(matterId), ...res, ...mt, AMCMatter: true};        
    }        
    logIt2(fa.debug, context, `[matterId::${matterId}].getDBMatterDetails res >>`, [JSON.stringify(res),JSON.stringify(mtInfo)]);
    return mtInfo;
 }

 const getDesc = function(columns, index)
 {
    const fieldData = (columns || []).find((row) => {return (index == row.web_form_column) ? row : null;});
    const desc = (fieldData && fieldData.description) || false;
    return desc;
 }

 const createCSVBuffer = function(recordData, colsDescriptions)
 {
    if (!recordData){
        return false;
    }

    let csvData = '';
    const h1 = "OCR FIELDs".padEnd(30, ' ');
    const h2 = "OCR VALUEs".padEnd(50, ' ');
    const header = `${h1}\t\t, ${h2}\t\t\n\n`;
    csvData = header;

    Object.keys(recordData).forEach(function(key) {
        const xDesc = getDesc(colsDescriptions, key) || key;
        const row1  = (xDesc||'').padEnd(30,' ');
        const trow2 = ((recordData[key]||'').replace(',',''));
        const row2  = trow2.replace('unselected', 'No').replace('selected', 'Yes').padEnd(50,' ');
        const line  = `${row1}\t\t, ${row2}\t\t\n`;
        csvData +=line;
    });
    return Buffer.from(csvData, 'utf-8');
 }
 

 exports.processOcrResultUpload = async function(dealId, context)
 {
    let colsDescriptions = [];
    let recordData  = {};
    let xFormData   = {};
    const pool = await database.createPoolConnection();    
    const res  = await database.getOcrDBResult(pool, dealId, context);
    if (!res){
        await database.releasePoolConnection(pool);
        return false;    
    }
    recordData       = res.ocr_fields || {};
    xFormData        = res.form_fields || {};
    colsDescriptions = await database.getColumnsMappingDescription(pool, context);    
    await database.releasePoolConnection(pool);

    const csvBuffer = createCSVBuffer(recordData, colsDescriptions);    
    if (!csvBuffer){
        return false;
    }
   
    const xfilname = (xFormData.contractUrl) ? path.basename(xFormData.contractUrl) : false;                                
    let xdocFile;
    if (xfilname){
        xdocFile = (xfilname||'').toLowerCase().replace('.pdf', `-ocr-result-${dealId}.csv`);
    }else{
        xdocFile = `contractdoc_ocr-result-${dealId}.csv`;
    }    
    logIt2(fa.debug, context, `[dealId::${dealId}].processOcrResultUpload recordData >>`, [JSON.stringify(recordData)]);
    return {payload: csvBuffer, filename: xdocFile, docType: 'application/csv'};
 }
 
exports.uploadASLinkMatter = async function(matterId, ocrResult, token, context)
{
    logIt2(fa.debug, context, `[matter::${matterId}].uploadASLinkMatter ocrResult >>`, [JSON.stringify(ocrResult)] );    
    const docX = {filename : ocrResult.filename, docType: ocrResult.docType};

    const response = await actionstep.uploadDocument(docX, ocrResult.payload, token, context);
    const actionstepFileId = uploadedDocument(response, matterId, context);
    await actionstep.linkDocumentToMatter(matterId, actionstepFileId, docX.filename, token, context);    
}

exports.getPipedriveDeal = async function(dealId, context)
{
    const pData = await pipedrive.getPipedriveDeal(dealId, context);
    const pDeal = pdDealDetails(pData, context);
    return pDeal;
}

exports.getActionstepMatterDetails = async function(matterId, token, context)
{
    const res = await actionstep.matterDetails(matterId, token, context);
    if (res && res.actions){
        const matterName = res.actions.name || '';
        let matterTypeId = res.actions.links['actionType'];

        if (matterName.indexOf('-SEL-') !=-1){
            matterTypeId = `${matterTypeId}01`;
        }else if (matterName.indexOf('-RPT-') !=-1){
            matterTypeId = `${matterTypeId}02`;
        }
        const matter = {matterId: matterId, matterName: matterName, matterType: matterTypeId };
        context.log(`[matterId:${matterId} getActionstepMatterDetails] debug matter >`, JSON.stringify(matter));
        return matter;
    }
    return false;
}

exports.verifyActionstepMatterDetails = function(matterId, matter, context)
{   
   let mt;
   let mtInfo = {};     
   if (!matter){
        mtInfo = {matterId: parseInt(matterId), AMCMatter: false, msg : 'Matter not found!'};
   }else{
        mt     = matterHelper.getMatterState(parseInt(matter.matterType));
        mtInfo = {matterId: parseInt(matterId), ...mt, AMCMatter: true};
   }   
   logIt2(fa.debug, context, `[matterId::${matterId}].verifyActionstepMatterDetails res >>`, [JSON.stringify(mtInfo)]);
   return mtInfo;
}

exports.getOcrFields = async function(state, context)
{
   const pool = await database.createPoolConnection();    
   const res = await database.getOcrFields(pool, state, context)
   let arrFields = {};      
   let rowFields = [];
   let prevCategory;
   
   const fieldRows = res.rows || [];
   await database.releasePoolConnection(pool);
   
   const planType = serviceHelper.getPlanTypeList(context);
   if (planType){
        arrFields["planType"] = planType[state];
   }
   
   for (let i = 0; i < fieldRows.length; i++) {
       const el = fieldRows[i];
       const category = el.category;
       const rowLine = {type: el.ocr_field_type, label: el.description, ph: el.ocr_field_hint || '', key: el.web_form_column};

       if ( i > 0 && category !=prevCategory){
           arrFields[prevCategory] = [...rowFields];
           rowFields.length = 0;
           rowFields = [];
       }
       rowFields.push(rowLine);
       prevCategory = category;
   }
   arrFields[prevCategory] = [...rowFields];
   return arrFields;
}

const myFilterOcr = function(ocrData)
{
    ocrData.b_sol_email       = filterOcr(ocrData.b_sol_email,'email');
    ocrData.b_sol_address     = filterOcr(ocrData.b_sol_address, 'address');
    ocrData.b_agent_address   = filterOcr(ocrData.b_agent_address, 'address');
    ocrData.s_agent_address1  = filterOcr(ocrData.s_agent_address1, 'address');
    ocrData.b_sol_name        = filterOcr(ocrData.b_sol_name);
    ocrData.s_agent_name      = filterOcr(ocrData.s_agent_name);
    ocrData.seller1_name      = filterOcr(ocrData.seller1_name, 'name');
    ocrData.buyer1_name       = filterOcr(ocrData.buyer1_name, 'name');
    ocrData.buyer1_email      = filterOcr(ocrData.buyer1_email,'email');
    ocrData.buyer1_address    = filterOcr(ocrData.buyer1_address, 'address');
    ocrData.property_address1 = filterOcr(ocrData.property_address1, 'address');
    ocrData.s_sol_name        = filterOcr(ocrData.s_sol_name);
    ocrData.s_sol_address     = filterOcr(ocrData.s_sol_address, 'address');
    ocrData.b_agent_name      = filterOcr(ocrData.b_agent_name);
    ocrData.b_agent_email     = filterOcr(ocrData.b_agent_email,'email');
    ocrData.b_agent_phone     = filterOcr(ocrData.b_agent_phone);
    ocrData.s_agent_email     = filterOcr(ocrData.s_agent_email,'email');
    ocrData.s_agent_phone     = filterOcr(ocrData.s_agent_phone);
    ocrData.s_sol_phone       = filterOcr(ocrData.s_sol_phone);
    ocrData.s_sol_email       = filterOcr(ocrData.s_sol_email,'email');
    ocrData.b_sol_phone       = filterOcr(ocrData.b_sol_phone);
    ocrData.s_agent_mobile    = filterOcr(ocrData.s_agent_mobile);      
    return ocrData;  
}


exports.getOcrData = async function(matterId, hash, context)
 {
    let recordData;
    
    const pool = await database.createPoolConnection();
    const res  = await database.getOcrUpdateData(pool, matterId, hash, context);
    
    context.log("DEBUG getOcrData rs", res);

    await database.releasePoolConnection(pool);
    let jsonResponse = {"status" : "ongoing", "message" : "processing", "data" : {}};

    if (res){
        const ocrFields  = res.ocr_update || {};
        const state      = (res.state || '').toUpperCase();
        const mcdate     = res.mcdate || '';
        const ctype      = res.ctype  || '';
        const mtypeName  = res.mattertypename  || '';
        const ocrStatus  = (res.ocr_status||'').trim();
        const fileRef    = res.file_ref  || '';
        const dealId     = res.deal_id;

        if (ocrStatus == 'succeeded'){            

            recordData       = (Object.keys(ocrFields).length === 0) ?  res.ocr_fields : ocrFields;
            const xObjectId  = `${res.id}m${res.deal_id}`
            recordData       = (Object.keys(recordData).length > 0) ? {objectId: xObjectId, mcdate : mcdate, ctype : ctype, dealState : state, ...recordData} : {};
            
            if (state == 'VIC'){
                recordData   = myFilterOcr(recordData);
            }
            
            // const ocrResponse = {
            //     keyDates     : ocr.extractKeyDates(recordData),
            //     genericData  : ocr.extractGenericData(recordData),
            //     ocrExtended  : ocr.extractOCRMatterCollection(recordData, context)
            // }
            // context.log("DEBUG <<<<<<<<<<<<<< ocrExtended >>>>>>>>>> ", ocrResponse);

            const matterParams = {
                matterId: matterId,
                dealId: dealId,
                state: state
            };

            const buyerList  = getClients('buyer',  recordData, matterParams, context);
            const sellerList = getClients('seller', recordData, matterParams, context);
            const matterParamRef = {buyer: buyerList || {}, seller: sellerList || {}, transfer: {}};
            const bstText = pdHelper.getBST(mtypeName);
            const funcTag = 'getocrdata';
            
            context.log(`DEBUG <<<<<<<<<<<<<< updateMatterReference <${mtypeName}> <${bstText}>>>>>>>>> `, mtypeName, matterParamRef);

            const fileReference  = await updateMatterReference(matterId, matterParamRef, bstText, funcTag, false, null, context);
            
            let deal;
            // let ocrObjects          = await collection.pdCollectionDataSourceExtended(deal, ocrResponse, null, context);
            const ocrObjects = pxcollect.processCollectionFields(recordData, state, context);

            const asFieldList       = asfields.actionstepFieldsMap || {};
            const asDateType        = asfields.actionstepFieldsType.dateType || [];
            const specialFields     = asfields.specialLogicFields || {};            
            const tenants_apply     = asfields.customFields.tenants_apply;

            recordData['file_reference'] = fileReference||'';
            context.log("DEBUG <<<<<<<<<<<<<< ocrObjects >>>>>>>>>> ", ocrObjects);
            for (const uiKey in ocrObjects) {  
                 if (ocrObjects.hasOwnProperty(uiKey)){
                    recordData[uiKey] = ocrObjects[uiKey];
                 }
            }
            recordData['property_council_area'] = recordData['property_council_area'] || recordData['property_local_gov'];
            
            jsonResponse = {"status" : "done", "data" : recordData, "message" : "success" };

        }else{
            jsonResponse = {"status" : "done", "message" : "ocrfailed", "data" : {}};
        }
    }
    

    logIt2(fa.debug, context, `[matterId::${matterId}].getOcrData res >>`, [JSON.stringify(res), JSON.stringify(jsonResponse)]);
    return jsonResponse;
 }

 exports.postOcrData = async function(matterId, ocrData, context)
 {
    const oid     = ocrData["objectId"] || '';    
    logIt2(fa.info, context, `[matterId::${matterId}].postOcrData (oid:${oid}) param >`, [JSON.stringify(ocrData)]);

    const arrId = (oid||'').split('m');
    delete ocrData["objectId"];

    if (arrId.length !=2){        
        throw new Error('objectId incomplete! '+oid);
    } 
    const oidX   = arrId[0];
    const dealId = arrId[1];
    const pool   = await database.createPoolConnection();
    (await database.postOcrUpdates(pool, matterId, oidX, ocrData, context));
    (await database.releasePoolConnection(pool));
    return { "dealId" : dealId, "oid" : oidX};
 }

const deleteMatterParticipantAll = async function(parties, participantType, matterParams, token, context)
{
    const partiescp = parties.clients || [];
    logIt2(fa.info, context, `[matterId::${matterParams.matterId}].deleteMatterParticipantAll participantType:${participantType}>`, [JSON.stringify(partiescp)]);

    for (let i = 0; i < partiescp.length; i++) {
        const el = partiescp[i];
        await deleteMatterParticipant(el.participantId, participantType, matterParams, token, context);
    }
}

const deleteMatterParticipant = async function(participantId, participantType, matterParams, token, context, objIdx=0)
{
    const delParties = matterParams.delParties || [];
    const matterId   = matterParams.matterId;
    logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipant objIdx:${objIdx} participantId:${participantId}/${participantType}>`, [JSON.stringify(delParties)]);
    
    try {
        if (matterParams.ocrUpdate && delParties.length > 0){
            let party;
            if ([155, 157].includes(participantType) && objIdx > 0){
                party = delParties.find((row) => {return (participantType === row.partyType && objIdx === row.participantNumber) ? row : null;});
            }else{
                party = delParties.find((row) => {return (participantType === row.partyType) ? row : null;});
            }        
            logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipant delete to proceed >`, [JSON.stringify(party)]);
            if (party &&  party.participantId){
                const res = await actionstep.deleteParticipant(matterId, participantType, party.participantId, token, context);
                logIt2(fa.info, context, `[matterId::${matterId}].deleteMatterParticipant DEBUG participantId:${participantId} response >>`, [res.status] );
            }        
        }   
    } catch (err) {
        const errMsg   = err.message || 'error';
        logIt2(fa.error, context, `[matterId::${matterId}].deleteMatterParticipant error >>`, [errMsg] );
    }    
}

const updateMatterReference = async function(matterId, clients, bstText, dbFileRef, ocrUpdate, token, context)
{
    const prefixRef = {
        "buyer" : "P/F",
        "seller" : "S/T",
        "transfer" : "T/t",
    }
   
    let sellerRefList= [];
    let buyerRefList = [];
    const sellerClient   = clients.seller || [];
    const buyerClient    = clients.buyer || [];
    
    if (ocrUpdate && (dbFileRef||'').length > 0 )
    {
        const matterNameParam = {
            actions : {
                reference : dbFileRef
            }            
        };
        logIt2(fa.info, context, `[matterId::${matterId}].updateMatterReference ocrUpdate=t dbFileRef:${dbFileRef} (+)`, [matterNameParam] );
        await actionstep.updateMatter(matterId, matterNameParam, token, context);
        return dbFileRef;
    }

    logIt2(fa.info, context, `[matterId::${matterId}].updateMatterReference xfileRef:${dbFileRef} sel (+)`, sellerClient );
    logIt2(fa.info, context, `[matterId::${matterId}].updateMatterReference xfileRef:${dbFileRef} buy (+)`, buyerClient );
    
    for (let idx = 0; idx < sellerClient.length; idx++){
        const el = sellerClient[idx];
        if (el){
            const first = (el.firstName) ? el.firstName[0] : '';
            const last  = (el.lastName)  ? el.lastName : '';
            const ref   = (el.isCompany) ? stringUp(el.companyName) : `${first}. ${last}`;
            if (ref){
                sellerRefList.push(ref);
            }            
        }
    }

    for (let idx = 0; idx < buyerClient.length; idx++){
        const el = buyerClient[idx];
        if (el){
            const first = (el.firstName) ? el.firstName[0] : '';
            const last  = (el.lastName)  ? el.lastName : '';
            const ref   = (el.isCompany) ? stringUp(el.companyName) : `${first}. ${last}`;
            if (ref){
                buyerRefList.push(ref);
            }
        }
    }
    
    const sellerRef = (sellerRefList || []).join(' & ');
    const buyerRef = (buyerRefList || []).join(' & ');

    if (bstText == 'buyer'){        
        matterReference = (buyerRefList.length > 0 || sellerRefList.length > 0) ? `${buyerRef} ${prefixRef[bstText]} ${sellerRef}` : '';
    }else if(bstText == 'seller'){
        matterReference = (buyerRefList.length > 0 || sellerRefList.length > 0) ? `${sellerRef} ${prefixRef[bstText]} ${buyerRef}` : '';
    }else if(bstText == 'transfer'){
        matterReference = (buyerRefList.length > 0 || sellerRefList.length > 0) ? `${sellerRef} ${prefixRef[bstText]} ${buyerRef}` : '';
    }

    const matterNameParam = {
        actions : {
            reference : matterReference
        }            
    };
    logIt2(fa.info, context, `[matterId::${matterId}].updateMatterReference `, [matterNameParam] );

    if (dbFileRef === 'getocrdata'){
        return matterReference;
    }
    await actionstep.updateMatter(matterId, matterNameParam, token, context);
    return matterReference;
}
