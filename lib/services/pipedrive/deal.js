const common = require('../common/constant');
const {fx,fxs, testJSON} = require('../common/utils');

exports.ready = function (body, context)
{
    let dealId;
    const pd_publish_logging = process.env.PD_PUBLISH_LOGGING || 'enable';

    if (body && body.current && body.current.id){
        dealId = body.current.id;
    }else{
        if (pd_publish_logging == 'enable'){
            context.log(`[ready] DEBUG ready.datapublish dealId=none`, JSON.stringify(body));
        }
        return false;
    }

    const stageId = body.current.stage_id;    
    const wonTime = body.current.won_time;    
    const label = body.current.label; //397 & 398
    const amcEnable = process.env.AMC_LABEL || 'disable';    
    const azureEnv = process.env.AMC_ENV || 'prod';

    let isPDLabelDisable = false;
    
    if (pd_publish_logging=='enable'){
        context.log(`[ready] DEBUG azureEnv:${azureEnv} stageId:${stageId} label:${label}-amcEnable(${amcEnable}) dealId:${dealId} wonTime:${wonTime}]`);
    }
    
    if ((amcEnable.trim()).toLowerCase() == 'disable'){
        isPDLabelDisable = true;
    }
    
    if (((stageId == common.PD_MCREADY_STATUS && azureEnv == 'prod') || 
         (stageId == common.PD_MC_TESTING && azureEnv == 'staging')) &&
       (label==amcEnable || isPDLabelDisable) &&
       (wonTime===undefined || wonTime==null)){
           
        context.log(`[deal::${dealId}].ready body.current >>`, JSON.stringify(body.current));
        return true;
    }
    return false;
}
 
const getMatterType = function(deal, context){
    const actionType = common.PD_MATTER_TYPE || [];
    const mtype = actionType.find((row) => {return (deal.state === row.state && common.PIPEDRIVE_BST[deal.bst] === row.bst) ? row : null;});
    const matterKind = {
        matterTypeId :  (mtype.id) ? mtype.id: 0,
        matterTypeName : `Conveyancing.com.au: ${mtype.state} - ${mtype.bst}`,
        bstText : mtype.bst
    }
    context.log(`[deal::${deal.dealId}].getMatterType >`, JSON.stringify(matterKind));
    return matterKind;
}

exports.dealDetails = function (body, context)
{
    const dealId                = body.current.id;
    const pdBST                 = `${body.current['32a26829a091f3b91608edac353d72af483ce188']}`;
    const pdState               = body.current['415157dbc97375a775fc1f28219a816ca52b9ca6'];
    const pdNewMatterType       = `${body.current['d158717bdfff0a742fe0a4670631edd2f0c70864']}`;
    const pdNewConveyancingType = `${body.current['0c0637325ba013d60d119ff415d50fd6c2dd3b06']}`;    
    const conciergeId           = body.current.user_id || body.current.creator_user_id;
    const fixedFee              = body.current['596cb96595ed1561da74322039f45a4711f793d8'];
    const offerApplied          = body.current['f1122a434a9a8adf869714cf947464c56f2aec22'];
    const discountOffered       = body.current['191322d3f05a278eb337c08dd08aef13eb2625cb'];
    const additionalInfo        = body.current['607538c553cfdfb0f2fe07f6421035c748ae9021'];
    
    const dealData = {
        dealId          : body.current.id,
        stageId         : body.current.stage_id,
        personName      : body.current.person_name,
        state           : (pdState) ? common.PD_STATE[`${pdState}`] : '',
        bst             : pdBST,
        owner           : body.current.owner_name,
        additionalInfo  : fxs(additionalInfo),
        discountOffered : fxs(discountOffered),
        fixedFee        : fxs(fixedFee),
        offerApplied    : fxs(offerApplied),
        conveyanceType              : (pdBST) ? common.AS_CONVEYANCE_TYPE[`${pdBST}`] : '',
        NewFieldMatterType          : (pdNewMatterType) ? common.PD_MATTER_TYPE_NEWFIELD[`${pdNewMatterType}`] : '',
        NewFieldConveyancingType    : (pdNewConveyancingType) ? common.PD_CONVEYANCING_TYPE_NEWFIELD[`${pdNewConveyancingType}`] : '',
        conciergeId                 : conciergeId
    }
    const matterType = getMatterType(dealData, context);
    const dealInfo = {...dealData, 
        matterTypeId    : matterType.matterTypeId, 
        matterTypeName  : matterType.matterTypeName, 
        fileReference   : '', 
        fileNote        : '',
        bstText         : matterType.bstText
    };
    context.log(`[deal::${dealId}].dealDetails(0)  >`, JSON.stringify(dealInfo));
    return dealInfo;
}

exports.validPipe = function (body, context)
{
    const messageId = body.messageId;
    const dealId    = body.dealId;
    const matterId  = body.matterId;
    const pipedata  = body.pipedata;
    const validJson = testJSON(JSON.stringify(pipedata));

    context.log(`[deal::${dealId}][matterId::${matterId}].validPipe DeBUG body >>`, JSON.stringify(body));

    if (validJson && dealId && matterId && pipedata){
        context.log(`[deal::${dealId}][matterId::${matterId}].validPipe body.pipedata >>`, JSON.stringify(pipedata));
        return {
            messageId : messageId,
            dealId    : dealId,
            matterId  : matterId,
            data : {
                clientId: pipedata.clientId,
                deal    : pipedata.deal,
                address : pipedata.address,
                person  : pipedata.person
            }
        };
    }else{
        context.log(`[deal::${dealId}][matterId::${matterId}].validPipe body >>`, body );
    }
    return {
        error:1
    };
}
 
exports.validPublishPipe = function (body, context)
{
    const dealId    = body.dealId;
    const pipedata  = body.pipedata;
    const validJson = testJSON(JSON.stringify(pipedata));

    context.log(`[deal::${dealId}].validPublishPipe body >`, JSON.stringify(body));

    if (validJson && dealId){
        context.log(`[deal::${dealId}].validPublishPipe body.pipedata >`, JSON.stringify(pipedata));
        return {
            dealId    : dealId,
            pipedata : {
                personId : pipedata.personId,
                deal     : pipedata.deal,
                address  : pipedata.address
            }
        };
    }
    return false;
}

exports.validMatterSourcePipe = function (dealId, mcSource, context)
{
    const pipedata  = mcSource || {};
    const validJson = testJSON(JSON.stringify(pipedata));
    context.log(`[deal::${dealId}].validMatterSourcePipe validJson:${validJson} >`, JSON.stringify(pipedata));

    if (validJson && dealId){
        return pipedata;
    }
    return false;
}

exports.pdDealDetails = function (pdBody, context)
{
    const dealId                = pdBody.id;
    const pdBST                 = `${pdBody['32a26829a091f3b91608edac353d72af483ce188']}`;
    const pdState               = pdBody['415157dbc97375a775fc1f28219a816ca52b9ca6'];
    const pdNewMatterType       = `${pdBody['d158717bdfff0a742fe0a4670631edd2f0c70864']}`;
    const pdNewConveyancingType = `${pdBody['0c0637325ba013d60d119ff415d50fd6c2dd3b06']}`;    
    const conciergeId           = pdBody.user_id.id || pdBody.creator_user_id.id;
    const fixedFee              = pdBody['596cb96595ed1561da74322039f45a4711f793d8'];
    const offerApplied          = pdBody['f1122a434a9a8adf869714cf947464c56f2aec22'];
    const discountOffered       = pdBody['191322d3f05a278eb337c08dd08aef13eb2625cb'];
    const additionalInfo        = pdBody['607538c553cfdfb0f2fe07f6421035c748ae9021'];
    
    const dealData = {
        dealId          : dealId,
        stageId         : pdBody.stage_id,
        personName      : pdBody.person_name,
        state           : (pdState) ? common.PD_STATE[`${pdState}`] : '',
        bst             : pdBST,
        owner           : pdBody.owner_name,
        additionalInfo  : fxs(additionalInfo),
        discountOffered : fxs(discountOffered),
        fixedFee        : fxs(fixedFee),
        offerApplied    : fxs(offerApplied),
        conveyanceType              : (pdBST) ? common.AS_CONVEYANCE_TYPE[`${pdBST}`] : '',
        NewFieldMatterType          : (pdNewMatterType) ? common.PD_MATTER_TYPE_NEWFIELD[`${pdNewMatterType}`] : '',
        NewFieldConveyancingType    : (pdNewConveyancingType) ? common.PD_CONVEYANCING_TYPE_NEWFIELD[`${pdNewConveyancingType}`] : '',
        conciergeId                 : conciergeId
    }
    const matterType = getMatterType(dealData, context);
    const dealInfo = {...dealData, 
        matterTypeId    : matterType.matterTypeId, 
        matterTypeName  : matterType.matterTypeName, 
        fileReference   : '', 
        fileNote        : '',
        bstText         : matterType.bstText
    };
    context.log(`[deal::${dealId}].dealDetails(0)  >`, JSON.stringify(dealInfo));
    return dealInfo;
}
