const common = require('../common/constant');

exports.propertyDetails = function (body, context)
{
    const dealId        = body.current.id;
    const unitNo        = body.current['3b1aa5bdc46fbf35bfdd71fcc5998d7840c417a8'];
    const postCode      = body.current['a6170b0ef59b625144a0296f34888541cbb41d86'] || '';
    const propertyCode  = body.current['af0c8bd753301f888695cfb36416dd40d9aaa0b3'] || '';
    const state         = body.current['415157dbc97375a775fc1f28219a816ca52b9ca6'] || '';
    const streetName    = body.current['8da7a98863d5dd86cd6cd54a4fd46b859cc7b3fa'] || '';
    const streetNo      = body.current['3997f0d4eb2c37db2d0bce3d309d3bdf6c9add9b'] || '';
    const streetType    = body.current['d0da6921c9ce3a945ca85d4444010d853b422f7b'] || '';
    const suburb        = body.current['f2ea1652bb0ee75470a045d615a3f28783479502'] || '';

    const pa = {
        post_code       : postCode,
        property_code   : propertyCode,
        state           : state ? common.PD_STATE[state] : '',
        street_name     : streetName,
        street_no       : streetNo,
        street_type     : streetType,
        suburb          : suburb,
        unit_no         : (unitNo && unitNo.length > 0 ) ? unitNo : ''
    }
    
    const propertyAddress = {...pa, property_type : `${common.PIPEDRIVE_PROPERTY_TYPE[pa.property_code]}`};
    context.log(`[deal::${dealId}].propertyDetails >>`, JSON.stringify(propertyAddress));
    return propertyAddress;
}
