const axios = require('axios');
var FormData = require('form-data');

// const process = {
//   env : {
//     PIPEDRIVE_API_ENDPOINT : "https://api.pipedrive.com",
//     PIPEDRIVE_API_TOKEN : "26080911ea69bce914ad1fead930770c2feaa495"
//   }
// }

exports.get = async function (resource, ctx) {
    const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;
    const options = {
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
      }
    };  
    const response = await axios.get(url, options);
    return response.data || {};
};

exports.get2 = async function (resource, context) {
  const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;

  try {
    const response = await axios.get(url);
    return response || {};

  } catch (err) {
    const errMsg = (err && err.response && err.response.data) ? err.response.data : err;
    if (context){
      context.log.error("[callApi].get2 error >>", JSON.stringify(errMsg));      
    }else{
      console.log("[callApi].get2 error >>", JSON.stringify(errMsg));
    }    
    return errMsg;
  }
};

exports.test_get2 = async function (resource) {
  const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;

  try {
    const response = await axios.get(url);
    return response || {};

  } catch (err) {
    const errMsg = (err && err.response && err.response.data) ? err.response.data : err;
    console.log("[callApi].get2 error >>", JSON.stringify(errMsg));
    return errMsg;
  }
};

exports.put = async function (resource, xdata, ctx) {
  const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;
  const options = {
    headers: {        
      'Content-Type': 'application/json'
    }
  };
  const response = await axios.put(url, xdata, options);
  return response.data || {};
};

exports.upload = async function(resource, dealId, filename, payload, docType, ctx)
{
  const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;
    var data = new FormData();
    data.append("file", payload, {contentType: docType, filename: filename });
    data.append('deal_id', dealId);

    var config = {
        method: 'post',
        url: url,
        headers: { 
            'Content-Type': 'multipart/form-data;boundary=' + data.getBoundary(),
            ...data.getHeaders()
        },
        data : data,
        maxContentLength: Infinity,
        maxBodyLength: Infinity
    };
    const response = await axios(config);
    return response;
}

exports.post = async function (resource, xdata, context) {
  const url = `${process.env.PIPEDRIVE_API_ENDPOINT}/${resource}api_token=${process.env.PIPEDRIVE_API_TOKEN}`;
  const options = {
    headers: {        
      'Content-Type': 'application/json'
    }
  };
  const response = await axios.post(url, xdata, options);
  return response.data || {};
};
