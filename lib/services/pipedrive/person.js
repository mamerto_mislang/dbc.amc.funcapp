const common = require('../common/constant');
const pd = require('./callApi');
const pipedrive = require('.');
const {allNumber, fx} = require('../common/utils')

const parseState = function(el, addrArr, i)
{
    const states = common.STATES;
    let xAddr = {suburb : '', state : '', post_code : ''};
  
    for (let ii = 0; ii < states.length; ii++) {
        const state = states[ii];
        const xpos = el.toUpperCase().lastIndexOf(state);
        if (xpos != -1 ){
            const xd = el.split(' ');
            if (i===0){
                // xAddr = { unit_street_no : '', street_name : '', street_type : '', suburb : '', state : '', post_code : ''};
                xAddr = { suburb : '', state : '', post_code : ''};
                xAddr.street_name2 = el.substring(0,xpos);
                const xlast = el.substring(xpos);
                const alast = xlast.split(' ');
                xAddr.state = alast[0].trim();
                xAddr.post_code = (alast[1]) ? alast[1].trim() : '';
                break;
            }

            if (xd[1]){
                xAddr.state = (xd[0] == state) ? xd[0].trim() : xd[1].trim();
                xAddr.post_code = (xd[0] == state) ? xd[1].trim() : xd[0].trim();
            }else{
                const y2 = i+1;
                xAddr.state = el.trim();
                xAddr.post_code = (addrArr[y2]) ? addrArr[y2].trim() : '';
            }
            const y1 = i-1;
            xAddr.suburb = (addrArr[y1]) ? addrArr[y1].trim() : '';      
            
        }
    }
    return xAddr;
}        
        
exports.extract = function(v)
{
    return exractNew(v);
}

const exractNew = function(fullAddress)
{
    if (!fullAddress){
        return false;
    }
    const addrArr = fullAddress.split(',');
    let xAddr = { unit_street_no : '', street_name : '', street_type : ''};
    if (fullAddress.lastIndexOf(',') === -1){
        xAddr.street_name = fullAddress;
        const xAddr2 = parseState(fullAddress, addrArr, 0);
        if (xAddr2.state){
            xAddr = {...xAddr, ...xAddr2};
        }
        return xAddr;
    }

    for (let i = 0; i < addrArr.length; i++) {
        const el = addrArr[i].trim();

        if (i===0){
            const unst = el.split(' ');
            if(unst[1]){
                if ((unst[0]).lastIndexOf('/') > 0 ){
                    xAddr.unit_street_no = unst[0];
                    xAddr.street_name = (unst[2]) ? `${unst[1]} ${unst[2]}` : unst[1];    
                }else{
                    xAddr.unit_street_no = unst[0];
                    xAddr.street_name =(unst[2]) ? `${unst[1]} ${unst[2]}` : unst[1];
                }               
                xAddr.street_name = (unst[3]) ? `${xAddr.street_name} ${unst[3]}` :  xAddr.street_name;
            }else{
                if (allNumber(unst[0])){
                    xAddr.unit_street_no = unst[0];
                }               
                xAddr.street_name = addrArr[++i].trim();
            }
        }
        
        const xAddr2 = parseState(el, addrArr, i);

        if (xAddr2.state){
            xAddr = {...xAddr, ...xAddr2};
        }
    }
    return xAddr;
}


exports.personDetails = async function (body, context)
{
    const dealId = body.current.id;
    const personId = body.current.person_id;
    const resource = `v1/persons/${personId}?`;
    const res = await pd.get2(resource, context);
    const rData = res.data.data;

   // context.log(`[dealId::${dealId}].personDetails ${resource} >>`, JSON.stringify(rData));

    const testLastName = rData['fa73897383f1f9b770336537db7500cf8d075509'];
    const testMiddleName = rData['33767eb28b8b2195c8aa0ce9a35de12bc23a5673'];
    const testStreetName = rData['445d83bc0eed59a72f3a3b22eec06a3ec42f9aa8'];
    const isCompany = rData['9be2d2996aca412eb7b2ae50258cf106bcb686a0'];
    
    let pdPerson =  {
        personId       : personId,
        firstName      : fx(rData.first_name),
        lastName       : fx(rData.last_name),
        name           : fx(rData.name),
        testFirstName  : fx(rData.name),
        testLastName   : fx(testLastName),
        testMiddleName : fx(testMiddleName),
        email          : fx(rData.email[0].value),
        phone          : fx(rData.phone[0].value),
        isCompany      : (isCompany==400) ? 'T' : 'F'
    }
    
    const xAddress = exractNew(testStreetName);        
    pdPerson = (xAddress) ? {...pdPerson, ...xAddress} : {...pdPerson};
    context.log(`[dealId::${dealId}].personDetails pdPerson >`, JSON.stringify(pdPerson));
    return pdPerson;
}

exports.personDetailsPipe = async function (dealId, personId, context)
{
    const resource  = `v1/persons/${personId}?`;
    const res       = await pd.get2(resource, context);
    const rData     = res.data.data;

    const testLastName   = rData['fa73897383f1f9b770336537db7500cf8d075509'];
    const testMiddleName = rData['33767eb28b8b2195c8aa0ce9a35de12bc23a5673'];
    const testStreetName = rData['445d83bc0eed59a72f3a3b22eec06a3ec42f9aa8'];
    const isCompany      = rData['9be2d2996aca412eb7b2ae50258cf106bcb686a0'];

    const rdLastName  = rData.last_name;
    const rdFirstname = rData.first_name
    const cLastName   = rdLastName  || testLastName || '';
    const cFirstname  = rdFirstname || (rData.name||'').replace(cLastName,'').trim();
    
    // {"firstName":"","lastName":"siva Eha","name":"siva Eha","testFirstName":"siva Eha","testLastName":"Sivakumaran","testMiddleName":""
    
    let pdPerson =  {
        personId       : personId,
        firstName      : fx(cFirstname),
        lastName       : fx(cLastName),
        name           : fx(rData.name),
        testFirstName  : fx(rData.name),
        testLastName   : fx(testLastName),
        testMiddleName : fx(testMiddleName),
        email          : fx(rData.email[0].value),
        phone          : fx(rData.phone[0].value),
        isCompany      : (isCompany==400) ? 'T' : 'F'
    }
    
    const pFirstName = pdPerson.firstName || '';
    const pLastName  = pdPerson.lastName || '';
    const pIsCompany = pdPerson.isCompany || 'F';

    const xAddress = exractNew(testStreetName);
    pdPerson = (xAddress) ? {...pdPerson, ...xAddress} : {...pdPerson};
    context.log(`[dealId::${dealId}].personDetailsPipe pdPerson >`, JSON.stringify(pdPerson));
    
    if (pIsCompany == 'F'){
        if (pFirstName.length === 0 || pLastName.length === 0){
            throw new Error('PRIMARY_CLIENT_NAME_INVALID');
        }
    }
    return pdPerson;
}

exports.resetAMCLabel = async function (body, context, correlationId)
{
    const dealId = body.current.id;
    context.log(`${correlationId}::resetAMCLabel`, dealId);

    const xdata = {
        label : ''
    };
    const res = await pipedrive.updatePipedrive(dealId, xdata, context);
    context.log(`${correlationId}::resetAMCLabel result`, JSON.stringify(res));
    return res;
}
