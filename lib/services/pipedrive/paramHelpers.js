const { fillUpName, fx } = require("../common/utils");

exports.createInitMatterParam = function (deal)
{
    let matterType = deal.matterTypeId;
    //filter matter_type
    if (deal.matterTypeId && deal.matterTypeId > 100){
        matterType =  parseInt((matterType.toString()).substr(0, 2));
    }

    const param = {
                actioncreate : {
                    actionName : 'Pending',
                    fileReference : deal.fileReference,
                    fileNote : deal.fileNote,
                    links : {
                        actionType : matterType
                    }
                }
    }
     return param;   
}

exports.createClientParam = function (person)
{
    const companyName = (person.isCompany == 'T') ? person.name : '';
    
    const clientPerson = {
        isCompany   : (person.isCompany == 'T') ? true : false,
        companyName : companyName ? companyName : '',
        firstName   : person.firstName ? person.firstName : '',
        lastName    : (person.lastName) ? person.lastName : person.testLastName,
        middleName  : person.testMiddleName ? person.testMiddleName : '',
        email       : person.email ? person.email : ''
      };
    return clientPerson;
}


exports.createOCRParticipantParam = function (party)
{
    const isCompany = party.isCompany;
    const person = fillUpName((party.name||'').trim());

    const partyPerson = {
        isCompany   : isCompany,
        companyName : (isCompany) ? fx(party.name) : '',
        firstName   : fx(person.firstName),
        lastName    : fx(person.lastName),
        middleName  : fx(person.middleName),
        email       : party.email ? fx(party.email) : ''
      };
    return partyPerson;
}


exports.createCompanyParam = function (name='', email=undefined)
{
        
    const partyPerson = {
        isCompany   : true,
        companyName : name||'', //fx(name),
        firstName   : '',
        lastName    : '',
        middleName  : '',
        email       : (email) ? fx(email) : ''
      };
    return partyPerson;
}

exports.getClientCredential = function(res, clientIdOnly=false)
{
    let asClientId;
    let aslastName;
    
    if (res.participants && res.participants.id){
        asClientId = res.participants.id;
        aslastName = (res.participants.isCompany == 'T') ? res.participants.companyName : res.participants.lastName;

    }else if(res.participants && res.participants[0] && res.participants[0].id){
        asClientId = res.participants[0].id;        
        aslastName = (res.participants[0].isCompany == 'T') ? res.participants[0].companyName : res.participants[0].lastName;
    }else{
        return false;
    }

    if (clientIdOnly){
        return (asClientId) ? {clientId : asClientId} : false;
    }
    const code3 = (aslastName) ? (aslastName.substr(0,3)).toUpperCase() : false;

    return (code3) ? {
        clientId : asClientId,
        clientCode : code3.padEnd(3, "0")
    } : false;
}

exports.getActionstepId = function(res)
{
    let asClientId;
    let aslastName;
    
    if (res.participants && res.participants.id){
        asClientId = res.participants.id;
        aslastName = res.participants.lastName;
    }else if(res.participants && res.participants[0] && res.participants[0].id){
        asClientId = res.participants[0].id; 
        aslastName = res.participants[0].lastName;
    }
    return (asClientId) ? asClientId : false;
}

exports.getBST = function(mtName)
{
    if ((mtName||'').indexOf('Seller') !=-1){
        return 'seller';
    }else  if ((mtName||'').indexOf('Transfer') !=-1){
        return 'transfer';
    }
    return 'buyer';
}