const pipedrive = require('./callApi');
const tule = require('../common/dataTools');
const doc = require('../common/documents');

const getFlow = async function (dealId, start = 0)
{
    const resource = `v1/deals/${dealId}/flow?start=${start}&`;
    const res = await pipedrive.get(resource, 'ctx');
    return res.data || [];
}

exports.getFlowData = async function(dealId, start = 0)
{
    let notes = [];
    let files = [];
    let flowData = [];

    const result = (await getFlow(dealId,start));
    for (let i = 0; i < result.length; i++) {
        const el = result[i];
        if (el.object === 'note'){
            notes.push(el.data.content);
        }else if (el.object === 'file'){
            files.push({fileId : el.data.id, filename: el.data.name, docType :  doc.getDocumentType(el.data.name)});
        }
    }
    flowData = {notes:notes, files:files};
    return flowData;
}

exports.updatePipedrive = async function(dealId, xdata, context)
{
    const resource = `v1/deals/${dealId}?`;
    const res = await pipedrive.put(resource, xdata, context);
    return res;
}

exports.getData = async function(fileId, context=null)
{
    const resource = `v1/files/${fileId}/download?`;
    const res = await pipedrive.get2(resource, context);
    if (!res || !res.request || !res.request.res || !res.request.res.responseUrl){
        throw Error("Error in pipedrive, response url is empty!")
    }
    const payload = await tule.urlGetContent(res.request.res.responseUrl);
    return payload;
}

exports.uploadDocumentInPipedrive = async function(dealId,filename, payload, docType, context)
{
    try {
        const resource = `api/v1/files?`;       
        const response = await pipedrive.upload(resource, dealId,filename, payload, docType, context);
        return response;
        
    } catch (err) {
        const errMsg = (err && err.message) ? err : {error: 'pipedrive api upload error!'};
        context.log.error("[pipedrive.upload].uploadDocumentInPipedrive error >>", JSON.stringify(errMsg));
        return errMsg;
    }
}

exports.postNotes = async function(xdata, context)
{
    const resource = `v1/notes?`;
    const response = await pipedrive.post(resource, xdata, context);
    return response;
}

exports.pipedriveDeal = async function (dealId, context)
{
    const resource = `v1/deals/${dealId}?`;
    const res = await pipedrive.get2(resource, context);
    // context.log(`[dealId::${dealId}].pipedriveDeal result >> `, res);

    if (!res.success && res.error){
        return {
            error : 1,
            msg : res.error
        }
    }
    const pipeData = res.data.data;
    const stageId = pipeData.stage_id;
    // context.log(`[dealId::${dealId}].pipedriveDealDetail ${resource} stageId::${stageId}`);
    return {stageId : stageId};
}

exports.getPipedriveDeal = async function (dealId, context)
{
    const resource = `v1/deals/${dealId}?`;
    const res = await pipedrive.get2(resource, context);
    //const res = await pipedrive.test_get2(resource);
    // console.log("DEBUG getPipedriveDeal >>", res);

    if (!res.success && res.error){
        return {
            error : 1,
            msg : res.error
        }
    }
    return (res && res.data && res.data.data) ? res.data.data : res;
}

exports.testGetFlowData = async function(dealId, start = 0)
{
    let notes = [];
    let files = [];
    let flowData = [];

    const result = (await getFlow(dealId,start));
    for (let i = 0; i < result.length; i++) {
        const el = result[i];
        
        if (el.object === 'mailMessage'){        
            console.log("DEBUG el", el);
        }
    }
    flowData = {notes:notes, files:files};
    return flowData;
}