const { jsonResponse, jsonCustomResponse } = require('../lib/services/common/helpers');
const db = require('../lib/services/database/mysql');

module.exports = async function (context, req) {
    
    const dealId       = req.body.dealId;
    const matterId     = req.body.matterId;
    const clientId     = req.body.clientId || 513;
    const matterTypeId = req.body.matterTypeId;
    const matterName   = req.body.matterName;

    try {
            context.log(`[deal::${dealId}] Contract Updates Process (~)`);
            if (dealId && matterId && clientId && matterTypeId)
            {
                const matterForCalcu = {
                    id              : dealId, 
                    matter_id       : matterId, 
                    file_note       : 'AMC Created Matter', 
                    matter_type_id  : matterTypeId, 
                    matter_name     : `${matterName}` , 
                    assignedto_id   : 513, 
                    client_id1      : clientId, 
                    status          : 8
                }
                
                db.createFastMatter(matterForCalcu, context);            
                context.res = jsonCustomResponse({ message: 'AMC Matter sent to FASTDB For Settlement Calculator use!' });
            }else{
                throw new Error('Matter Data for FAST Calcu used is invalid!');
            }
        
    } catch (err) {
        
        context.log.error(`[deal::${dealId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[deal::${dealId}].main(-) Stacktrace >>`, err.stack);
        }
        context.res = jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}
