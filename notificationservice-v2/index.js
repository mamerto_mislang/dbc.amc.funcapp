const service = require('../lib/services');
const { jsonResponse, jsonCustomResponse } = require('../lib/services/common/helpers');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    const dealId = req.body.deal_id;
    const matterId = req.body.matter_id;
    const status   = req.body.status || '';
    const message  = req.body.message || '';
    const stepId  = req.body.step_id || 0;
    const mcInfo  = req.body.mc_info || '';

    try {
            context.log(`[deal::${dealId}] Notification Process (~) step_id: ${stepId} mc-info:`, mcInfo);
            if (dealId){
                if (status == 'error'){
                    (await service.resetPipedriveAMCLabel(dealId, context));
                }
                (await service.sendNotificationToPipedrive(matterId, dealId, status, message, context));
                context.res = jsonCustomResponse({ message: 'Deal Successfully notified!', dealId: dealId, matterId: matterId });
                if (stepId > 0 ){
                    (await service.updateMCStep(dealId, stepId, mcInfo, context));
                }
                
            }else{
                context.res = jsonCustomResponse({ message: 'Notify Service Alert!' });
            }            
        
    } catch (err) {
        context.res = jsonResponse(err.message, 400);
        context.log.error(`[${correlationId}][deal::${dealId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[deal::${dealId}].main(-) Stacktrace >>`, err.stack);
        }

    }finally {
        context.done();
    }
}