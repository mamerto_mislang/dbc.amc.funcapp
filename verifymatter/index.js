const service = require('../lib/services');
const { accessToken } = require('../lib/services/actionstep/getASToken');
const { jsonResponse, jsonCustomResponse  } = require('../lib/services/common/helpers');
const {logIt2} = require('../lib/services/common/logger');
const fa = require('../lib/services/common/share/loglevel');

module.exports = async function (context, req) {
    const matterId = req.query && (req.query.matter_id || req.query.matterId);
    const token     = (await accessToken());
    
    try {        

        logIt2(fa.info, context, `[matterId::${matterId}](+)verifymatter matterId:${matterId}`);
        const matter  = await service.getActionstepMatterDetails(matterId, token, context);
        // const matterInfo = await service.getDBMatterDetails(matterId, context);
        // if (matterInfo["matterType"]){
        //     delete matterInfo["matterType"];
        //     delete matterInfo["mcSource"];
        // }

        const matterInfo = service.verifyActionstepMatterDetails(matterId, matter, context);
        context.res  = jsonCustomResponse(matterInfo);           
        
    } catch (err) {
        const errMsg   = err.message || 'error';
        const errStack = err.stack || '';
        
        logIt2(fa.error, context, `[matterId::${matterId}].main(-) Error response >>`, [err.message, errStack]);
        context.res = jsonResponse(errMsg, 400);

    }finally {
        context.done();
    }
}
