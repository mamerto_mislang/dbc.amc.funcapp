const service = require('../lib/services');
const { accessToken } = require('../lib/services/actionstep/getASToken');
const { getFlowData } = require('../lib/services/pipedrive');
const { personDetailsPipe } = require('../lib/services/pipedrive/person');
const collection = require('../lib/services/actionstep/collections');
const datalake = require('../lib/services/datalake');
const { jsonResponse, jsonCustomResponse, jsonBusCalculatorParams } = require('../lib/services/common/helpers');
const { validPublishPipe } = require('../lib/services/pipedrive/deal');
const { send2BusQueue, send2BusQueueV2 } = require('../lib/services/notify');
const sysMsg = require('../lib/services/common/share/messages');
const {logIt2} = require('../lib/services/common/logger');
const fa = require('../lib/services/common/share/loglevel');
const bus = require('../lib/services/servicebus');
let AMCStep = 0;
let retryCounter = 0;
let mcInfoBus;

const sendCalcuDatatoBus = async function(dealId, msg, context)
{
    const body = {
        dealId       : msg.dealId,
        matterId     : msg.matterId,
        clientId     : msg.clientId,
        matterTypeId : msg.matterTypeId,
        matterName   : `${msg.matterName}`
    };

    const payload = jsonBusCalculatorParams(body, context);
    await bus.sendMessageId(payload, 'settlement-calculator', `${dealId}`, context);
}

const AMCStep2 = async function(deal, person, personId, token, context)
{    
    logIt2(fa.info, context, `[deal::${deal.dealId}] running step AMCStep2 (+)`);  
    const client   = (await service.processClientParticipant(deal, person, token, context));    
    const matter   = (await service.createMatter(deal, client, token, context));
    // const matterId = matter.matterId;
    // const clientId = (client.clientIdCode && client.clientIdCode.clientId) || false;
    return  {client : client, matter : matter};
}

const AMCStep3 = async function(matter, token, context)
{
    logIt2(fa.info, context, `[matter::${matter.matterId}] running step AMCStep3 (+)`);
    (await service.updateMatterName(matter.matterId, matter.matterName, token, context));
}

const AMCStep4 = async function(deal, person, address, matterId, clientId, token, context)
{
    logIt2(fa.info, context, `[deal::${deal.dealId}] running step AMCStep4 (+)`);
    (await service.createMatterParticipants(matterId, deal, clientId, token, context));   
    (await service.createPropertyAddressParticipant(matterId, address, person, token, context));
    (await service.createSalesPersonParticipant(matterId, deal, token, context));                     
    (await service.createSMSGatewayParticipant(matterId, token, context));    
}

const AMCStep5 = async function(deal, matterId, token, context)
{
    logIt2(fa.info, context, `[deal::${deal.dealId}] running step AMCStep5 (+)`);
    const pdDataFlow = (await getFlowData(deal.dealId));
    (await service.updateMatterFileNotes(matterId, pdDataFlow.notes, token, context));
    (await service.uploadDocsPipedriveToActionstep(matterId, pdDataFlow.files, token, context));
}

const AMCStep6 = async function(dealId, matterId, context)
{
    logIt2(fa.info, context, `[deal::${dealId}] running step AMCStep6 (+)`);
    (await datalake.updateMatterPipedrive(matterId, dealId, context));
}       

const AMCStep7 = async function(dealId, matter, context)
{
    logIt2(fa.info, context, `[deal::${dealId}] running step AMCStep7 (+)`);
    const res = (await service.updatePipedriveStatus(matter, dealId, context));
}       

const AMCStep8 = async function(deal, matter, address, token, context)
{
    logIt2(fa.info, context, `[deal::${deal.dealId}] running step AMCStep8 (+)`);
    const ocrResult = await service.processOCR(matter, deal, token, {"ocrUpdate": false, "oid": 0}, context);    
    (await collection.updateMatterDataCollection(matter.matterId, deal, address, ocrResult, token, context));        
    (await service.notifyTeams(matter, deal, address, ocrResult.dailyMatterCount, context));
}

const AMCStep9 = async function(deal, matter, person, address, context)
{
    logIt2(fa.info, context, `[deal::${deal.dealId}] running step AMCStep9 (+)`);
    (await service.sendMCNotesToPipedrive(matter, deal, person, address, context));
}

const processMatterV2 = async function(xpipe, context, stepId=0)
{
    const dealId    = xpipe.dealId;
    const deal      = xpipe.pipedata.deal;
    const address   = xpipe.pipedata.address;
    const personId  = xpipe.pipedata.personId;

    // const token     = (await accessToken());
    let token  = (await accessToken());

    const tokenExpired = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhIjoiT0dFNU56UTNZamswTURCa1lUY3pNREUyTldKaE5XRTFZall5TVRVeVlXTTBaR0ZqWXpOak16SmxZVFppIiwiYiI6Im5hdGlvbmFsbHMiLCJjIjoiMTAuNjAuMy4xNjEiLCJnIjoiVDE2LTE3LTE0IiwiZSI6IkYiLCJmIjoiQXVzdHJhbGlhXC9CcmlzYmFuZSIsImgiOiJiIiwiZCI6IiIsIm5iZiI6MTYzNjE3NzI1MywiZXhwIjoxNjM2MjA2MDYyfQ.OyeiRnoaIW13ZxujdY_KhBVB5KzQcKCGK3doZVTY3WnhxeToEV-uTQb7SwL8u_ncoYSH9ySjnfYc5uLFuzhMtJg_MLKKHxYf0JQWr3WfLWJjqx_7dqdHbeoFRBvIoZ-ZbEfQaEwzVOwb3tpmjaX7FmjBbKxbgNBDmsmxgF1E_7AWb0h1bOaSZTfT8vY_vVlelHCq6U6qRAAwVI_1Z9NlALxL8BNY0rT30priNd19H9Tq0w4EGcDWcB4DWqwNv67SVAto-wFMbbSdAVno6CbS9Mbu7qe4AtD1l3BBTS8cjiJ9RXYeAi450O5Qnd3jUXkeGAlzr2C1Z4ZCmq1mJm8S_g';
    
    let client, matter;
    let matterId, clientId;

    try {
        logIt2(fa.debug, context, `[deal::${dealId}].dataprocess (+++) stepId = ${stepId}`);
        const isProcessed = await service.processDealwPayload(dealId, JSON.stringify(xpipe), context);        
        if (isProcessed===true && stepId === 0){
            logIt2(fa.info, context, `[deal::${dealId}] dataprocess - deal already exist! >> `, [isProcessed]);
            return false;
        }
        await send2BusQueue(dealId, 0, 'info', '<b>STATUS:</b> Automatic Matter Creation initiated ....', context);
        const person = (await personDetailsPipe(dealId, personId, context));

        AMCStep=2;                
        if (stepId <= 2){
            const step2 = await AMCStep2(deal, person, personId, token, context);
            client   = step2.client;
            clientId = (client.clientIdCode && client.clientIdCode.clientId) || false;
            matter   = step2.matter;
        }else{
            matter   = {matterId: mcInfoBus.matterId, matterName: mcInfoBus.matterName};
            clientId = mcInfoBus.clientId;
        }
        
        matterId = matter.matterId;
        
        if (stepId === 0){
            mcInfoBus =  {...matter, clientId: clientId, dealId: dealId, matterTypeId: deal.matterTypeId };
            await sendCalcuDatatoBus(dealId, mcInfoBus, context);
        }
        
        if (stepId <= 3){
            AMCStep = 3;
            (await AMCStep3(matter, token, context));
        }
        
        if (stepId <= 4 || stepId === 8){
            AMCStep = 4;
            (await service.deleteMatterParticipants(matterId, token, context));
            await AMCStep4(deal, person, address, matterId, clientId, token, context);
            stepId = 4;
        }
        
        if (stepId <= 5){
            AMCStep = 5;
            (await AMCStep5(deal, matterId, token, context));
        }

        if (stepId <= 6){
            AMCStep = 6;
            (await AMCStep6(dealId, matterId, context));
        }

        if (stepId <= 7){
            AMCStep = 7;
            (await AMCStep7(dealId, matter, context));
        }

        if (stepId <= 8){
            AMCStep = 8;
            (await AMCStep8(deal, matter, address, token, context));
        }

        if (stepId <= 9){
            AMCStep = 9;
            (await AMCStep9(deal, matter, person, address, context));
        }
        return matter;

    }catch (err){
        logIt2(fa.error, context, `[deal::${dealId}].processMatter(-) Error response >>`, [err.message]);
        const errMsg   = err.message || 'error';
        const errStack = (err.stack) ? (err.stack).replace(/\n/gi, "</br/>") : '';
        if(err.stack){
            logIt2(fa.error, context, `[deal::${dealId}].processMatter(-) Stacktrace >>`, [errMsg, errStack]);
        }
        
        let expired = false;
        let actionRetry = false;
        
        if (errMsg && (JSON.stringify(errMsg)).indexOf('expired_token') !=-1){
            expired = true;
        }else if (errMsg && (JSON.stringify(errMsg)).indexOf('Bad') !=-1){
            //502 Bad Gateway
            actionRetry = true;
        }
        return {error: true, step: AMCStep, expiredToken : expired, errorData : err, actionRetry : actionRetry };
    }
    
}

const initGlobal = function()
{
    AMCStep = 0;
    retryCounter = 0;
    mcInfoBus = {};
}

module.exports = async function (context, req) {
    const dealId = req.body.dealId;
    initGlobal();
    
    try {
        const pipe = validPublishPipe(req.body, context);
        if (!pipe){
            context.res = jsonResponse(`Deal ${dealId} is invalid!`);
        }else{
            logIt2(fa.info, context, `[deal::${dealId}].main(~) pipedata received >>`, [JSON.stringify(pipe)]);
            const matter = await processMatterV2(pipe, context);
            
            if(matter && matter.matterId){
                await send2BusQueue(dealId, matter.matterId, 'info', '<b>STATUS:</b> Automatic Matter Creation - Completed!', context);
                context.res  = jsonCustomResponse({ message: 'Deal successfully processed!', dealId: pipe.dealId, matterId : matter.matterId, matterName : matter.matterName });
            }else{
                
                if (matter.error){
                    if (matter.expiredToken || matter.actionRetry){
                        let mc_step = matter.step;
                        for (let index = 0; index < 1; index++) {
                            retryCounter++;
                            const matter2 = await processMatterV2(pipe, context, mc_step );

                            if (matter2.error){
                                mc_step = matter2.step;
                            }else{
                                await send2BusQueue(dealId, matter2.matterId, 'info', '<b>STATUS:</b> Automatic Matter Creation - Completed!', context);
                                context.res = jsonCustomResponse({ message: 'Deal successfully processed!', dealId: pipe.dealId, matterId : matter2.matterId, matterName : matter2.matterName });
                                break;
                            }                        
                        }
                    }else{
                        const err01 = matter.errorData || 'empty error main >>';
                        throw new Error(err01);
                    }

                }else{
                    context.res = jsonResponse(`Deal ${dealId} already exist!`);
                }
            }
        }       
        
    } catch (err) {
        
        const errMsg    = err.message || 'error';
        const errStack0 = (err.stack) ? err.stack : '';
        const errStack  = (err.stack) ? (err.stack).replace(/\n/gi, "</br/>") : '';
        
        logIt2(fa.error, context, `[deal::${dealId}].main(-) Error response >>`, [errMsg, errStack]);
        await send2BusQueueV2(AMCStep, dealId, 0, 'error', sysMsg.SYS_ERROR1, context, errMsg, errStack, JSON.stringify(mcInfoBus));
        await service.notifyTeamsAMCError(dealId, 0, errMsg, errStack0, context);
        context.res = jsonResponse(err.message, 400);

    }finally {
        context.done();
    }
}