const fs = require('fs');
const helper = require('../lib/services/common/helpers');
const { BlobServiceClient } = require('@azure/storage-blob');
const multipart = require("parse-multipart");
const services = require('../lib/services');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    context.log(`[${correlationId}] processing request (+)....`);

    try {
        context.log(`[${correlationId}](+) debug params1:`, req.query);
        context.log(`[${correlationId}](+) debug body:`, req.body);
        
        if (!req.body || req.body===undefined){

            const customData = helper.createResponse(req, null, null);
            context.log(`[${correlationId}](+)No file Uploaded here, customData:`, customData);
            context.res = helper.jsonResponse(customData);

        }else{

            const bodyBuffer = Buffer.from(req.body);
            const boundary = multipart.getBoundary(req.headers['content-type']);
            const parts = multipart.Parse(bodyBuffer, boundary);                       

            const blobServiceClient = await BlobServiceClient.fromConnectionString(process.env.AZURE_STORAGE_CONNECTION);        
            const container = process.env.AZURE_STORAGE_CONTAINER;
            const containerClient = await blobServiceClient.getContainerClient(container);
            
            const blobName = 'webforms/docs/'+helper.getBlobName2(parts[0].filename);
            const blockBlobClient = containerClient.getBlockBlobClient(blobName);              
            const uploadBlobResponse = await blockBlobClient.upload(parts[0].data, parts[0].data.length);
            context.log(`[${correlationId}](+) uploadBlobResponse:`, uploadBlobResponse);
            const code = await services.createDocumentMap(blobName, container, req);
            const customData =  helper.createResponse2(req, container, blobName, code);
            context.log(`[${correlationId}](+) customData:`, customData);
            context.res = helper.jsonResponse(customData);
        }

    } catch (err) {
        context.res = jsonResponse(err.message, 400);
        context.log.error(`[${correlationId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[${correlationId}].main(-) Stacktrace >>`, err.stack);
        }

    }finally {
        context.done();
    }
}
