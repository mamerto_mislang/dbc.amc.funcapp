const { dealDetails, ready } = require('../lib/services/pipedrive/deal');
const { propertyDetails } = require('../lib/services/pipedrive/property');
const { jsonResponse, jsonCustomResponse, jsonBusParams } = require('../lib/services/common/helpers');
const bus = require('../lib/services/servicebus');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    let dealId;
   
    try {
        const isReady = ready(req.body, context);
        if (!isReady){
            return;
        }else{
            const deal = dealDetails(req.body, context);
            dealId = deal.dealId;

            const address = propertyDetails(req.body, context);
            const body = {
                dealId     : dealId,
                pipedata : {
                    personId : req.body.current.person_id,
                    deal     : deal,
                    address  : address
                }
            };
            context.log(`[deal::${dealId}] data publish to bus`, JSON.stringify(body));
            const payload = jsonBusParams(body, context);
            await bus.sendMessageId(payload, 'datapublish', `${dealId}`, context);
            context.res = jsonCustomResponse({ message: `dealId ${dealId} data successfully published!`, data: body});
            
        }
    } catch (err) {
        context.log.error(`[${correlationId}][deal::${dealId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[deal::${dealId}].main(-) Stacktrace >>`, err.stack);
        }
        context.res = jsonResponse(err.message, 400);
        
    }finally {
        context.done();
    }
}