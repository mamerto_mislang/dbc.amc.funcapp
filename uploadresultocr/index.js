const service = require('../lib/services');
const { accessToken } = require('../lib/services/actionstep/getASToken');
const { jsonResponse, jsonCustomResponse  } = require('../lib/services/common/helpers');
const {logIt2} = require('../lib/services/common/logger');
const fa = require('../lib/services/common/share/loglevel');

module.exports = async function (context, req) {
    const dealId = (req.body && (req.body.dealId || req.body.deal_id));
    const matterId = (req.body && (req.body.matterId || req.body.matter_id));
    
    try {
        logIt2(fa.info, context, `[matterId::${matterId}](+)uploadresultocr matterId:${matterId}`);
        const ocrResult = await service.processOcrResultUpload(dealId, context);
        console.log("DEBUG MAIN >> ", ocrResult);
        if (ocrResult){
            const token = (await accessToken());
            await service.uploadASLinkMatter(matterId, ocrResult, token, context);
            context.res  = jsonCustomResponse({matterId: matterId, message: 'Successfully processed!'});
        }else{
            context.res  = jsonCustomResponse({matterId: matterId, message: 'Empty OCR was process!'});
        }
                
    } catch (err) {
        const errMsg   = err.message || 'error';
        const errStack = err.stack || '';
        
        logIt2(fa.error, context, `[matterId::${matterId}].main(-) Error response >>`, [err.message, errStack]);
        context.res = jsonResponse(errMsg, 400);

    }finally {
        context.done();
    }
}
