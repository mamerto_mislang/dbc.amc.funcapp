const { 
    PD_MCREADY_STATUS,
    PD_NO_SALE,
    PD_CONTRACTREVIEW_BOOKED_STATUS,
    PD_MATTER_CREATED_STATUS 
} = require('../lib/services/common/constant');
const helper = require('../lib/services/common/helpers');
const { pipedriveDeal } = require('../lib/services/pipedrive');

module.exports = async function (context, req) {
    const correlationId = context.invocationId;
    context.log(`[${correlationId}] processing request (+)....`);

    try {
        context.log(`[${correlationId}](+) debug params:`, req.query);
        const dealId =  req.query && (req.query.deal_id || req.query.dealId);
        let alreadyExist = false;
        let customData;

        if (dealId){
            const pipe = await pipedriveDeal(dealId, context);
            if (pipe.error){
                alreadyExist = false;
            }else{
                const inProgress = [PD_MCREADY_STATUS, PD_NO_SALE,PD_CONTRACTREVIEW_BOOKED_STATUS,PD_MATTER_CREATED_STATUS];
                alreadyExist = inProgress.includes(pipe.stageId);
            }           
            context.log(`[${correlationId}-deal::${dealId}](+)debug pipe:`, JSON.stringify(pipe), alreadyExist);

            if (alreadyExist){
                customData = helper.errorResponse(dealId);    
            }else{
                customData = helper.validResponse(dealId);    
            }
            
        }else{
            customData = helper.errorResponse(0, 'Invalid or empty deal Id provided!');
        }
        context.res = helper.jsonResponse(customData);
        
    } catch (err) {
        context.res = helper.jsonResponse(err.message, 400);
        context.log.error(`[${correlationId}].main(-) Error response >>`, err.message);
        if(err.stack){
            context.log.error(`[${correlationId}].main(-) Stacktrace >>`, err.stack);
        }

    }finally {
        context.done();
    }
}
